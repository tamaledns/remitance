<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This converts the 1000s to Ks
 * converts Millions to Ms
 *
 * */



if ( ! function_exists('number_convert'))
{
function number_convert($number,$decimalPlaces=2){
    if($number<1000){
        return round($number,$decimalPlaces);
    }elseif($number<1000000){
        return round(($number/1000),$decimalPlaces).'K';
    }elseif($number>=1000000){
        return round(($number/1000000),$decimalPlaces).'M';
    }
}
}

if ( ! function_exists('memory_convert')) {
    function memory_convert($size)
        {
            $unit = array('kb', 'mb', 'gb', 'tb', 'pb');
            return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
        }
}


if ( ! function_exists('remove_p'))
{
    function remove_p($string){


        $string=str_replace('<p>', '', $string);
        $string=str_replace('</p>', '', $string);
        $string=str_replace('<br/>', '', $string);

        return $string;
    }
}
if ( ! function_exists('textfom'))
{
    function textfom($txt){

        $ftext = htmlspecialchars($txt, ENT_QUOTES);
        return str_replace(array("\r\n", "\n"), array("<br />", "<br />"), $ftext);
    }
}

if ( ! function_exists('br2nl'))
{
    function br2nl($string)
        {
            return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
        }
}

if(!function_exists('stringNumbers')) {

    function stringNumbers($x)
        {
            $nwords = array("zero", "one", "two", "three", "four", "five", "six", "seven",
                "eight", "nine", "ten", "eleven", "twelve", "thirteen",
                "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
                "nineteen", "twenty", 30 => "thirty", 40 => "forty",
                50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
                90 => "ninety");


            if (!is_numeric($x))
                $w = '#';
            else if (fmod($x, 1) != 0)
                $w = '#';
            else {
                if ($x < 0) {
                    $w = 'minus ';
                    $x = -$x;
                }
                else
                    $w = '';
                // ... now $x is a non-negative integer.

                if ($x < 21)   // 0 to 20
                    $w .= $nwords[$x];
                else if ($x < 100) {   // 21 to 99
                    $w .= $nwords[10 * floor($x / 10)];
                    $r = fmod($x, 10);
                    if ($r > 0)
                        $w .= ' ' . $nwords[$r];
                }
                else if ($x < 1000) {   // 100 to 999
                    $w .= $nwords[floor($x / 100)] . ' hundred';
                    $r = fmod($x, 100);
                    if ($r > 0) {
                        $w .= ' and ';
                        if ($r < 100) {   // 21 to 99
                            $w .= $nwords[10 * floor($r / 10)];
                            $r = fmod($r, 10);
                            if ($r > 0)
                                $w .= ' ' . $nwords[$r];
                        }
                    }
                }
                else if ($x < 1000000) {   // 1000 to 999999
                    $w .= $nwords[floor($x / 1000)] . ' thousand';
                    $r = fmod($x, 1000);
                    if ($r > 0) {
                        $w .= ' ';
                        if ($r < 100)
                            $w .= ' and ';
                        $w .= $nwords[$r];
                    }
                }
                else {    //  millions
                    $w .= stringNumbers(floor($x / 1000000)) . ' million';
                    $r = fmod($x, 1000000);
                    if ($r > 0) {
                        $w .= ' ';
                        if ($r < 100)
                            $w .= ' and ';
                        $w .= int_to_words($r);
                    }
                }
            }
            //echo $w;

            return $w;


        }
}

if(!function_exists('phone')) {
    function phone($phone)
        {
            if (strstr($phone, '+')) {
                $phone = substr($phone, 1);
                $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
                return $mobile;
            }
            else {
                $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
                return $mobile;
            }
        }
}

if(!function_exists('phone2')) {
    function phone2($phone)
        {
            if (strstr($phone, '+')) {
//                776716138

                $phone = substr($phone, 1);
                $mobile = strlen($phone) == 10 ? '256' . substr($phone,1) : $phone;
                return $mobile;
            }
            else {
                $mobile = strlen($phone) == 9 ? '256' . $phone : $phone;
                return $mobile;
            }
        }
}


if (!function_exists('trending_date')) {
    function trending_date($date)
        {
            return date('Y-m-d') == date('Y-m-d', $date) ? date('H:i:s', $date) : date('d-m-Y', $date);
        }
}
if (!function_exists('trending_date_time')) {
    function trending_date_time($date)
        {
            return date('Y-m-d') == date('Y-m-d', $date) ? date('H:i:s', $date) : date('d-m-Y H:i', $date).'hrs';
        }
}

if(!function_exists('code')){
      function code($length = 10)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
}

if(!function_exists('hashValue')) {
    function hashValue($v)
        {
            return sha1(md5($v));
        }
}

if (!function_exists('readJsonFgets')) {
    function readJsonFgets(){
        $data = json_decode(file_get_contents('php://input'), true);
        return $data;
    }
}
if (!function_exists('readJson')) {

    function readJson($obj, $type = 'url')
        {
            if ($type == 'url') {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL, $obj);
                $result = curl_exec($ch);
                curl_close($ch);
            }
            else {
                $result = $obj;
            }

            return $obj = json_decode($result);

        }
}
