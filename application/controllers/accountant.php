<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class accountant extends CI_Controller
{

    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";

    public $notification = array();

    public $view_data = array();

    function __construct()
    {
        parent::__construct();

        $this->isloggedin() == true ? '' : $this->logout();
        $this->page_level = $this->uri->slash_segment(1);
        $this->page_level2 = $this->uri->slash_segment(2);
        // $this->load->library('cart');

    }

    public function isloggedin()
    {
        return $this->session->userdata('user_type') == 4 || $this->session->userdata('user_type') == 1 ? true : false;

    }

    public function logout()
    {

        $this->session->sess_destroy();
        redirect('home/login', 'refresh');
        //	echo '<META http-equiv=refresh content=0;URL='.base_url().'index.php/login>';
    }

    public function index()
    {
        $this->dashboard();
    }

    public function dashboard()
    {
        $page_level = $this->uri->slash_segment(1);

        $data = array(
            'title' => 'dashboard',
            'subtitle' => 'Welcome',
            'link_details' => 'Account overview',
            'page_level' => $page_level
        );


        $this->form_validation->set_rules('filter', 'Filter', 'xss_clean|trim');
        if ($this->form_validation->run() == true) {

            $days = $this->input->post('filter');

            switch ($days) {
                case 1:
                    $data['fdate'] = date('Y-m-01');
                    break;

                case 2:
                    $day = date('Y-m-d');
                    $date = date("Y-m-d", strtotime($day . " -1 month "));
                    $data['fdate'] = $date;
                    break;

                case 3:
                    //this is the reperesentation for the last 60 days
                    $day = date('Y-m-d');
                    $date = date("Y-m-d", strtotime($day . " -2 month "));
                    $data['fdate'] = $date;
                    break;

                case 4:
                    //this is the representation for the last 90 days
                    $day = date('Y-m-d');
                    $date = date("Y-m-d", strtotime($day . " -3 month "));
                    $data['fdate'] = $date;

                    break;
                case 5:
                    //this is a repesentation for the last month

                    $day = date('Y-m');

                    $date = date("Y-m-01", strtotime($day . " last month "));
                    $data['last_day'] = date('Y-m-d', strtotime(' last day of this month', strtotime($date)));
                    $data['fdate'] = $date;
                    break;
                case 6:

                    //this is a representation for last year
                    $day = date('Y');
                    $date = date("Y-01-01", strtotime($day . " last year "));
                    $data['last_day'] = date('Y-m-d', strtotime(' last day of December', strtotime($date)));
                    $data['fdate'] = $date;

                    break;

            }

        } else {
            $data['fdate'] = date("Y-m-d", strtotime(date('Y-m-d') . " -1 month "));
        }

        $this->load->view($page_level . 'header', $data);
        $this->load->view($page_level . 'dashboard/dashboard2', $data);
        //$this->load->view($page_level.'dashboard/dashboard',$data);
        $this->load->view($page_level . 'footer', $data);

    }


    // this is the function for the users

    function users_ajax()
    {
        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => 'users',
            'link_details' => 'Account overview',

        );
        $this->load->view('ajax/users_ajax', $data);
    }

//function for adding logs

    function logs($type = 'all', $start = 0)
    {
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level

        );
        $this->load->view($page_level . 'header', $data);
        ///////////////////////// this is the begining of the pagination
        $data['number'] = $number = $this->db->count_all('logs');
        $config['uri_segment'] = 4;
        $config['display_pages'] = FALSE;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['base_url'] = base_url('index.php/' . $this->page_level . '/logs/' . $type);
        $config['total_rows'] = $number;
        $data['per_page'] = $per_page = $config['per_page'] = 15;
        $this->pagination->initialize($config);
        $data['pg'] = $this->pagination->create_links();
        /////////////////////////this is the end of the paginition//////////////////////////

        $this->db->select()->from('logs');
        $type == 'all' ? '' : $this->db->where('transaction_type', $type);
        //// this is when the filter is applied//////////////////
        $this->form_validation->set_rules('from', 'From', 'xss_clean|trim')->set_rules('to', 'To', 'xss_clean|trim');
        if ($this->form_validation->run() == true) {
            $from = strtotime(date($this->input->post('from')));
            $to = strtotime(date($this->input->post('to').' 23:59:59'));
            $this->db->where(array('created_on >=' => $from, 'created_on <=' => $to));
        }
        ///////////////this is the end of the filter//////////////
        $rows = $data['tlogs'] = $this->db->order_by('id', 'desc')->limit($per_page, $start)->get()->result();
        $data['rows'] = count($rows);
        $this->load->view('admin/logs/logs', $data);
        $this->load->view($page_level . 'footer_table', $data);

    }


    //this is the function which calculates the commission


    public function transaction_id($type = 'O')
    {
        $branch_code = $this->db->select('branch_code')->from('branch')->where(array('id' => $this->session->userdata('branch_id')))->get()->row();

        $token = strtoupper($this->session->userdata('country') . $branch_code->branch_code . date('ymd') . $type . $this->GetSMSCode(3));

        while (1) {
            if ($this->db->select('id')->from('transactions')->where(array('transaction_id' => $token))->get()->num_rows() == 0) {
                break;
            } else {
                $token = strtoupper($this->session->userdata('country') . $branch_code->branch_code . date('ymd') . $type . $this->GetSMSCode(3));
            }

        }

        return $token;
    }
    //getting enum values
    function get_enum_values( $table, $field )
        {
            $type = $this->db->query( "SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'" )->row( 0 )->Type;
            preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
            $enum = explode("','", $matches[1]);
            return $enum;
        }
    //getting enum values
    public function secret_code()
    {


        $token = $this->code(10);

        while (1) {
            if ($this->db->select('id')->from('transactions')->where(array('secret_code' => $token))->get()->num_rows() == 0) {
                break;
            } else {
                $token = $this->code(10);
            }

        }

        return strtoupper($token);
    }

    public function forex_rate($from_currency, $convert_currency)
    {
        if ($from_currency == $convert_currency) {
            return 1;
        } elseif ($from_currency != $convert_currency) {
            $forex = $this->db->select('to_amount')->from('forex_settings')
                ->where(array(
                    'country' => $this->session->userdata('country'),
                    'from_currency' => $from_currency,
                    'to_currency' => $convert_currency
                ))->get()->row();
            return isset($forex->to_amount) ? $forex->to_amount : 1;
        }

    }


    public function GetSMSCode($length)
    {

        $codes = array();
        $chars = "0123456789";
        srand((double)microtime() * 1000000);
        $i = 0;
        $code = '';
        $serial = '';

        $i = 0;

        while ($i < $length) {
            $num = rand() % 10;
            $tmp = substr($chars, $num, 1);
            $serial = $serial . $tmp;
            $i++;
        }

        return $serial;

    }

    public function hashValue($v)
    {
        return sha1(md5($v));
    }

    // this is the function which initialises the  extract function

    public function commission($amount)
    {

        $commission = $this->db->select('percentage')->from('commission')
            ->where(array('country' => $this->session->userdata('country'), 'from_amount >=' => $amount))
            ->get()->row();
        return isset($commission->percentage) ? $amount * $commission->percentage : 0;


    }





    //this is the function orders

    function users($type = null, $id = null)
        {

            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'link_details' => 'Account overview',


            );
            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;
            $this->load->view($page_level . 'header', $data);

            //this is the default qry to be loaded
            $this->db->select()->from('users');
            $this->db->where(array('id !=' => $this->session->userdata('id'),'user_type !='=>5));
            $this->session->userdata('user_type') == '2' ? $this->db->where(array('user_type' => 5)) : '';
            $data['t'] = $t = $this->db->order_by('id', 'desc')->get()->result();
            // this is the end of query loaded


            switch ($type) {

                case 'new':
                    //this is the section for the form validation
                    $this->form_validation->set_rules('full_name', 'full_name', 'xss_clean|trim|required')
                        ->set_rules('email', 'Email Address', 'xss_clean|trim|required|is_unique[users.email]')
                        ->set_rules('zip_code', 'Zip Code', 'xss_clean|trim|required')
                        ->set_rules('phone', 'Phone', 'xss_clean|trim|required|is_unique[users.phone]|max_length[15]')
                        ->set_rules('gender', 'Gender', 'xss_clean|trim|required|exact_length[1]')
                        // ->set_rules('country','Country','xss_clean|trim|required')
                        ->set_rules('access', 'Access', 'xss_clean|trim')
                        ->set_rules('branch', 'Branch', 'xss_clean|trim|required')
                        ->set_rules('role', 'User Role', 'xss_clean|trim|n_list[1,2,3,4]')
                        ->set_rules('dob', 'Date of Birth', 'xss_clean|trim');
                    if ($this->form_validation->run() == true) {
//id, full_name, city, password, username,country, phone, email, gender, photo, user_type, sub_type, status, verified, created_on, created_by, updated_on, updated_by,
                        $bran = $this->db->select('a.id,branch_name,a.country,a.state')->from('branch a')->where('a.id', $this->input->post('branch'))->get()->row();

                        $values = array(
                            'full_name' => $name = $this->input->post('full_name'),
                            'email' => $this->input->post('email'),
                            'username' => $this->input->post('email'),
                            'password' => $this->hashValue('123'),
                            'city' => $bran->state,
                            'country' => $bran->country,
                            'phone' => $this->phone($this->input->post('phone')),
                            'zip_code' => $this->input->post('zip_code'),
                            'gender' => $this->input->post('gender'),
                            'user_type' => $this->input->post('role'),
                            'access' => $this->input->post('access'),
                            'dob' => strtotime($this->input->post('dob')),
                            'branch_id' => strlen($this->input->post('branch')) > 0 ? $this->input->post('branch') : $this->session->userdata('branch_id'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')

                        );
                        $this->db->insert('users', $values);
                        $data['message'] = 'Record has added successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        //this Lists the users in the system
                        $data['t'] = $t;
                        $this->load->view($root . 'users', $data);
                        $this->add_logs('account_creation', $name, 'Sender account has been created');

                    }
                    else {
                        $data['t'] = $t;
                        $this->load->view($root . 'new', $data);
                    }

                    break;
                case 'edit':
                    $id = $data['id'] = $id / date('Y');
                    //this is the section for the form validation
                    $this->form_validation->set_rules('email', 'Email', 'valid_email|xss_clean|trim')
                        ->set_rules('phone', 'Phone', 'xss_clean|trim|max_length[15]|min_length[10]')
                        ->set_rules('fullname', 'Full Name', 'xss_clean|trim')
                        ->set_rules('role', 'Role', 'xss_clean|trim')
                        ->set_rules('access', 'Access', 'xss_clean|trim')
                        ->set_rules('branch', 'Branch', 'xss_clean|trim|required');
                    if ($this->form_validation->run() == false) {

                        $this->load->view($root . 'user_profile', $data);
                    }
                    else {
                        $bran = $this->db->select('a.id,branch_name,a.country,a.state')->from('branch a')->where('a.id', $this->input->post('branch'))->get()->row();
                        $this->db->where('id', $id)
                            ->update('users', array(
                                'email' => $this->input->post('email'),
                                'phone' => $this->input->post('phone'),
                                'user_type' => $this->input->post('role'),
                                'access' => $this->input->post('access'),
                                'full_name' => $this->input->post('fullname'),
                                'branch_id' => $this->input->post('branch'),
                                'city' => $bran->state,
                                'country' => $bran->country

                            ));

                        $data['message'] = 'User account is updated Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        $this->load->view($root . 'user_profile', $data);
                        $this->add_logs('account_modification', $this->input->post('fullname'), 'Sender account has been Updated');

                    }
                    break;
                //this is the part for changing avatar
                case 'change_avatar':
                    $id = $data['id'] = $id / date('Y');

                    // this is the function which uploads the profile image
                    $this->form_validation->set_rules('image', 'Image', 'xss_clean|trim')->set_rules('pa', 'Pas', 'xss_clean|trim');
                    //this is the company name

                    $cname = 'users_profile';
                    //managing of the images
                    $path = $config['upload_path'] = './uploads/profile/' . underscore($cname) . '/';
                    $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                    $config['max_size'] = '200';
                    $config['max_width'] = '1920';
                    $config['max_height'] = '850';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }
                    if ($this->form_validation->run() == true && $this->upload->do_upload('image') == true) {

                        $values = array(
                            'photo' => $path . $this->upload->file_name,
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id')
                        );
                        $this->db->where('id', $id)->update('users', $values);
                        // $this->session->set_userdata(array('photo'=>$path.$this->upload->file_name));
                        $data['message'] = 'Image has been updated Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);

                    }
                    else {
                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($root . 'user_profile', $data);

                    }

                    break;
                //this is the part for changing the password
                case 'change_password':
                    $id = $data['id'] = $id / date('Y');
                    $this->form_validation->set_rules('new_pass', 'New Password', 'required|trim|xss_clean|matches[rpt_pass]')->set_rules('rpt_pass', 'Repeat Password', 'required|xss_clean|trim');
                    if ($this->form_validation->run() == false) {

                        $this->load->view($root . 'user_profile', $data);
                    }
                    else {
                        $this->db->where('id', $id)->update('users', array('password' => $this->hashValue($this->input->post('new_pass'))));
                        $data['message'] = 'Password has been changed successfully <br/>';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        $this->load->view($root . 'user_profile', $data);
                        $fn = $this->db->select('full_name')->from('users')->where('id', $id)->get()->row();
                        $this->add_logs('password_change', $fn->full_name, 'Sender\'s  account password changed');
                    }

                    break;


                //deleting user
                case 'delete':
                    $id = $data['id'] = $id / date('Y');
                    $this->delete($id, 'users');
                    $data['message'] = 'Record has been Deleted successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    //this is where the default page i loaded
                    $data['t'] = $t;
                    $this->load->view($root . 'users', $data);
                    break;


                case 'make_admin':
                    $id = $data['id'] = $id / date('Y');
                    $this->db->where('id', $id)->update('users',
                        array('user_type' => '1',
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id')));
                    $data['message'] = 'Record has been Updated successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    //this is where the default page i loaded
                    $data['t'] = $t;
                    $this->load->view($root . 'users', $data);
                    break;

                case 'ban':
                    $id = $data['id'] = $id / date('Y');
                    $this->db->where('id', $id)->update('users',
                        array('status' => '2',
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id')));
                    $data['message'] = 'Record has been Blocked from accessing the System successfully';
                    $data['alert'] = 'warning';
                    $this->load->view('alert', $data);
                    //this is where the default page i loaded
                    $data['t'] = $t;
                    $this->load->view($root . 'users', $data);
                    break;

                case 'unblock':
                    $id = $data['id'] = $id / date('Y');
                    $this->db->where('id', $id)->update('users',
                        array('status' => '0',
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id')));
                    $data['message'] = 'Record has been unblocked from accessing the System successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    //this is where the default page i loaded
                    $data['t'] = $t;
                    $this->load->view($root . 'users', $data);
                    break;

                case 'filter':

                    $this->form_validation
                        ->set_rules('country', 'Country', 'xss_clean|trim')
                        ->set_rules('role', 'Role', 'xss_clean|trim');

                    if ($this->form_validation->run() == true) {
                        $this->db->select()->from('users');
                        //this is selecting for the date
                        strlen($this->input->post('country')) > 0 ? $this->db->where(array('country' => $this->input->post('country'))) : '';
                        strlen($this->input->post('role')) > 0 ? $this->db->where(array('user_type' => $this->input->post('role'))) : '';
                        $this->db->where(array('id !=' => $this->session->userdata('id')));
                        $data['t'] = $this->db->get()->result();
                        $this->load->view($root . 'users', $data);
                    }
                    else {
                        $this->load->view($root . 'users_ajax');
                    }


                    break;

                default :

                    // $data['t'] = $t;
                    $this->load->view($root . 'users',$data);
//                    $this->load->view($root . 'users_ajax');

                    break;


            }

            $this->load->view($page_level . 'footer_table', $data);

        }


    function greater_than_today($str)
    {


        $selected_date = strtotime($str);
        $today =time();

        if (($selected_date < $today) ) {
            $this->form_validation->set_message('greater_than_today',  '<strong> %s</strong> Must be greater than today');
            return false;
        } else {
            return true;
        }

    }



    function add_logs($trans_type, $target = '', $desc = '')
    {
        $this->load->library('user_agent');
        $user = $this->session->userdata('username');

        switch ($trans_type) {
            case 'transfer':
            case 'cashout':
            case 'hold':
            case 'release':
            case 'approve':
            case 'canceled':
                // all logs related to Transactions
                $details = "TxnID - $target ($desc)";
                break;

            case 'account_modification':
            case'account_deletion':
            case'account_creation':
            case'password_change':

                //So Any Logs related to An account, the Target should be the Account being affect...
                $details = "$user Performed " . humanize($trans_type) . " - $target ($desc)";

                break;

        }

        $values = array(
            'transaction_type' => $trans_type,
            'target' => $target,
            'details' => $details,
            'created_by' => $user,
            'created_on' => time(),
            'platform' => $this->agent->platform(),
            'browser' => $this->agent->browser() . '-' . $this->agent->version(),
            'agent_string' => $this->agent->agent_string(),
            'ip' => $this->session->userdata('ip_address'),
            'agent_referal' => $this->agent->is_referral() ? $this->agent->referrer() : '',
        );
        $this->db->insert('logs', $values);
    }



    //this is the function which deletes data

    public function approve_transactions($type = null, $id = null)
    {
        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',


        );
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $this->load->view($page_level . 'header', $data);

        switch ($type) {
            case 'approve':
                $id = $data['id'] = $id / date('Y');
                $values = array(
                    'status' => 'pending',
                    'approved_by' => $this->session->userdata('id'),
                    'approved_on' => time()

                );
                $this->db->where('id', $id)->update('transactions', $values);


                $data['message'] = 'Transaction has been approved successfully';
                $data['alert'] = 'success';
                $this->load->view('alert', $data);


                $this->load->view($root . 'approve_transactions', $data);
                break;

            case 'hold':
                $this->form_validation->set_rules('reason', 'Hold Reason', 'xss_clean|trim|required');
                if ($this->form_validation->run() == true) {
                    $id = $data['id'] = $id / date('Y');
                    $values = array(
                        'status' => 'hold',
                        'hold_reason' => $this->input->post('reason'),
                        'approved_by' => $this->session->userdata('id'),
                        'approved_on' => time()

                    );
                    $this->db->where('id', $id)->update('transactions', $values);

                    $data['message'] = 'Transaction has been put onhold successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                } else {

                    $this->load->view($root . 'hold_reason', $data);
                }
                $this->load->view($root . 'approve_transactions', $data);

                break;
            case 'remove_hold':


                $id = $data['id'] = $id / date('Y');
                $amount = $this->db->select('sent_amount_usd')->from('transactions')->where('id', $id)->get()->row();
                $status = $amount->sent_amount_usd >= 5000 ? 'not_approved' : 'pending';
                $values = array(
                    'status' => $status,
                    // 'hold_reason'=>$this->input->post('reason'),
                    'approved_by' => $this->session->userdata('id'),
                    'approved_on' => time()

                );
                $this->db->where('id', $id)->update('transactions', $values);

                $data['message'] = 'Transaction has been released for transfer successfully';
                $data['alert'] = 'success';
                $this->load->view('alert', $data);


                $this->db->select()->from('transactions');
                $this->session->userdata('user_type') == '1' ? $this->db->where(array('sender_country' => $this->session->userdata('country'))) : '';
                $this->session->userdata('user_type') == '1' ? '' : $this->db->where(array('receiver_country' => $this->session->userdata('country')));
                //$this->db->where(array('created_by'=>$this->session->userdata('id')));
                $data['t'] = $this->db->order_by('id', 'desc')->get()->result();


                $this->load->view($root . 'transfers', $data);

                break;

            default:
                $this->load->view($root . 'approve_transactions', $data);
                break;
        }


        $this->load->view($page_level . 'footer_table', $data);

    }

    // this is the function for the users

    public function orders($type = null)
    {
        $data['page_level'] = $page_level = $this->uri->slash_segment(1);
        $data = array(
            'title' => 'orders',
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level

        );

        $this->load->view($page_level . 'header', $data);

        switch ($type) {
            case 'edit_product':
                $this->load->view($page_level . 'orders/new_product', $data);
                break;
            default:
                $this->load->view($page_level . 'orders/orders');
                break;
        }

        $this->load->view($page_level . 'footer_orders', $data);
        $this->load->view('ajax/orders');

    }

    // this is the function for the users

    function orders_table()
    {

        /*
* Paging
*/

        //var_dump($_POST["selected"]);


        $iTotalRecords = $this->db->count_all('orders_table');
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $status_list = array(
            array("success" => "Pending"),
            array("info" => "Closed"),
            array("danger" => "On Hold"),
            array("warning" => "Fraud")
        );
        $id = 1;
        foreach ($this->db->select()->from('orders_table')->limit($end)->get()->result() as $or) {
            switch ($or->status) {
                case 'Pending':
                    $status = $status_list[0];
                    break;
                case 'Closed':
                    $status = $status_list[1];
                    break;
                case 'On Hold':
                    $status = $status_list[2];
                    break;
                case 'Fraud':
                    $status = $status_list[3];
                    break;
                default:
                    $status = $status_list[0];
                    break;
            }

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="' . $or->id . '">',
                $id,
                $or->purchased_on,
                $or->customer,
                $or->ship_to,
                $or->base_price,
                $or->purchase_price,
                '<span class="label label-sm label-' . (key($status)) . '">' . (current($status)) . '</span>',
                '<a href="' . base_url('index.php/admin/orders/edit_product/' . $or->id) . '" class="btn btn-sm  btn-default btn-editable"><i class="fa fa-search"></i> View</a>',
            );
            $id++;
        }


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


    // this is the function for the users

    function delete($id, $table)
    {
        $this->db->where('id', $id);
        $this->db->delete($table);
    }

    // this is the function for the reports

    public function approvals($type = null)
    {

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',


        );
        $page_level = $this->page_level;
        $this->load->view($page_level . 'header', $data);

        switch ($type) {
            case 'reports':
                $this->load->view($page_level . 'reports/reports', $data);
                break;
            case 'issues':
                $this->load->view($page_level . 'products/shipping', $data);
                break;
            default:
                $this->load->view($page_level . 'users/users', $data);
                break;
        }

        $this->load->view($page_level . 'footer_table', $data);

    }

    public function statement($type = null)
    {

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',


        );
        $page_level = $this->page_level;
        $this->load->view($page_level . 'header', $data);

        switch ($type) {
            case 'reports':
                $this->load->view($page_level . 'reports/reports', $data);
                break;
            case 'issues':
                $this->load->view($page_level . 'products/shipping', $data);
                break;
            default:
                $this->load->view($page_level . 'users/users', $data);
                break;
        }

        $this->load->view($page_level . 'footer_table', $data);

    }

    public function notifications($type = null)
    {

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',


        );
        $page_level = $this->page_level;
        $this->load->view($page_level . 'header', $data);

        switch ($type) {
            case 'reports':
                $this->load->view($page_level . 'reports/reports', $data);
                break;
            case 'issues':
                $this->load->view($page_level . 'products/shipping', $data);
                break;
            default:
                $this->load->view($page_level . 'users/users', $data);
                break;
        }

        $this->load->view($page_level . 'footer_table', $data);

    }



    // this is the function for the users
    function transfer($type = null, $id = null)
    {


        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type

        );
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $this->load->view($page_level . 'header', $data);

        $data['status'] = $this->get_enum_values('transactions', 'status');

        switch ($type) {

            default:

                $this->load->view($root . 'transfers', $data);

                break;

            case 'new':
                isset($id) ? $data['id'] = $id / date('Y') : '';

                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('amount', 'Amount', 'xss_clean|trim|required')
                    ->set_rules('receiver_email', 'Email Address', 'xss_clean|trim')
                    ->set_rules('receiver_name', 'Receiver Name', 'xss_clean|trim|required')
                    ->set_rules('receiver_phone', 'Phone', 'xss_clean|trim|required|max_length[10]')
                    ->set_rules('receiver_zip_code', 'Sender Zip Code', 'xss_clean|trim')
                    ->set_rules('country', 'Country', 'xss_clean|trim|required')
                    ->set_rules('commission', 'Commission', 'xss_clean|trim')
                    ->set_rules('branch', 'Receiver Branch', 'xss_clean|trim|required')
                    ->set_rules('currency', 'Currency', 'xss_clean|trim|required')
                    ->set_rules('convert_currency', 'Converted Currency', 'xss_clean|trim|required')
                    ->set_rules('user_id', 'User ID', 'xss_clean|trim')
                    ->set_rules('sender_name', 'Sender name', 'xss_clean|trim')
                    ->set_rules('sender_email', 'Sender Email', 'xss_clean|trim')
                    ->set_rules('sender_phone', 'Sender Phone', 'xss_clean|trim')
                    ->set_rules('sender_zip_code', 'Sender Zip Code', 'xss_clean|trim')
                    ->set_rules('sender_branch', 'Sender Branch', 'xss_clean|trim')
                    ->set_rules('other_charges', 'Other Charges', 'xss_clean|trim')
                    ->set_rules('commission_computed', 'Commission Computed', 'xss_clean|trim')
                    ->set_rules('net_amount', 'Net Amount', 'xss_clean|trim');

                if ($this->form_validation->run() == true) {
//
//id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, id, id

                    if (strlen($this->input->post('user_id')) > 0) {

                        //this is where commission is calculated from
                        $cc = $this->input->post('commission_computed');
                        $commission_usd = strlen($cc) > 0 ? $this->input->post('commission_computed') : $this->input->post('amount') - $this->input->post('net_amount');
                        $commission = $this->forex_rate('USD', $this->input->post('currency')) * $commission_usd;


                        //this where we get the current conversation for the specifed currency
                        $forex_rate = $this->forex_rate($this->input->post('currency'), $this->input->post('convert_currency'));

                        // this is where original amount is converted to usd
                        $in_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $this->input->post('amount');


                        //this is the commission ,amount and other charges in usd
                        $before_convert = ($this->input->post('amount') - $commission - $this->input->post('other_charges'));
                        $received_amount = $before_convert * $forex_rate;

                        $received_amount_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $before_convert;


                        $other_charges_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $this->input->post('other_charges');


                        //checking for the amount above 5000 usd
                        $above_5000 = ($in_usd) > 5000 ? 'not_approved' : 'pending';


                        // this is the one which adds the expiry date
                        $expiry_date = date('Y-m-d', strtotime('+1 years'));


                        $values = array(
                            'sent_amount' => $this->input->post('amount'),
                            'sender_id' => $this->input->post('user_id'),
                            'expiry_date' => strtotime($expiry_date),
                            'commission' => $commission,
                            'forex_rate' => $forex_rate,
                            'currency' => $this->input->post('currency'),
                            'recipient_currency' => $this->input->post('convert_currency'),
                            'received_amount' => $received_amount,
                            'transaction_id' => $txn_id = $this->transaction_id(),
                            'secret_code' => $this->secret_code(),
                            'sender_country' => $this->session->userdata('country'),
                            'receiver_country' => $this->input->post('country'),
                            'receiver_phone' => $this->input->post('receiver_phone'),
                            'receiver_branch' => $receiver_branch=$this->input->post('branch'),
                            'receiver_name' => $receiver = $this->input->post('receiver_name'),
                            'receiver_email' => $this->input->post('receiver_email'),
                            'transfer_type' => 'teller',
                            'other_charges' => $this->input->post('other_charges'),
                            'branch_id' => $branch_id = $this->session->userdata('branch_id'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id'),
                            'status' => $above_5000,
                            'sent_amount_usd' => $in_usd,
                            //this is when all charges are converted to usd
                            'received_amount_usd' => $received_amount_usd,
                            'commission_usd' => $commission_usd,
                            'other_charges_usd' => $other_charges_usd

                        );








                        if ($this->site_options->title('send_to_branch_without_money') == 'no') {


                            if ($this->custom_library->branch_balance($receiver_branch) > $received_amount_usd) {


                                $this->db->insert('transactions', $values);

                                $sn = $this->db->select('full_name')->from('users')->where('id', $this->input->post('user_id'))->get()->row();

                                //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                $this->custom_library->update_statement($branch_id, $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                $this->custom_library->update_teller_statement($this->session->userdata('id'), $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                //this is the registering of the logs

                                $this->add_logs('transfer', $txn_id, $sn->full_name . ' Sent money to ' . $receiver);
                                $r_id = $this->db->select('id')->from('transactions')->where('transaction_id', $txn_id)->get()->row();

                                isset($r_id->id) ? redirect($this->page_level . 'receipt/sender/' . $r_id->id * date('Y') . '/s') : '';


                            }
                            else {

                                $data['alert'] = 'danger';
                                $data['message'] = 'Transaction cannot be made due to insufficient funds at recipient branch';                                           $this->load->view('alert', $data);

                                $this->load->view($root . 'new', $data);
                            }


                        }
                        else {

                            $this->db->insert('transactions', $values);

                            $sn = $this->db->select('full_name')->from('users')->where('id', $this->input->post('user_id'))->get()->row();

                            //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                            $this->custom_library->update_statement($branch_id, $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver);

                            //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                            $this->custom_library->update_teller_statement($this->session->userdata('id'), $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                            //this is the registering of the logs

                            $this->add_logs('transfer', $txn_id, $sn->full_name . ' Sent money to ' . $receiver);
                            $r_id = $this->db->select('id')->from('transactions')->where('transaction_id', $txn_id)->get()->row();

                            isset($r_id->id) ? redirect($this->page_level . 'receipt/sender/' . $r_id->id * date('Y') . '/s') : '';


                        }



                    }
                    elseif (strlen($this->input->post('user_id')) == 0) {

                        $user_id = $this->db->select('id')->from('users')->where(array('username' => $this->input->post('sender_email'), 'password' => $this->hashValue('123'), 'full_name' => $this->input->post('sender_name')))->get()->row();


                        if (!isset($user_id->id)) {

                            $sender_details = array(
                                'full_name' => $this->input->post('sender_name'),
                                'email' => $this->input->post('sender_email'),
                                'username' => $this->input->post('sender_email'),
                                'phone' => $this->input->post('sender_phone'),
                                'zip_code' => $this->input->post('sender_zip_code'),
                                'branch_id' => $this->input->post('sender_branch'),
                                'country' => $this->session->userdata('country'),
                                'password' => $this->hashValue('123'),
                                'branch_id' => $this->session->userdata('branch_id'),
                                'city' => $this->session->userdata('city'),
                                'user_type' => '5',
                                'created_on' => time(),
                                'created_by' => $this->session->userdata('id')

                            );
                            //inserting users into database
                            $this->db->insert('users', $sender_details);

                            //this is the adding of the account creation
                            $this->add_logs('account_creation', $this->input->post('sender_name'), 'Sender account has been created');

                            ///selecting inserted User

                            $user_id = $this->db->select('id,full_name')->from('users')->where(array('username' => $this->input->post('sender_email'), 'password' => $this->hashValue('123'), 'full_name' => $this->input->post('sender_name')))->get()->row();


                            //if the user is found successfully go on and make a transaction
                            if (isset($user_id->id)) {


                                $cc = $this->input->post('commission_computed');
                                $commission_usd = strlen($cc) > 0 ? $this->input->post('commission_computed') : $this->input->post('net_amount') - $this->input->post('commission_computed');
                                $commission = $this->forex_rate('USD', $this->input->post('currency')) * $commission_usd;

                                //this where we get the current conversation for the specifed currency
                                $forex_rate = $this->forex_rate($this->input->post('currency'), $this->input->post('convert_currency'));

                                // this is where original amount is converted to usd
                                $in_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $this->input->post('amount');


                                //checking for the amount above 5000 usd
                                $above_5000 = ($in_usd) > 5000 ? 'not_approved' : 'pending';


                                //this is the commission ,amount and other charges in usd
                                $before_convert = ($this->input->post('amount') - $commission - $this->input->post('other_charges'));
                                $received_amount = $before_convert * $forex_rate;

                                $received_amount_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $before_convert;


                                $other_charges_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $this->input->post('other_charges');


                                // this is the one which adds the expiry date
                                $expiry_date = date('Y-m-d', strtotime('+1 years'));


                                $values = array(
                                    'sent_amount' => $this->input->post('amount'),
                                    'sender_id' => $user_id->id,

                                    'expiry_date' => strtotime($expiry_date),
                                    'commission' => $commission,
                                    'forex_rate' => $forex_rate,
                                    'currency' => $this->input->post('currency'),
                                    'recipient_currency' => $this->input->post('convert_currency'),
                                    'received_amount' => $received_amount,
                                    'transaction_id' => $txn_id = $this->transaction_id(),
                                    'secret_code' => $this->secret_code(),
                                    'sender_country' => $this->session->userdata('country'),
                                    'receiver_country' => $this->input->post('country'),
                                    'receiver_phone' => $this->input->post('receiver_phone'),
                                    'receiver_branch' => $receiver_branch=$this->input->post('branch'),
                                    'receiver_name' => $receiver = $this->input->post('receiver_name'),
                                    'receiver_email' => $this->input->post('receiver_email'),
                                    'transfer_type' => 'teller',
                                    'other_charges' => $this->input->post('other_charges'),
                                    'branch_id' => $branch_id = $this->session->userdata('branch_id'),
                                    'created_on' => time(),
                                    'created_by' => $this->session->userdata('id'),
                                    'status' => $above_5000,
                                    'sent_amount_usd' => $in_usd,

                                    //this is when all charges are converted to usd
                                    'received_amount_usd' => $received_amount_usd,
                                    'commission_usd' => $commission_usd,
                                    'other_charges_usd' => $other_charges_usd


                                );




                                if ($this->site_options->title('send_to_branch_without_money') == 'no') {


                                    if ($this->custom_library->branch_balance($receiver_branch) > $received_amount_usd) {


                                        $this->db->insert('transactions', $values);


                                        $sn = $this->db->select('full_name')->from('users')->where('id', $this->input->post('user_id'))->get()->row();

                                        //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                        $this->custom_library->update_statement($branch_id, $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                        //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                        $this->custom_library->update_teller_statement($this->session->userdata('id'), $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                        //this is the registering of the logs

                                        $this->add_logs('transfer', $txn_id, $user_id->full_name . ' Sent money to ' . $receiver);

                                        $this->add_logs('transfer', $receiver, 'Initiation of the transaction');
                                        //redirecting to the receipt
                                        $r_id = $this->db->select('id')->from('transactions')->where('transaction_id', $txn_id)->get()->row();
                                        isset($r_id->id) ? redirect($this->page_level . 'receipt/sender/' . $r_id->id * date('Y') . '/s') : '';



                                    }
                                    else {

                                        $data['alert'] = 'danger';
                                        $data['message'] = 'Transaction cannot be made due to insufficient funds at recipient branch';                                     $this->load->view('alert', $data);
                                        $this->load->view($root . 'new', $data);
                                    }


                                }
                                else {

                                    $this->db->insert('transactions', $values);


                                    $sn = $this->db->select('full_name')->from('users')->where('id', $this->input->post('user_id'))->get()->row();

                                    //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                    $this->custom_library->update_statement($branch_id, $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                    //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                    $this->custom_library->update_teller_statement($this->session->userdata('id'), $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                    //this is the registering of the logs

                                    $this->add_logs('transfer', $txn_id, $user_id->full_name . ' Sent money to ' . $receiver);

                                    $this->add_logs('transfer', $receiver, 'Initiation of the transaction');
                                    //redirecting to the receipt
                                    $r_id = $this->db->select('id')->from('transactions')->where('transaction_id', $txn_id)->get()->row();
                                    isset($r_id->id) ? redirect($this->page_level . 'receipt/sender/' . $r_id->id * date('Y') . '/s') : '';



                                }

                            }
                            else {
                                $data['message'] = 'Transaction has Failed';
                                $data['alert'] = 'danger';
                                $this->load->view('alert', $data);
                            }
                        }
                        else {


                            if (isset($user_id->id)) {

                                $user = $user_id->id;

                                //this is where commission is calculated from
                                $cc = $this->input->post('commission_computed');
                                $commission_usd = strlen($cc) > 0 ? $this->input->post('commission_computed') : $this->input->post('amount') - $this->input->post('net_amount');
                                $commission = $this->forex_rate('USD', $this->input->post('currency')) * $commission_usd;

                                //this where we get the current conversation for the specifed currency
                                $forex_rate = $this->forex_rate($this->input->post('currency'), $this->input->post('convert_currency'));

                                // this is where original amount is converted to usd
                                $in_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $this->input->post('amount');


                                //this is the commission ,amount and other charges in usd
                                $before_convert = ($this->input->post('amount') - $commission - $this->input->post('other_charges'));
                                $received_amount = $before_convert * $forex_rate;

                                $received_amount_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $before_convert;


                                $other_charges_usd = ($this->forex_rate($this->input->post('currency'), 'USD')) * $this->input->post('other_charges');


                                //checking for the amount above 5000 usd
                                $above_5000 = ($in_usd) > 5000 ? 'not_approved' : 'pending';


                                // this is the one which adds the expiry date
                                $expiry_date = date('Y-m-d', strtotime('+1 years'));


                                $values = array(
                                    'sent_amount' => $this->input->post('amount'),
                                    'sender_id' => $user,
                                    'expiry_date' => strtotime($expiry_date),
                                    'commission' => $commission,
                                    'forex_rate' => $forex_rate,
                                    'currency' => $this->input->post('currency'),
                                    'recipient_currency' => $this->input->post('convert_currency'),
                                    'received_amount' => $received_amount,
                                    'transaction_id' => $txn_id = $this->transaction_id(),
                                    'secret_code' => $this->secret_code(),
                                    'sender_country' => $this->session->userdata('country'),
                                    'receiver_country' => $this->input->post('country'),
                                    'receiver_phone' => $this->input->post('receiver_phone'),
                                    'receiver_branch' => $receiver_branch=$this->input->post('branch'),
                                    'receiver_name' => $receiver = $this->input->post('receiver_name'),
                                    'receiver_email' => $this->input->post('receiver_email'),
                                    'transfer_type' => 'teller',
                                    'other_charges' => $this->input->post('other_charges'),
                                    'branch_id' => $branch_id=$this->session->userdata('branch_id'),
                                    'created_on' => time(),
                                    'created_by' => $this->session->userdata('id'),
                                    'status' => $above_5000,
                                    'sent_amount_usd' => $in_usd,
                                    //this is when all charges are converted to usd
                                    'received_amount_usd' => $received_amount_usd,
                                    'commission_usd' => $commission_usd,
                                    'other_charges_usd' => $other_charges_usd

                                );


//    this is the part of checking wetha a transaction can continue


                                if ($this->site_options->title('send_to_branch_without_money') == 'no') {


                                    if ($this->custom_library->branch_balance($receiver_branch) > $received_amount_usd) {

                                        $this->db->insert('transactions', $values);


                                        $sn = $this->db->select('full_name')->from('users')->where('id', $user)->get()->row();

                                        //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                        $this->custom_library->update_statement($branch_id, $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                        //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                        $this->custom_library->update_teller_statement($this->session->userdata('id'), $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                        //this is the registering of the logs


                                        $this->add_logs('transfer', $txn_id, $sn->full_name . ' Sent money to ' . $receiver);
                                        $r_id = $this->db->select('id')->from('transactions')->where('transaction_id', $txn_id)->get()->row();

                                        isset($r_id->id) ? redirect($this->page_level . 'receipt/sender/' . $r_id->id * date('Y') . '/s') : '';

                                    }
                                    else {

                                        $data['alert'] = 'danger';
                                        $data['message'] = 'Transaction cannot be made due to insufficient funds at recipient branch';                                     $this->load->view('alert', $data);
                                        $this->load->view($root . 'new', $data);
                                    }


                                }
                                else {

                                    $this->db->insert('transactions', $values);



                                    $sn = $this->db->select('full_name')->from('users')->where('id', $user)->get()->row();

                                    //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                    $this->custom_library->update_statement($branch_id, $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);

                                    //credit the branch balance or account update_statement($branch_id,$amount, $transaction_type, $description = '')
                                    $this->custom_library->update_teller_statement($this->session->userdata('id'), $in_usd, 'credit', $sn->full_name . ' Sent money to ' . $receiver,$txn_id);


                                    //this is the registering of the logs

                                    $this->add_logs('transfer', $txn_id, $sn->full_name . ' Sent money to ' . $receiver);
                                    $r_id = $this->db->select('id')->from('transactions')->where('transaction_id', $txn_id)->get()->row();

                                    isset($r_id->id) ? redirect($this->page_level . 'receipt/sender/' . $r_id->id * date('Y') . '/s') : '';

                                }


                            }
                            else {

                                $data['message'] = 'Error has occurred !!!';
                                $data['alert'] = 'danger';
                                $this->load->view('alert', $data);
                            }


                        }

                    }

                    //this Lists the users in the system
                    $this->db->select()->from('transactions');

                    $this->session->userdata('user_type') == '1' ? $this->db->where(array('sender_country' => $this->session->userdata('country'))) : '';

                    $this->db->where(array('receiver_country' => $this->session->userdata('country')));
                    //$this->db->where(array('created_by'=>$this->session->userdata('id')));
                    $data['t'] = $this->db->order_by('id', 'desc')->get()->result();

                    // $this->load->view($root . 'transfers', $data);

                }
                else {
                    $data['search_results'] = $this->db->select('id,full_name,city,phone')->from('users')->where('user_type', 5)->order_by('id', 'desc')->limit(20)->get()->result();
                    $this->load->view($root . 'new', $data);
                }


                break;

            //This is the function for searching the users
            case 'search_users':
                //this is the default query
                $this->db->select()->from('users')->where('user_type', 5);
                //this helps with the Form validation
                $this->form_validation->set_rules('query', 'Search User', 'xss_clean|trim|required');
                if ($this->form_validation->run() == true) {
                    $data['search_results'] = $this->db->like('full_name', $this->input->post('query'))->get()->result();
                    $this->load->view($root . 'new', $data);
                }
                else {
                    $data['search_results'] = $this->db->order_by('id', 'desc')->limit(20)->get()->result();
                    $this->load->view($root . 'new', $data);
                }
                break;

            case 'filter':
                $this->form_validation
                    ->set_rules('from', 'From', 'xss_clean|trim')
                    ->set_rules('to', 'To', 'xss_clean|trim')
                    ->set_rules('country', 'Country', 'xss_clean|trim')
                    ->set_rules('status', 'Status', 'xss_clean|trim')
                    ->set_rules('transaction', 'transaction', 'xss_clean|trim');


                if ($this->form_validation->run() == true) {


                    $this->db->select()->from('transactions');

                    if (strlen($this->input->post('transaction')) > 0) {

                        $this->db->like(
                            array
                            (
                                'transaction_id' => $this->input->post('transaction')

                            )
                        );

                        $this->session->userdata('user_type') == '1' || $this->session->userdata('user_type') == '6' ? $this->db->where(array('sender_country' => $this->session->userdata('country'))) : '';

                        $this->session->userdata('user_type') == '1' ? '' : $this->db->where(array('receiver_country' => $this->session->userdata('country')));

                        $this->session->userdata('user_type') == '6' ? $this->db->where(array('branch_id' => $this->session->userdata('branch_id')))->or_where(array('receiver_branch' => $this->session->userdata('branch_id'))) : '';

                        $data['t'] = $this->db->get()->result();

                    }
                }


                $this->load->view($root . 'transfers', $data);


                break;

            case 'hold':
                $this->form_validation->set_rules('reason', 'Hold Reason', 'xss_clean|trim|required');
                if ($this->form_validation->run() == true) {
                    $id = $data['id'] = $id / date('Y');
                    $values = array(
                        'status' => 'hold',
                        'hold_reason' => $this->input->post('reason'),
                        'approved_by' => $this->session->userdata('id'),
                        'approved_on' => time()

                    );
                    $this->db->where('id', $id)->update('transactions', $values);

                    $data['message'] = 'Transaction has been put onhold successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $rc = $this->db->select('transaction_id,receiver_name')->from('transactions')->where('id', $id)->get()->row();
                    $receiver = $rc->receiver_name;
                    $this->add_logs('hold', $rc->transaction_id, 'Transaction has been put on Hold for ' . $receiver);
                }
                else {

                    $this->load->view($root . 'hold_reason', $data);
                }


                $this->db->select()->from('transactions');
                $this->session->userdata('user_type') == '1' ? $this->db->where(array('sender_country' => $this->session->userdata('country'))) : '';
                $this->session->userdata('user_type') == '1' ? '' : $this->db->where(array('receiver_country' => $this->session->userdata('country')));
                //$this->db->where(array('created_by'=>$this->session->userdata('id')));
                $data['t'] = $this->db->order_by('id', 'desc')->get()->result();


                $this->load->view($root . 'transfers', $data);

                break;

            case 'cancel':
                $this->form_validation->set_rules('reason', 'Hold Reason', 'xss_clean|trim|required');
                if ($this->form_validation->run() == true) {
                    $id = $data['id'] = $id / date('Y');
                    $values = array(
                        'status' => 'canceled',
                        'other_reason' => $reason = $this->input->post('reason'),
                        'canceled_by' => $this->session->userdata('id'),
                        'canceled_on' => time()

                    );
                    $this->db->where('id', $id)->update('transactions', $values);

                    $data['message'] = 'Transaction has been canceled successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $rc = $this->db->select('transaction_id,receiver_name,sent_amount_usd,received_amount_usd,branch_id,created_by')->from('transactions')->where('id', $id)->get()->row();
                    $receiver = $rc->receiver_name;
                    $txn_id=$rc->transaction_id;


                    //this is where the transaction is canceled from
                    $this->custom_library->update_statement($rc->branch_id, $rc->sent_amount_usd, 'debit', $reason . ' for ' . $receiver,$txn_id);
                    //this is the end of updating the statement

                    //this is where the transaction is canceled from
                    $this->custom_library->update_teller_statement($rc->created_by, $rc->sent_amount_usd, 'debit', $reason . ' for ' . $receiver,$txn_id);
                    //this is the end of updating the statement

                    $this->add_logs('canceled',$txn_id , 'Transaction has been canceled for ' . $receiver);

                    header('Refresh: 3; url=' . base_url('index.php/' . $this->page_level . 'receipt/sender/' . $id * date('Y')));
                }
                else {

                    $this->load->view($root . 'cancel_reason', $data);
                }


                $this->db->select()->from('transactions');
                $this->session->userdata('user_type') == '1' ? $this->db->where(array('sender_country' => $this->session->userdata('country'))) : '';
                $this->session->userdata('user_type') == '1' ? '' : $this->db->where(array('receiver_country' => $this->session->userdata('country')));
                //$this->db->where(array('created_by'=>$this->session->userdata('id')));
                $data['t'] = $this->db->order_by('id', 'desc')->get()->result();


                // $this->load->view($root.'transfers',$data);

                break;

            case 'remove_hold':


                $id = $data['id'] = $id / date('Y');
                $amount = $this->db->select('sent_amount_usd')->from('transactions')->where('id', $id)->get()->row();
                $status = $amount->sent_amount_usd >= 5000 ? 'not_approved' : 'pending';
                $values = array(
                    'status' => $status,
                    // 'hold_reason'=>$this->input->post('reason'),
                    'approved_by' => $this->session->userdata('id'),
                    'approved_on' => time()

                );
                $this->db->where('id', $id)->update('transactions', $values);

                $data['message'] = 'Transaction has been released for transfer successfully. ';
                $data['alert'] = 'success';
                $this->load->view('alert', $data);


                //adding for logs

                $rc = $this->db->select('transaction_id,receiver_name')->from('transactions')->where('id', $id)->get()->row();
                $receiver = $rc->receiver_name;
                $this->add_logs('release', $rc->transaction_id, 'Transaction has been Released for ' . $receiver);

                // this is the end of the user transactions


                $this->db->select()->from('transactions');
                $this->session->userdata('user_type') == '1' ? $this->db->where(array('sender_country' => $this->session->userdata('country'))) : '';
                $this->session->userdata('user_type') == '1' ? '' : $this->db->where(array('receiver_country' => $this->session->userdata('country')));
                //$this->db->where(array('created_by'=>$this->session->userdata('id')));
                $data['t'] = $this->db->order_by('id', 'desc')->get()->result();


                $this->load->view($root . 'transfers', $data);

                break;


        }

        $this->load->view($page_level . 'footer_table', $data);


    }



    public function cashout($type = null, $id = null)
    {

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',


        );

        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $this->load->view($page_level . 'header', $data);

        switch ($type) {
            case 'process':
                $id = $data['id'] = $id / date('Y');

                // this is the function which uploads the profile image
                $this->form_validation
                    ->set_rules('document', 'Document Image', 'xss_clean|trim')
                    ->set_rules('document_id', 'Document ID', 'xss_clean|trim|required')
                    ->set_rules('document_type', 'Document Type', 'xss_clean|trim|required')
                    ->set_rules('document_expiry_date', 'Document Expiry Date', 'xss_clean|trim|required|callback_greater_than_today')
                    ->set_rules('receiver_name', 'Receiver Name', 'xss_clean|trim|required')
                    ->set_rules('country', 'Receiver Country', 'xss_clean|trim|required')
                    ->set_rules('street', 'Street Address', 'xss_clean|trim|required')
                    ->set_rules('receiver_mail', 'receiver email', 'xss_clean|trim')
                    ->set_rules('phone2', 'receiver Second Phone', 'xss_clean|trim')
                    ->set_rules('txn_id', '', 'xss_clean|trim');
                //this is the company name

                $cname = 'recipients';
                //managing of the images
                $path = $config['upload_path'] = './uploads/' . underscore($cname) . '/';
                $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                $config['max_size'] = '200';
                $config['max_width'] = '1920';
                $config['max_height'] = '850';
                $this->upload->initialize($config);


                if (!is_dir($path)) //create the folder if it's not already exists
                {
                    mkdir($path, 0777, TRUE);
                }
                if ($this->form_validation->run() == true) {

                    $current_user=$this->session->userdata('id');


                    ///This is the for the document verification
                    $document_values = array(
                        'document' => strlen($this->upload->file_name) > 1 ? $path . $this->upload->file_name : 'assets/user-id-icon.png',
                        'document_id' => $this->input->post('document_id'),
                        'document_type' => $this->input->post('document_type'),
                        'expiry_date' => strtotime($this->input->post('document_expiry_date')),
                        'transaction_id' => $id,
                        'status' => 'verified',
                        'created_on' => time(),
                        'created_by' => $current_user
                    );


//                        $trans = $this->db->select('sent_amount_usd,received_amount_usd')->from('transactions')->where('id', $id)->get()->row();

//                        if($this->custom_library->branch_balance($this->session->userdata('branch_id')) > $trans->received_amount_usd ){

                    $this->db->insert('recipient_details', $document_values);

                    //this updates the transaction to change the status from pending to cashed_out


                    $trans_values = array(
                        'status' => 'cashed_out',
                        'receiver_name' => $receiver = $this->input->Post('receiver_name'),
                        'receiver_country' => $this->input->Post('country'),
                        'receiver_email' => $this->input->Post('receiver_email'),
                        'receiver_phone2' => $this->input->Post('phone2'),
                        // 'receiver_street_address'=>$this->input->Post('street'),
                        'updated_on' => time(),
                        'updated_by' => $current_user
                    );

                    $this->db->where('id', $id)->update('transactions', $trans_values);

                    $txn_id=$this->input->post('txn_id');
                    $received_amount_usd=$this->input->post('received_amount_usd');

                    $this->custom_library->update_teller_statement($current_user,$received_amount_usd ,'debit', 'Cashout for ' . $receiver,$txn_id);

                    $this->custom_library->update_statement($this->session->userdata('branch_id'), $received_amount_usd,'debit', 'Cashout for ' . $receiver,$txn_id);




                    $this->load->view($root . 'cashout_process', $data);

                    $this->add_logs('cashout', $txn_id, 'Verified Document and success cashout for' . $receiver);

                    redirect($this->page_level . 'receipt/cashout/' . $id * date('Y') . '/s');

//                    }else{
//
//
//                            $data['alert']='error';
//                            $data['message']='Transaction cannot be made due to insufficient funds at this branch';
//                            $this->load->view($root . 'cashout_process', $data);
//                        }


                }
                else {

                    $data['error'] = $this->upload->display_errors();

                    $this->load->view($root . 'cashout_process', $data);

                }


                break;


            default:

                // this is the overall filter for the reports
                $this->form_validation
                    ->set_rules('from', 'From', 'xss_clean|trim')
                    ->set_rules('to', 'To', 'xss_clean|trim')
                    ->set_rules('country', 'Country', 'xss_clean|trim')
                    ->set_rules('status', 'Status', 'xss_clean|trim');
                // this is the end of the overall reports for the filter of reports

                $first_day = strtotime('first day of this month', time());
                $last_day = strtotime('last day of this month', time());
                $data = array('first_day' => $first_day, 'last_day' => $last_day);

                $this->db->select()->from('transactions');

                //this is selecting for the date
                strlen($this->input->post('from')) > 0 ? $this->db->where(
                    array
                    (
                        'created_on >=' => strtotime($this->input->post('from'). ' 00:00:00'),
                        'created_on <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                    )
                ) : $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )
                );

                //this is selecting for the other
                $this->db->where(array('receiver_country' => $this->session->userdata('country')));

                $data['t'] = $this->db->where('status', 'cashed_out')->order_by('id', 'desc')->get()->result();


                $data['receiver_country'] = $this->session->userdata('country');

                $this->load->view($root . 'cashout', $data);
                break;
        }

        $this->load->view($page_level . 'footer_table', $data);

    }


//this is the function which checks for the currency if it exists

    public function receipt($type = null, $id = null)
    {

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',

        );
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $this->load->view($page_level . 'header', $data);
        //isset($this->uri->segment(5));
        $data['message'] = 'Transaction processed successfully <i class="fa fa-check"></i>';
        $data['alert'] = 'success';

        $id = $data['id'] = $id / date('Y');

        switch ($type) {
            default:

                break;
            case 'cashout':

                $this->load->view($root . 'receipt', $data);
                break;
            case 'sender':

                $this->load->view($root . 'receipt', $data);
                break;
        }

        $this->load->view($page_level . 'footer_table', $data);
    }


    /**
     * @param null $type
     * @param null $id
     */

    public function branch($type = null, $id = null)
    {

        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => 'reports',
            'subtitle' => 'branches',
            'page_level' => $page_level

        );

        $this->load->view($page_level . 'header', $data);
        $data['id'] = $id / date('Y');


        $this->form_validation
            ->set_rules('from', 'From', 'xss_clean|trim')
            ->set_rules('to', 'To', 'xss_clean|trim');


        //this the default date

        if ($this->input->post('from') && $this->input->post('from')) {
            $data['first_day'] = strtotime($this->input->post('from'). ' 00:00:00');
            $data['last_day'] = strtotime($this->input->post('to').' 23:59:59');
        }
        else {
            $data['first_day'] = strtotime(date('Y-m-d 00:00:00'));
            $data['last_day'] = strtotime(date('Y-m-d 23:59:59'));
        }


        //this is the end of default date

        switch ($type) {

            default:

                break;


            case 'credit':
            case 'debit':

                $this->form_validation->set_rules('amount', 'Amount', 'xss_clean|trim|required');

                if ($this->form_validation->run() == true) {
                    $description = "Manual branch balance $type";

                    if ($this->custom_library->update_statement($id / date('Y'), $this->input->post('amount'), $type, $description)) {

                        $data['alert'] = 'success';
                        $data['message'] = $this->input->post('amount') . " has been " . $type . "ed to the selected branch";
                        $data['hide'] = 1;
                        $this->load->view('alert', $data);
                    }
                    else {
                        $data['alert'] = 'danger';
                        $data['message'] = "<b>Opps ! </b> Error occurred while " . $type . "ing Branch";
                        $data['hide'] = 1;
                        $this->load->view('alert', $data);
                    }

                }
                else {

                    $this->load->view($this->page_level . 'branch/credit_debit');
                }

                $this->load->view($this->page_level . 'branch/statement', $data);
                break;

            case 'statement':
                $data['id'] = $id / date('Y');
                $this->load->view($this->page_level . 'branch/statement', $data);
                break;


        }


        //  $this->input->post() && $type == 'statement' ? '' : $this->load->view($this->page_level . 'branch/statement', $data);
        $this->load->view($page_level . 'footer_table', $data);


    }

    /**
     * @param null $type
     * @param null $id
     */

    public function teller($type = null, $id = null)
    {

        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => 'reports',
            'subtitle' => 'branches',
            'page_level' => $page_level

        );

        $this->load->view($page_level . 'header', $data);
        $data['id'] = $id / date('Y');


        $this->form_validation
            ->set_rules('from', 'From', 'xss_clean|trim')
            ->set_rules('to', 'To', 'xss_clean|trim');


        //this the default date

        if ($this->input->post('from') && $this->input->post('from')) {
            $data['first_day'] = strtotime($this->input->post('from'). ' 00:00:00');
            $data['last_day'] = strtotime($this->input->post('to'). ' 23:59:59');
        }
        else {
            $data['first_day'] = strtotime(date('Y-m-d 00:00:00'));
            $data['last_day'] = strtotime(date('Y-m-d 23:59:59'));
        }


        //this is the end of default date

        switch ($type) {

            default:

                break;


            case 'credit':
            case 'debit':

                $this->form_validation->set_rules('amount', 'Amount', 'xss_clean|trim|required');

                if ($this->form_validation->run() == true) {
                    $description = "Manual Teller balance $type";

                    if ($this->custom_library->update_teller_statement($id / date('Y'), $this->input->post('amount'), $type, $description,'Manual')) {

                        $data['alert'] = 'success';
                        $data['message'] = $this->input->post('amount') . " has been " . $type . "ed to the selected teller";
                        $data['hide'] = 1;
                        $this->load->view('alert', $data);
                    }
                    else {
                        $data['alert'] = 'danger';
                        $data['message'] = "<b>Opps ! </b> Error occurred while " . $type . "ing Branch";
                        $data['hide'] = 1;
                        $this->load->view('alert', $data);
                    }

                }
                else {

                    $this->load->view($this->page_level . $this->page_level2.'credit_debit');
                }

                $this->load->view($this->page_level . $this->page_level2.'statement', $data);
                break;

            case 'statement':
                $data['id'] = $id / date('Y');
                $this->load->view($this->page_level . $this->page_level2.$type, $data);
                break;


        }


        //  $this->input->post() && $type == 'statement' ? '' : $this->load->view($this->page_level . 'branch/statement', $data);
        $this->load->view($page_level . 'footer_table', $data);


    }



    /**
     * @param null $type
     * @param null $filter
     * @param null $id
     */


    public function reports($type = null, $filter = null, $id = null)
    {

        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level

        );
        $data['status'] = $this->get_enum_values('transactions', 'status');
        $this->load->view($page_level . 'header', $data);

        // this is the overall filter for the reports
        $this->form_validation
            ->set_rules('filter_type', 'Filter Type', 'xss_clean|trim')
            ->set_rules('from', 'From', 'xss_clean|trim')
            ->set_rules('to', 'To', 'xss_clean|trim')
            ->set_rules('country', 'Country', 'xss_clean|trim')
            ->set_rules('status', 'Status', 'xss_clean|trim')
            ->set_rules('branches[]', 'Branches', 'trim')
            ->set_rules('compare_branch', 'branch', 'trim');
        // this is the end of the overall reports for the filter of reports





        $created_on=$this->input->post('filter_type')=='date_cashout'?'updated_on':'created_on';

        switch ($type) {

            case 'transfer':
                //this is the query which extracts data from the database

                $this->db->select()->from('transactions');
                strlen($this->input->post('status')) ? $this->db->where(array('status' => $this->input->post('status'))) : '';
                $this->db->where(array('sender_country' => $this->session->userdata('country'), 'created_by' => $this->session->userdata('id')));
                $data['t'] = $this->db->order_by('id', 'desc')->get()->result();
                //this is the end of the qury

                $this->load->view($root . $type, $data);
                break;

            case 'cashout':


                $first_day = strtotime('first day of this month', time());
                $last_day = strtotime('last day of this month', time());
                $data = array('first_day' => $first_day, 'last_day' => $last_day);

                $this->db->select()->from('transactions');

                //this is selecting for the date
                strlen($this->input->post('from')) > 0 ? $this->db->where(
                    array
                    (
                        $created_on.' >=' => strtotime($this->input->post('from').' 00:00:00'),
                        $created_on.'  <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                    )
                ) : $this->db->where(
                    array
                    (
                        $created_on.'  >=' => $first_day,
                        $created_on.'  <=' => $last_day,
                    )
                );

                //this is selecting for the other
                $this->db->where(array('receiver_country' => $this->session->userdata('country')));

                $data['t'] = $this->db->where('status', 'cashed_out')->order_by('id', 'desc')->get()->result();

                $this->load->view($root . $type, $data);
                break;

            case 'weekly':

                $first_day = strtotime("last Monday");
                $last_day = strtotime('next sunday');
                $data = array('first_day' => $first_day, 'last_day' => $last_day);

                if ($filter == 'export') {
                    $this->load->view($root . $type, $data);
                }
                else {
                    $this->db->select()->from('transactions');


                    //this is selecting for the date
                    $this->input->post('from') ? $this->db->where(
                        array
                        (
                            $created_on.'  >=' => strtotime($this->input->post('from').' 00:00:00'),
                            $created_on.'  <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                        )
                    ) : $this->db->where(
                        array
                        (
                            $created_on.'  >=' => $first_day,
                            $created_on.'  <=' => $last_day,
                        )
                    );

                    strlen($this->input->post('status')) ? $this->db->where(array('status' => $this->input->post('status'))) : '';

                    //this is selecting for the other
                    $country=$this->input->post('country');
                    $this->input->post('country') ?  $this->db->where("(receiver_country = '$country' OR sender_country = '$country' )") : '';

                    $data['t'] = $this->db->order_by('id', 'desc')->get()->result();
                    $this->load->view($root . 'periodic_report', $data);
                }
                break;
            case 'daily':

                $first_day = strtotime(date('Y-m-d 00:00:00'));
                $last_day = strtotime(date('Y-m-d 23:59:59'));
                $data = array('first_day' => $first_day, 'last_day' => $last_day);

                if ($filter == 'export') {

                    $this->load->view($root . $type, $data);

                }
                else {
                    $this->db->select()->from('transactions');


                    //this is selecting for the date
                    $this->input->post('from') ? $this->db->where(
                        array
                        (
                            $created_on.'  >=' => strtotime($this->input->post('from').' 00:00:00'),
                            $created_on.'  <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                        )
                    ) : $this->db->where(
                        array
                        (
                            $created_on.'  >=' => $first_day,
                            $created_on.'  <=' => $last_day,
                        )
                    );

                    strlen($this->input->post('status')) ? $this->db->where(array('status' => $this->input->post('status'))) : '';

                    //this is selecting for the other
                    $country=$this->input->post('country');
                    $this->input->post('country') ?  $this->db->where("(receiver_country = '$country' OR sender_country = '$country' )") : '';


                    $data['t'] = $this->db->order_by('id', 'desc')->get()->result();
                    $this->load->view($root . 'periodic_report', $data);
                }
                break;

            case 'monthly':

                if ($filter == 'export') {

                    $data['first_day'] = strtotime('first day of this month', time());
                    $data['last_day'] = strtotime('last day of this month', time());

                    $this->load->view($root . $type, $data);
                }
                else {
                    $this->db->select()->from('transactions');
                    $first_day = strtotime('first day of this month', time());
                    $last_day = strtotime('last day of this month', time());

                    $data = array('first_day' => $first_day, 'last_day' => $last_day);

                    //this is selecting for the date
                    strlen($this->input->post('status')) ? $this->db->where(array('status' => $this->input->post('status'))) : '';
                    strlen($this->input->post('from')) > 0 ? $this->db->where(
                        array
                        (
                            $created_on.'  >=' => strtotime($this->input->post('from').' 00:00:00'),
                            $created_on.'  <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                        )
                    ) : $this->db->where(
                        array
                        (
                            $created_on.'  >=' => $first_day,
                            $created_on.'  <=' => $last_day,
                        )
                    );


                    //this is selecting for the other
                    $country=$this->input->post('country');
                    $this->input->post('country') ?  $this->db->where("(receiver_country = '$country' OR sender_country = '$country' )") : '';
                    $data['t'] = $this->db->order_by('id', 'desc')->get()->result();
                    $this->load->view($root . 'periodic_report', $data);
                }
                break;

            case 'above_5000':

                if ($filter == 'export') {

                    $this->load->view($root . $type, $data);
                }
                else {
                    $this->db->select()->from('transactions');

                    //this is selecting for the date
                    strlen($this->input->post('from')) > 0 ? $this->db->where(
                        array
                        (
                            $created_on.'  >=' => strtotime($this->input->post('from').' 00:00:00'),
                            $created_on.'  <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                        )
                    ) : '';

                    //this is selecting for the other
                    $country=$this->input->post('country');
                    $this->input->post('country') ?  $this->db->where("(receiver_country = '$country' OR sender_country = '$country' )") : '';
                    strlen($this->input->post('status')) ? $this->db->where(array('status' => $this->input->post('status'))) : '';

                    $this->db->where(array('sent_amount_usd >=' => '5000'));

                    $data['t'] = $this->db->order_by('id', 'desc')->get()->result();
                    $this->load->view($root . 'periodic_report', $data);
                }
                break;

            case 'quarterly':

                if ($filter == 'export') {
                    $this->load->view($root . $type, $data);
                }
                else {
                    $this->db->select()->from('transactions');

                    $last_day = strtotime('last day of this month', time());
                    $first_day = strtotime(date('Y-m-d', strtotime('first day of this month', time())) . " -2 month");

                    $data = array('first_day' => $first_day, 'last_day' => $last_day);

                    //this is selecting for the date
                    strlen($this->input->post('from')) > 0 ? $this->db->where(
                        array
                        (
                            $created_on.'  >=' => strtotime($this->input->post('from').' 00:00:00'),
                            $created_on.'  <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                        )
                    ) : $this->db->where(
                        array
                        (
                            $created_on.'  >=' => $first_day,
                            $created_on.'  <=' => $last_day,
                        )
                    );

                    //this is selecting for the other
                    $country=$this->input->post('country');
                    $this->input->post('country') ?  $this->db->where("(receiver_country = '$country' OR sender_country = '$country' )") : '';
                    strlen($this->input->post('status')) ? $this->db->where(array('status' => $this->input->post('status'))) : '';

                    $data['t'] = $this->db->get()->result();
                    $this->load->view($root . 'periodic_report', $data);
                }
                break;

            case 'on_hold':

                if ($filter == 'export') {
                    $this->load->view($root . $type, $data);
                }
                else {
                    $this->db->select()->from('transactions');

                    //this is selecting for the date
                    strlen($this->input->post('from')) > 0 ? $this->db->where(
                        array
                        (
                            $created_on.'  >=' => strtotime($this->input->post('from').' 00:00:00'),
                            $created_on.'  <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                        )
                    ) : '';

                    //this is selecting for the other
                    $country=$this->input->post('country');
                    $this->input->post('country') ?  $this->db->where("(receiver_country = '$country' OR sender_country = '$country' )"): '';

                    $this->db->where(array('status' => 'hold'));
                    $data['t'] = $this->db->order_by('id', 'desc')->get()->result();
                    $this->load->view($root . 'periodic_report', $data);
                }
                break;

            case 'teller':

                //  this the default Period
                $first_day = strtotime('first day of this month', time());
                $last_day = time();
                //this is the end of default period

                if ($filter == 'details') {

                    $data['details'] = 'details';
                    $id = $data['id'] = $id / date('Y');
                    $branch_id = $this->session->userdata('branch_id');


                    //this is selecting for the date
                    if ($this->form_validation->run() == TRUE) {

                        if (strlen($this->input->post('from')) > 0) {
                            $first_day = strtotime($this->input->post('from') . ' 00:00:00');
                            $last_day = strtotime($this->input->post('to') . ' 23:59:59');
                        }

                        $country = strlen($this->input->post('country')) > 0 ? $this->input->post('country') : $this->session->userdata('country');
                        $status = strlen($this->input->post('status')) > 0 ? $this->input->post('status') : '';


                        $default_qry = "SELECT * FROM (`transactions`) WHERE
                    ((`created_on` >= $first_day AND `created_on` <= $last_day AND `created_by` = $id ) OR (`updated_on` >= $first_day AND `updated_on` <= $last_day AND `updated_by` = $id))  ";

                        if ($this->session->userdata('user_type') == 6) {


                            $qry1 = "$default_qry AND `branch_id` = $branch_id ORDER BY `id` desc";
                            $qry_status = "$default_qry AND `branch_id` = $branch_id AND `status`= '$status' AND (`receiver_country` = '$country' OR `sender_country` = '$country') ORDER BY `id` desc";
                            $qry = strlen($this->input->post('status')) > 0 ? $qry_status : $qry1;


                        } else {

                            $qry1 = "$default_qry ORDER BY `id` desc";
                            $qry_status = "$default_qry AND `status`= '$status' AND (`receiver_country` = '$country' OR `sender_country` = '$country') ORDER BY `id` desc";
                            $qry = strlen($this->input->post('status')) > 0 ? $qry_status : $qry1;

                        }

                    } else {

                        $default_qry = "SELECT * FROM (`transactions`) WHERE
                    ((`created_on` >= $first_day AND `created_on` <= $last_day AND `created_by` = $id ) OR (`updated_on` >= $first_day AND `updated_on` <= $last_day AND `updated_by` = $id))  ";

                        if ($this->session->userdata('user_type') == 6) {
                            $qry = "$default_qry  AND  `branch_id` = $branch_id ORDER BY `id` desc";

                        } else {

                            $qry = "$default_qry ORDER BY `id` desc";

                        }


                    }

                    $data['t'] = $this->db->query($qry)->result();

                    $this->load->view($root . 'periodic_report', $data);
                } else {

                    strlen($this->input->post('from')) > 0 ?
                        $data = array
                        (
                            'first_day' => strtotime($this->input->post('from') . ' 00:00:00'),
                            'last_day' => strtotime($this->input->post('to') . ' 23:59:59'),
                        )
                        :
                        $data = array(
                            'first_day' => $first_day,
                            'last_day' => $last_day
                        );

                    $this->db->select('id,full_name,branch_id')->from('users');


                    $this->session->userdata('user_type') == 6 ? $this->db->where(array('branch_id' => $this->session->userdata('branch_id'))) : '';

//

                    $data['tellers'] = $this->db->where(array('user_type != ' => '5'))->get()->result();

                    $this->load->view($root . 'teller_report', $data);
                }


                break;

            case 'branch':

                //  this the default Period
                $first_day = strtotime('first day of this month', time());
                $last_day = time();
                //this is the end of default period

                $user_type = $this->session->userdata('user_type');

                if ($filter == 'details' || $user_type == '6') {

                    $data['details'] = 'details';
                    $id = $data['id'] = $user_type == '6' && strlen($id) == 0 ? $this->session->userdata('branch_id') : $id / date('Y');

                    if (strlen($this->input->post('from')) > 0) {

                        $first_day = $data['first_day'] = strtotime($this->input->post('from').' 00:00:00');

                        $last_day = $data['last_day'] = strtotime($this->input->post('to') . ' 23:59:59');
                    }
                    else {

                        $data['first_day'] = $first_day;
                        $data['last_day'] = $last_day;

                    }


                    $compare_branch = strlen($this->input->post('compare_branch')) > 0 ? $this->input->post('compare_branch') : '';

                    $period=" AND (`$created_on` >= $first_day AND `$created_on` <= $last_day)";

                    $where="";

                    $where .=$this->input->post('compare_branch')?
                        "((`receiver_branch` = $id AND  `branch_id` = $compare_branch $period) OR (`branch_id` = $id AND  `receiver_branch` = $compare_branch $period) )"
                        :
                        "((`receiver_branch` = $id $period) OR  (`branch_id` = $id $period)) ";







                    $this->db->select()->from('transactions');

                    $this->input->post('status') && strlen($this->input->post('status'))?$this->db->where('status',$this->input->post('status')):'';

                    $this->db->where($where);

                    $this->db->order_by('id','desc');
                    $data['t'] =  $this->db->get()->result();

                    //SELECT * FROM (`transactions`) WHERE (`receiver_branch` = 16 OR `branch_id` = 16)AND (`created_on` >= 1488397830 AND `created_on` <= 1490385030) ORDER BY `id` desc



                    $this->load->view($root . 'periodic_report', $data);

                }

                else {


                    $data = strlen($this->input->post('from')) > 0 ?
                        array
                        (
                            'first_day' => strtotime($this->input->post('from').' 00:00:00'),
                            'last_day' => strtotime($this->input->post('to') . ' 23:59:59'),
                        )
                        :
                        array
                        (
                            'first_day' => $first_day,
                            'last_day' => $last_day,
                        );


                    $branches = $this->input->post('branches');


                    $where =" (`$created_on` >= $first_day AND `$created_on` <= $last_day) AND";
                    $selected_branches = '';

                    if ($branches) {

                        if (count($branches)) {
                            // print_r($data_en);
                            $selected_branches = " (";
                            $got = count($branches);
                            $n = 0;

                            foreach ($branches as $d) {


                                $selected_branches .= "`id` = $d";

                                $n++;
                                $n != $got ? $selected_branches .= " OR" : "";

                            }
                            $selected_branches .= " )";
                        }

                    }
                    else {
                        $selected_branches = 1;
                    }


                    $data['compare_branch']=strlen($this->input->post('compare_branch'))?$this->input->post('compare_branch'):'';


                    $sq_ent = "SELECT id,branch_name,country,balance FROM `branch` WHERE  $selected_branches";





                    $data['selected_branches']=$selected_branches;
                    $data['branch'] = $this->db->query($sq_ent)->result();

                    //$data['branch'] = $this->db->select()->from('branch')->where($where[0])->get()->result();

                    $this->load->view($root . 'branch_report', $data);

                    if($this->input->post('export')){
                        $first_day=strtotime($this->input->post('from').' 00:00:00');
                        $last_day=strtotime($this->input->post('to').' 00:00:00');
                        $branches= count($branches)?$branches:1;

                        $this->export_excel('branch_report',$first_day,$last_day,$branches);
                    }


                }


                break;


            default:
                $this->db->select()->from('transactions');


                //this is selecting for the date
                strlen($this->input->post('from')) > 0 ? $this->db->where(
                    array
                    (
                        $created_on.'  >=' => strtotime($this->input->post('from').' 00:00:00'),
                        $created_on.'  <=' => strtotime($this->input->post('to') . ' 23:59:59'),
                    )
                ) : '';

                //this is selecting for the other
                $country=$this->input->post('country');
                $this->input->post('country') ?  $this->db->where("(receiver_country = '$country' OR sender_country = '$country' )"): '';
                if (strlen($this->input->post('status')) > 0) {
                    if ($this->input->post('status') == 'above_5000') {
                        $this->db->where(array('sent_amount_usd >=' => '5000'));
                    }
                    else {
                        $this->db->where(array('status' => $this->input->post('status')));
                    }
                }

                $this->db->order_by('id','desc');
                $data['t'] = $this->db->get()->result();
                $this->load->view($page_level . 'reports/reports', $data);
                break;
        }

        $this->load->view($page_level . 'footer_table', $data);

    }

    function export($type = null, $from = null, $to = null ,$country=null,$status=null,$branch=null)
    {
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level

        );
        $this->load->view($page_level . 'header', $data);
        //this is the part where the export is initialised
        $this->load->view('php-excel-reader/Classes/PHPExcel');
        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/periodic_report_template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level

        $this->db->select()->from('transactions');

        $selected_country = isset($country)&& strlen($country) > 0  && $country!='null'? $country : $this->session->userdata('country');

        $where=isset($country)&& strlen($country)>0 && $country!='null' ? "(receiver_country = '$selected_country' OR sender_country = '$selected_country' )" : '';

        //this is the filter for the status
        isset($status) && strlen($status) > 0 ? $this->db->where(array('status'=>$status)):'';




        switch ($type) {

            case 'custom_date':

                $this->db->where(
                    array
                    (
                        'created_on >=' => $from,
                        'created_on <=' => $to,
                    )
                );

                strlen($where)>0? $this->db->where($where):'';


                break;

            case 'daily':

                //this is these are the days
                $first_day = strtotime(date('Y-m-d 00:00:00'));
                $last_day = strtotime(date('Y-m-d 23:59:59'));

                $status=$from;
                isset($status) && strlen($status) > 0 ? $this->db->where(array('status'=>$status)):'';

                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )
                );


                break;

            case 'weekly':

                //this is these are the days
                $last_day = strtotime('next sunday');
                $first_day = strtotime("last Monday");

                $status=$from;
                isset($status) && strlen($status) > 0 ? $this->db->where(array('status'=>$status)):'';
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )
                );
                strlen($where)>0? $this->db->where($where):'';


                break;

            case 'monthly':



                $first_day = strtotime('first day of this month', time());
                $last_day = strtotime('last day of this month', time());


                $status=$from;
                isset($status) && strlen($status) > 0 ? $this->db->where(array('status'=>$status)):'';
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )
                );



                break;

            case 'above_5000':


                $status=$from;
                isset($status) && strlen($status) > 0 ? $this->db->where(array('status'=>$status)):'';
                $this->db->where(array('sent_amount_usd >=' => '5000'));
                $this->db->order_by('id', 'desc');

                break;

            case 'on_hold':


                $this->db->where(array('status' => 'hold'));
                $this->db->order_by('id', 'desc');

                break;

            case 'quarterly':

                $last_day = strtotime('last day of this month', time());
                $first_day = strtotime(date('Y-m-d', strtotime('first day of this month', time())) . " -2 month");

                $status=$from;
                isset($status) && strlen($status) > 0 ? $this->db->where(array('status'=>$status)):'';
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )
                );


                break;
        }

        $q = $this->db->get()->result();


        //this is the part for the beginning of the export

        $no = 2;
        foreach ($q as $rs) {


            $user = $this->db->select('full_name')->from('users')->where('id', $rs->sender_id)->get()->row();

            $objWorksheet->getCell('A' . $no)->setValue($rs->transaction_id);

            $sender = isset($user->full_name) ? ucwords($user->full_name) : 'N/A';

            $objWorksheet->getCell('B' . $no)->setValue($sender);

            $objWorksheet->getCell('C' . $no)->setValue(ucwords($rs->receiver_name));

            $objWorksheet->getCell('D' . $no)->setValue($rs->received_amount);

            $objWorksheet->getCell('E' . $no)->setValue($rs->commission);

            $objWorksheet->getCell('F' . $no)->setValue($rs->other_charges);

            $objWorksheet->getCell('G' . $no)->setValue($rs->receiver_phone);

            $c = $this->db->select('country')->from('country')->where('a2_iso', $rs->sender_country)->get()->row();
            $sender_country = isset($c->country) ? $c->country : 'N/A';
            $objWorksheet->getCell('H' . $no)->setValue($sender_country);


            $r = $this->db->select('country')->from('country')->where('a2_iso', $rs->receiver_country)->get()->row();
            $receiver_country = isset($r->country) ? $r->country : 'N/A';
            $objWorksheet->getCell('I' . $no)->setValue($receiver_country);


            $bn = $this->db->select('branch_name')->from('branch')->where('id', $rs->branch_id)->get()->row();
            $branch_name = isset($bn->branch_name) ? $bn->branch_name : 'N/A';
            $objWorksheet->getCell('J' . $no)->setValue($branch_name);


            $objWorksheet->getCell('K' . $no)->setValue(date('d-m-Y H:i:s', $rs->created_on));
            $objWorksheet->getCell('L' . $no)->setValue(humanize($rs->status));

            $no++;
        }

        //this is the part for the export
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');
        echo $objWriter->save($dir = 'downloads/WMT_' . $type . '_report_from_' . date('d-m-Y', $first_day) . '_to_' . date('d-m-Y', $last_day) . '_' . time() . '.xls');
        redirect(base_url($dir));

        //this is the end of the initialisation
        $this->load->view($page_level . 'footer_table');
    }


    function export_excel($type=null,$first_day=null,$last_day=null,$id=null){

        //this is the part where the export is initialised
        $this->load->view('php-excel-reader/Classes/PHPExcel');
        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        switch($type){

            default:
                echo 'default';
                break;

            case 'branch_statement':

                //echo $type;
                $objWorksheet->getCell('A1')->setValue('Date');
                $objWorksheet->getCell('B1')->setValue('Type');
                $objWorksheet->getCell('C1')->setValue('Description');
                $objWorksheet->getCell('D1')->setValue('Debit');
                $objWorksheet->getCell('E1')->setValue('Credit');
                $objWorksheet->getCell('F1')->setValue('Balance');

                $qry=$this->db->select('a.*,b.full_name')->from('branch_statements a')
                    ->join('users b','a.created_by=b.id')
                    ->where(
                        array(
                            'a.branch_id'=>$id/date('Y'),
                            'a.created_on >=' => $first_day,
                            'a.created_on <=' => $last_day
                        )
                    )->order_by('a.id','desc')->get()->result();

                $no=2;

                foreach($qry as $c):

                    $objWorksheet->getCell('A'.$no)->setValue(date('d-m-Y H:i:s',$c->created_on));
                    $objWorksheet->getCell('B'.$no)->setValue(humanize($c->type));
                    $objWorksheet->getCell('C'.$no)->setValue( $c->description);
                    $objWorksheet->getCell('D'.$no)->setValue($c->debit);
                    $objWorksheet->getCell('E'.$no)->setValue($c->credit);
                    $objWorksheet->getCell('F'.$no)->setValue($c->balance);

                    $no++;
                endforeach;

                break;

            case 'teller_statement':

                //echo $type;
                $objWorksheet->getCell('A1')->setValue('Date');
                $objWorksheet->getCell('B1')->setValue('Type');
                $objWorksheet->getCell('C1')->setValue('Description');
                $objWorksheet->getCell('D1')->setValue('Debit');
                $objWorksheet->getCell('E1')->setValue('Credit');
                $objWorksheet->getCell('F1')->setValue('Balance');

                $qry=$this->db->select('a.*,b.full_name')->from('teller_statements a')
                    ->join('users b','a.created_by=b.id')
                    ->where(
                        array(
                            'a.teller_id'=>$id/date('Y'),
                            'a.created_on >=' => $first_day,
                            'a.created_on <=' => $last_day
                        )
                    )->order_by('a.id','desc')->get()->result();

                $no=2;

                foreach($qry as $c):

                    $objWorksheet->getCell('A'.$no)->setValue(date('d-m-Y H:i:s',$c->created_on));
                    $objWorksheet->getCell('B'.$no)->setValue(humanize($c->type));
                    $objWorksheet->getCell('C'.$no)->setValue( $c->description);
                    $objWorksheet->getCell('D'.$no)->setValue($c->debit);
                    $objWorksheet->getCell('E'.$no)->setValue($c->credit);
                    $objWorksheet->getCell('F'.$no)->setValue($c->balance);

                    $no++;
                endforeach;

                break;

            case 'branch_report':


                // var_dump($id);

                //echo $type;
                $objWorksheet->getCell('A1')->setValue('Branch');
                $objWorksheet->getCell('B1')->setValue('Country');
                $objWorksheet->getCell('C1')->setValue('Sent Amount(USD)');
                $objWorksheet->getCell('D1')->setValue('Received Amount(USD)');
                $objWorksheet->getCell('E1')->setValue('Cashout(USD)');
                $objWorksheet->getCell('F1')->setValue('Pending');
                $objWorksheet->getCell('G1')->setValue('Commission');
                $objWorksheet->getCell('H1')->setValue('Balance');



                $selected_branches = '';

                if (isset($id) && $id!=1) {

                    if (count($id)) {
                        // print_r($data_en);
                        $selected_branches = "( ";
                        $got = count($id);
                        $n = 0;

                        foreach ($id as $d) {


                            $selected_branches .= "`a`.`id` = $d";

                            $n++;
                            $n != $got ? $selected_branches .= " OR " : "";

                        }
                        $selected_branches .= " )";
                    }

                }
                else {
                    $selected_branches = 1;
                }


                $period= array(
                    'a.created_on >=' => $first_day,
                    'a.created_on <=' => $last_day
                );

                $this->db
                    ->select('a.id,a.branch_name,b.country,a.balance')
                    ->from('branch a')
                    ->join('country b','a.country=b.a2_iso');
                isset($id) && $id!=1 && count($id)>0 && $id !=false?$this->db->where($selected_branches):'';
                $qry=$this->db->get()->result();

                $no=2;

                foreach($qry as $c):


                    $sent_amount= $this->db->select_sum('sent_amount_usd')->from('transactions a')->where(array('branch_id'=>$c->id))->where($period)->get()->row();

                    $transfer=$this->db->select_sum('received_amount_usd')->from('transactions a')->where('receiver_branch',$c->id)->where($period)->get()->row();

                    $cashout=$this->db->select_sum('received_amount_usd')->from('transactions a')->where(array('receiver_branch'=>$c->id,'status'=>'cashed_out'))->where($period)->get()->row();
                    $pending=$this->db->select_sum('received_amount_usd')->from('transactions a')->where(array('receiver_branch'=>$c->id,'status'=>'pending'))->where($period)->get()->row();
                    $comm=$this->db->select_sum('commission_usd')->from('transactions a')->where(array('receiver_branch'=>$c->id,'status'=>'cashed_out'))->where($period)->get()->row();

                    $objWorksheet->getCell('A'.$no)->setValue(humanize($c->branch_name));
                    $objWorksheet->getCell('B'.$no)->setValue(humanize($c->country));
                    $objWorksheet->getCell('C'.$no)->setValue( $sent_amount->sent_amount_usd);
                    $objWorksheet->getCell('D'.$no)->setValue($transfer->received_amount_usd);
                    $objWorksheet->getCell('E'.$no)->setValue($cashout->received_amount_usd);
                    $objWorksheet->getCell('F'.$no)->setValue($pending->received_amount_usd);
                    $objWorksheet->getCell('G'.$no)->setValue($comm->commission_usd);
                    $objWorksheet->getCell('H'.$no)->setValue($c->balance);

                    $no++;
                endforeach;

                break;


            case 'teller_report':

                //echo $type;

                //this is the title of the report
                $objWorksheet->getCell('A1')->setValue('Name');
                $objWorksheet->getCell('B1')->setValue('Branch');
                $objWorksheet->getCell('C1')->setValue('TT Transfer(USD)');
                $objWorksheet->getCell('D1')->setValue('TT Cashout');
                $objWorksheet->getCell('E1')->setValue('TT Commission');



                $period = array(

                    'created_on >=' => $first_day,
                    'created_on <=' => $last_day
                );

                $this->db->select('a.id,a.full_name,a.branch_id,branch_name')->from('users a')->join('branch b','a.branch_id=b.id');
                $this->session->userdata('user_type') == 6 ? $this->db->where(array('a.branch_id' => $this->session->userdata('branch_id'))) : '';
                $qry = $this->db->where(array('user_type != ' => '5'))->get()->result();

                $no=2;

                foreach($qry as $c):

                    $transfer=$this->db->select_sum('sent_amount_usd')->from('transactions')->where( $period )->where(array( 'created_by'=>$c->id))->get()->row();
                    $cashout=$this->db->select_sum('received_amount_usd')->from('transactions')->where($period)->where(array('status'=>'cashed_out','updated_by'=>$c->id))->get()->row();
                    $comm=$this->db->select_sum('commission_usd')->from('transactions')->where($period)->where('created_by',$c->id)->get()->row();


                    $objWorksheet->getCell('A'.$no)->setValue(isset($c->full_name)?ucwords($c->full_name):'N/A');
                    $objWorksheet->getCell('B'.$no)->setValue(humanize($c->branch_name));
                    $objWorksheet->getCell('C'.$no)->setValue( $transfer->sent_amount_usd);
                    $objWorksheet->getCell('D'.$no)->setValue($cashout->received_amount_usd);
                    $objWorksheet->getCell('E'.$no)->setValue($comm->commission_usd);

                    $no++;
                endforeach;

                break;


        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $objWriter->save($dir = $path . $type . date('d-m-Y', time()) . '_' . time() . '.xls');

        // force_download($dir, NULL);

        redirect(base_url($dir));



    }

    public function import($type = null)
        {

            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;
            $data = array(
                'title' => 'transfer',
                'subtitle' => $type,
                'link_details' => 'Account overview',
                'page_level' => $page_level

            );
            $this->load->helper('download');
            $this->load->view($page_level . 'header', $data);
            switch ($type) {
                case 'transactions':
                    // this is the function which uploads the profile image
                    $this->form_validation->set_rules('file_upload', 'File', 'xss_clean|trim')->set_rules('pa', 'Pas', 'xss_clean|trim');
                    //this is the company name


                    //managing of the images
                    $path = $config['upload_path'] = './uploads/imports/' . $type . '/';
                    $config['allowed_types'] = 'xls|xlsx|csv|XLS|XLSX|CSV';
                    $config['max_size'] = '200';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }
                    if ($this->form_validation->run() == true && $this->upload->do_upload('file_upload') == true) {

                        $values = array(
                            'path' => $path . $this->upload->file_name,
                            'file_name' => $this->upload->file_name,
                            'file_type' => $this->upload->file_type,
                            'file_ext' => $this->upload->file_ext,
                            'file_size' => $this->upload->file_size,
                            'original_name' => $this->upload->orig_name,
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')
                        );
                        $this->db->insert('file_uploads', $values);

                        //ignore_user_abort(true); set_time_limit(0);
                        ///////////// this is the begining of the extraction of the files///////////////////
                        $excel = $this->extract_excel_rows($path . $this->upload->file_name);

                        $data['message'] = $this->upload->file_name . ' File is being uploaded Please Wait ';
                        $no = 0;
                        if (strlen(trim($excel[2]['A'])) != 14) {
                            $data['alert'] = 'danger';
                            $data['message'] = 'The file you are Uploading is not corresponding with the required file try again !!!
                        <br/> <b>Transaction Id(' . $excel[2]['A'] . ') is MUST be of 14 Characters </b> ' . anchor($root . 'transactions', 'Upload New');
                            $this->load->view('alert', $data);
                        }
                        else {


                            foreach ($excel as $col => $row) {
                                if ($col > 1) {

                                    if ($row['A'] != '' && $row['B'] != '' && $row['C'] != '' && $row['D'] != '' && $row['E'] != '' && $row['F'] != '' && $row['G'] != '' && $row['N'] != '' && $row['J'] != '' && $row['K'] != '' && $row['M'] != '') {

                                        $record_check = $this->db->select('secret_code')->from('transactions')->where(array('secret_code' => $row['A']))->get()->row();

                                        if (!isset($record_check->secret_code)) {

                                            //// this is the end of the default transactions////////////


                                            // this is where original amount is converted to usd
                                            $in_usd = ($this->forex_rate($row['F'], 'USD')) * $row['E'];


                                            //checking for the amount above 5000 usd
                                            $above_5000 = ($in_usd) > 5000 ? 'not_approved' : 'pending';


                                            // this is the one which adds the expiry date
                                            $expiry_date = date('Y-m-d', strtotime('+1 years'));

                                            // selecting for the ID
                                            $user_id = $this->getting_userid($row['B']);


                                            //Asecret_code	Bsender_name	Ccurrency	Drecipient_currency	Eforex_rate	Fsent_amount	Gcommission	Hother_charges	Ireceived_amount	Jreceiver_country	Kreceiver_street_address	Lreceiver_name	Mreceiver_email	Nreceiver_phone	Oreceiver_gender

                                            $receiver_branch = $this->db->select('id')->from('branch')->where('branch_code', $row['K'])->get()->row();
                                            $trans = array(
                                                'transaction_id' => $this->transaction_id('N'),
                                                'secret_code' => strtoupper(trim($row['A'])),
                                                'sender_id' => $user_id,
                                                'currency' => $row['C'],
                                                'recipient_currency' => $row['D'],
                                                'forex_rate' => $row['E'],
                                                'sent_amount' => $row['F'],
                                                'commission' => $row['G'],
                                                'other_charges' => $row['H'],
                                                'received_amount' => $row['I'],
                                                'receiver_country' => $row['J'],
                                                'receiver_branch' => isset($receiver_branch->id) ? $receiver_branch->id : 0,
                                                'receiver_name' => $row['L'],
                                                'receiver_email' => $row['M'],
                                                'receiver_phone' => $row['N'],
                                                'receiver_gender' => $row['O'],
                                                'created_on' => time(),
                                                'created_by' => $this->session->userdata('id'),
                                                'expiry_date' => strtotime($expiry_date),
                                                'status' => $above_5000,
                                                'sent_amount_usd' => $in_usd,
                                                'sender_country' => $this->session->userdata('country'),
                                                'transfer_type' => 'teller',
                                                'branch_id' => $this->session->userdata('branch_id')
                                            );

                                            $this->db->insert('transactions', $trans);

                                            $data['message'] = 'Data has been extracted Successfully ' . anchor($root . 'transactions', 'Upload New');
                                            $data['alert'] = 'success';
                                            $this->load->view('alert', $data);


                                        }
                                        else {
                                            $data['message'] = '<b>' . trim($row['A']) . ' </b>This secret Code has already been Used';
                                            $data['alert'] = 'danger';
                                            $this->load->view('alert', $data);
                                        }
                                    }
                                    else {
                                        $data['message'] = 'Please Fill in all the required Fields in the Excel Sheet  !!! ' . anchor($root . 'transactions', 'Upload New');
                                        $data['alert'] = 'danger';
                                        $this->load->view('alert', $data);
                                    }
                                    $no++;
                                }

                            }
                            ////at this point a record is inserted into the results status
                            //$this->results_status($excel[2]['C'], $excel[2]['A'], $level);
                            ///this is the end of the results status

                        }


                        ///message for the successfully upload and extraction of the file
                        $data['message'] = '1.File has been Uploaded Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);

                    }
                    else {
                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($this->page_level . $this->page_level2 . 'transactions', $data);

                    }
                    $this->load->view($root . 'file_uploads', $data);

                    break;
                default:

                    break;

            }

            $this->load->view($page_level . 'footer_table', $data);
        }



    /**
     * This is where all  the message applies
     * sent message
     * received message
     * inbox
     * templates
     **/
    public function messaging($type = null, $id = null)
        {

            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;


            switch ($type) {

                default:
                    $this->load->view($this->page_level . 'header', $data);
                    $this->load->view($root . 'messaging');
                    break;

                case 'app_inbox_reply':
                    $this->load->view($root . $type);
                    break;
                case 'inbox':

                    if (strlen($id) > 0) {
                        if ($id == 'Emails') {
                            $message_group = 'Email';
                        }
                        elseif ($id == 'SMS') {
                            $message_group = 'SMS';
                        }
                    }


                    $this->db->select()->from('inbox');

                    isset($message_group) ? $this->db->where(array('m_type' => $message_group)) : '';

//                    $this->session->userdata('user_type')==1?'':$this->db->where(array('company_id'=>$this->session->company));

                    $data['message'] = $this->db->order_by('id', 'desc')->limit(25)->get()->result();

                    $this->load->view($root . $type, $data);
                    break;
                case 'outbox':

                    if (strlen($id) > 0) {
                        if ($id == 'Emails') {
                            $message_group = 'Email';
                        }
                        elseif ($id == 'SMS') {
                            $message_group = 'SMS';
                        }
                    }


                    $this->db->select()->from('outbox');

                    isset($message_group) ? $this->db->where(array('m_type' => $message_group)) : '';

//                    $this->session->user_type==1?'':$this->db->where(array('company_id'=>$this->session->company));

                    $data['message'] = $this->db->order_by('id', 'desc')->limit(25)->get()->result();

                    $this->load->view($root . $type, $data);
                    break;
                case 'app_inbox_compose':
                    $this->load->view($root . $type);
                    break;
                case 'app_inbox_view':
                    $data['message_id'] = $id;//$this->input->get('message_id');//$_GET['message_id'];
                    $file_name=$this->uri->segment(5);;
                    $file="app_".$file_name."_view";
                    $this->load->view($root . $file, $data);
                    break;


            }


            // $this->load->view($this->page_level . 'footer', $data);

        }


    //this is the function for the users

    function extract_excel_rows($file_and_dir)
        {
            $this->load->view('php-excel-reader/Classes/PHPExcel');
            $objPHPExcel = PHPExcel_IOFactory::load($file_and_dir);
            $objPHPExcel->setActiveSheetIndex(0);
            $rows = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            return $rows;
        }

    function getting_userid($full_name)
        {
            if (strlen($full_name) > 0) {
                $values = array(
                    'full_name' => $full_name,
                    'branch_id' => $this->session->userdata('branch_id'),
                    'user_type' => '5',
                    'country' => $this->session->userdata('country')
                );

                $user = $this->db->select('id')->from('users')->where($values)->get()->row();
                if (isset($user->id)) {
                    return $user->id;
                }
                else {
                    $values = array(
                        'full_name' => $full_name,
                        'username' => $full_name,
                        'branch_id' => $this->session->userdata('branch_id'),
                        'city' => $this->session->userdata('city'),
                        'country' => $this->session->userdata('country'),
                        'password' => $this->hashValue('123'),
                        'created_by' => $this->session->userdata('id'),
                        'user_type' => '5'

                    );
                    $this->db->insert('users', $values);
                    $user = $this->db->select('id')->from('users')->where($values)->get()->row();
                    if (isset($user->id)) {
                        return $user->id;
                    }
                    else {
                        return 0;
                    }

                }

            }
            else {
                return 0;
            }
        }

    function user()
        {

            //id, full_name, city, password, username, region, country, phone, email, gender
            $user = $data['user'] = strlen($this->input->post('fin')) == 0 ? '' :
                $this->db->select()
                    ->where('user_type', 5)
                    ->or_like(array('full_name' => $this->input->post('fin')))
                    ->from('users')->get()->row();
            isset($user->full_name) ?
                $this->load->view($this->page_level . 'users/auto_load', $data) :
                $this->load->view($this->page_level . 'users/auto_load_empty', $data);


            ?>


            <?php


        }

//this is the function for sending a message

    function branch_exist($branch, $state)
        {
            $this->db->where(array('branch_name' => $branch, 'state' => $state))->from('branch');
            $result = $this->db->count_all_results();
            if ($result == 0) {

                return true;

            }
            else {
                $this->form_validation->set_message('branch_exist', 'You can not enter %s Twice, Check the branch and City ');
                return false;
            }
        }

    public function settings($type = null, $id = null)
        {
            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;

            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'link_details' => 'Account overview',
                'page_level' => $page_level
            );

            $this->load->view($page_level . 'header', $data);

            switch ($type) {
                //////////////////////// This is the part for the countries ///////////////////////////////////////
                case 'countries':
                    $this->load->view($root . 'countries', $data);
                    break;

                case 'new_country':
                    $this->form_validation->set_rules('country', 'Country', 'xss_clean|trim|is_unique[selected_countries.a2_iso]');
                    //checking if the form validation is passed
                    if ($this->form_validation->run() == true) {
                        $c = $this->db->select()->from('country')->where('a2_iso', $this->input->post('country'))->get()->row();
//                    country, a2_iso, a3_un, num_un, dialing_code, created_by, created_on, a2_iso, id
                        if (isset($c->country)) {
                            $values = array(
                                'country' => $c->country,
                                'a2_iso' => $c->a2_iso,
                                'a3_un' => $c->a3_un,
                                'num_un' => $c->num_un,
                                'dialing_code' => $c->dialing_code,
                                'created_by' => $this->session->userdata('id'),
                                'created_on' => time()

                            );
                            $this->db->insert('selected_countries', $values);

                            $data['message'] = 'Record has been unblocked from accessing the System successfully';
                            $data['alert'] = 'success';
                            $this->load->view('alert', $data);
                        }
                        else {
                            $data['message'] = 'An error occurred during selection of the countries ' . anchor($this->page_level . $this->page_level2 . 'new_country', ' <i class="fa fa-refresh"></i> Try again', 'class="btn green-jungle btn-sm"');
                            $data['alert'] = 'warning';
                            $this->load->view('alert', $data);
                        }

                    }
                    else {


                        $this->load->view($root . 'new_country', $data);
                    }

                    $this->load->view($root . 'countries', $data);
                    break;

                //blocking the country
                case 'ban_country':
                    if ($this->db->where('a2_iso', $id)->update('selected_countries', array('status' => '0'))) {
                        $this->db->where('country', $id)->update('branch', array('country_status' => '0'));
                        $data['message'] = 'Country has Been blocked and its Child Branches';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                    }


                    $this->load->view($root . 'countries', $data);
                    break;

                //this is the function for unblocking
                case 'unblock_country':
                    if ($this->db->where('a2_iso', $id)->update('selected_countries', array('status' => '1'))) {
                        $this->db->where('country', $id)->update('branch', array('country_status' => '1'));
                        $data['message'] = 'Country has unblocked and its Child Branches';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                    }
                    $this->load->view($root . 'countries', $data);
                    break;
                //deleting_country
                case 'delete_country':
                    $this->db->where(array('a2_iso' => $id))->delete('selected_countries');
                    $data['message'] = 'Country has Deleted successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'countries', $data);
                    break;

                //////////// this is the end of the part for the countries//////////////////////

                /////////////////// this is the part for the branches//////////////////////////
                case 'branches':
                    $this->load->view($root . 'branches', $data);
                    break;

                case 'new_branch':
                    $this->form_validation
                        ->set_rules('country', 'Country', 'xss_clean|trim|required')
                        ->set_rules('company', 'Branch Company', 'xss_clean|trim|required')
                        ->set_rules('city', 'City', 'xss_clean|trim|required')
                        ->set_rules('branch_name', 'Branch Name', 'xss_clean|trim|required|callback_branch_exist[' . $this->input->post('city') . ']')
                        ->set_rules('telephone', 'Branch Telephone', 'xss_clean|trim|required')
                        ->set_rules('street_address', 'Street Address', 'xss_clean|trim')
                        ->set_rules('branch_code', 'Branch Code', 'xss_clean|trim|required|is_unique[branch.branch_code]');
                    //checking if the form validation is passed
                    if ($this->form_validation->run() == true) {

//id, branch_name, country, state, street, phone, website, others, created_by, created_on, updated_by, updated_on, id, id
                        $country_status = $this->db->select('status')->from('selected_countries')->where('a2_iso', $this->input->post('country'))->get()->row();
                        $c_status = isset($country_status->status) ? $country_status->status : 1;
                        $values = array(
                            'country' => $this->input->post('country'),
                            'company' => $this->input->post('company'),
                            'branch_name' => $this->input->post('branch_name'),
                            'state' => $this->input->post('city'),
                            'street' => $this->input->post('street_address'),
                            'phone' => $this->input->post('telephone'),
                            'country_status' => $c_status,
                            'branch_code' => strtoupper($this->input->post('branch_code')),
                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()

                        );
                        $this->db->insert('branch', $values);

                        $data['message'] = 'Branch has been added successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);


                    }
                    else {

                        $this->load->view($root . 'new_branch', $data);
                    }

                    $this->load->view($root . 'branches', $data);
                    break;

                case 'edit_branch':
                    $id = $data['id'] = $id / date('Y');
                    $this->form_validation
                        ->set_rules('country', 'Country', 'xss_clean|trim|required')
                        ->set_rules('company', 'Branch Company', 'xss_clean|trim|required')
                        ->set_rules('city', 'City', 'xss_clean|trim|required')
                        ->set_rules('branch_name', 'Branch Name', 'xss_clean|trim|required')
                        ->set_rules('telephone', 'Branch Telephone', 'xss_clean|trim|required')
                        ->set_rules('street_address', 'Street Address', 'xss_clean|trim')
                        ->set_rules('branch_code', 'Branch Code', 'xss_clean|trim|required');
                    //checking if the form validation is passed
                    if ($this->form_validation->run() == true) {

//id, branch_name, country, state, street, phone, website, others, created_by, created_on, updated_by, updated_on, id, id
                        $country_status = $this->db->select('status')->from('selected_countries')->where('a2_iso', $this->input->post('country'))->get()->row();
                        $c_status = isset($country_status->status) ? $country_status->status : 1;
                        $values = array(
                            'country' => $this->input->post('country'),
                            'company' => $this->input->post('company'),
                            'branch_name' => $this->input->post('branch_name'),
                            'state' => $this->input->post('city'),
                            'street' => $this->input->post('street_address'),
                            'phone' => $this->input->post('telephone'),
                            'country_status' => $c_status,
                            'branch_code' => strtoupper($this->input->post('branch_code')),
                            'updated_by' => $this->session->userdata('id'),
                            'updated_on' => time()

                        );
                        $this->db->where('id', $id)->update('branch', $values);

                        $data['message'] = 'Branch has been Updated successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);


                    }
                    else {

                        $this->load->view($root . 'edit_branch', $data);
                    }

                    $this->load->view($root . 'branches', $data);
                    break;
                //blocking branches from accessing the system
                case 'block_branch':
                    $id = $data['id'] = $id / date('Y');
                    $this->db->where('id', $id)->update('branch', array('status' => 'blocked'));
                    $data['message'] = 'Country has Been blocked';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'branches', $data);
                    break;

                //this is the function for unblocking
                case 'unblock_branch':
                    $id = $data['id'] = $id / date('Y');
                    $this->db->where('id', $id)->update('branch', array('status' => 'active'));
                    $data['message'] = 'Country has unblocked';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'branches', $data);
                    break;
                //deleting_branches
                case 'delete_branch':
                    $id = $data['id'] = $id / date('Y');
                    $this->delete($id, 'branch');
                    $data['message'] = 'Country has Deleted successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'branches', $data);
                    break;

                //////// This is the end of part for branches/////////////////////
                /////////////// this is the section for currency //////////////////

                case 'currency':
                    //this is the default converstion for the currency
                    //$currency=$this->db->select('currency_alphabetic_code')->from('country')->where('a2_iso',$this->session->userdata('country'))->get()->row();
                    // $currency=$currency->currency_alphabetic_code;
                    //$data['usd']=$this->convert(1,'USD',$currency);
                    //$data['gbp']=$this->convert(1,'GBP',$currency);
                    //$data['eur']=$this->convert(1,'EUR',$currency);
                    //$data['currency']=$currency;

                    $this->form_validation
                        ->set_rules('amount', 'Amount', 'xss_clean|trim|required')
                        ->set_rules('from', 'From', 'xss_clean|trim|required')
                        ->set_rules('to', 'To', 'xss_clean|trim|required');
                    if ($this->form_validation->run() == true) {
                        $amount = $data['amount'] = $this->input->post('amount');
                        $from = $data['from'] = $this->input->post('from');
                        $data['to'] = $to = $this->input->post('to');
                        //this is for google
                        // $data['output']=$this->convertCurrency($amount,$from,$to);
                        //this is for yahoo
                        $data['output2'] = $this->convert($amount, $from, $to);
                    }
                    $this->load->view('currency_converter/currency_converter', $data);
                    break;

                case 'forex_rate':

                    $this->load->view($root . 'forex_rates', $data);
                    break;

                case 'add_forex_rate':
                    $this->form_validation
                        ->set_rules('amount', 'Amount', 'xss_clean|trim|required')
                        ->set_rules('reverse_amount', 'Reverse Amount', 'xss_clean|trim|required')
                        ->set_rules('from', 'From', 'xss_clean|trim|required')
                        ->set_rules('to', 'To', 'xss_clean|trim|required|callback_currency_check')
                        ->set_rules('country', 'Country', 'xss_clean|trim|required');

                    if ($this->form_validation->run() == true) {


                        //entering the forex rates in the database
                        $values = array(
                            'from_currency' => $this->input->post('from'),
                            'to_amount' => $this->input->post('amount'),
                            'to_currency' => $to = $this->input->post('to'),
                            'country' => $this->input->post('country'),
                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()
                        );

                        $reverse_values = array(
                            'from_currency' => $this->input->post('to'),
                            // 'to_amount' => 1 / $this->input->post('amount'),
                            'to_amount' => $this->input->post('reverse_amount'),

                            'to_currency' => $to = $this->input->post('from'),
                            'country' => $this->input->post('country'),
                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()
                        );

                        $this->db->insert('forex_settings', $values);
                        $this->db->insert('forex_settings', $reverse_values);
                        $data['message'] = 'Exchange Rate Set settings has been entered Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        // $this->load->view($root.'forex_rates',$data);

                    }
                    else {
                        $this->load->view($root . 'add_forex_rate', $data);
                    }
                    $this->load->view($root . 'forex_rates', $data);

                    break;


                case 'edit_exchange_rate':
                    $id = $data['id'] = $id / date('Y');
                    $this->form_validation
                        ->set_rules('amount', 'Amount', 'xss_clean|trim|required')
                        ->set_rules('from', 'From', 'xss_clean|trim|required')
                        ->set_rules('to', 'To', 'xss_clean|trim|required')
                        ->set_rules('country', 'Country', 'xss_clean|trim|required');
                    if ($this->form_validation->run() == true) {


                        //entering the forex rates in the database
                        $values = array(
                            'from_currency' => $this->input->post('from'),
                            'to_amount' => $this->input->post('amount'),
                            'to_currency' => $to = $this->input->post('to'),
                            'country' => $this->input->post('country'),
                            'updated_by' => $this->session->userdata('id'),
                            'updated_on' => time()
                        );
                        $this->db->where('id', $id)->update('forex_settings', $values);
                        $data['message'] = 'Exchange Rate Set settings has been Updated Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        // $this->load->view($root.'forex_rates',$data);

                    }
                    else {
                        $this->load->view($root . 'edit_exchange_rate', $data);
                    }
                    $this->load->view($root . 'forex_rates', $data);

                    break;

                //this is the function for deleting exchange rate
                case 'delete_exchange_rate':
                    $id = $data['id'] = $id / date('Y');
                    if ($this->delete($id, 'forex_settings') == true) {
                        $data['message'] = 'Exchange Rate Set settings has been Deleted Successfully';
                        $data['alert'] = 'success';
                    }
                    else {
                        $data['message'] = 'An Error has Occurred while deleting a record';
                        $data['alert'] = 'danger';
                    }
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'forex_rates', $data);
                    break;
                //this is the editing of the exhange rate

                ////////////// this is the end of section for the currency////////////////////
                ///////////////This is the Section for th Commission of the System///////////////////////
                case 'commission':
                    $this->load->view($root . 'commission/commission', $data);
                    break;

                case 'add_commission':
                    $this->form_validation
                        ->set_rules('country', 'Country', 'xss_clean|trim|required')
                        ->set_rules('branch', 'Branch', 'xss_clean|trim|required')
                        ->set_rules('unit_value', 'Value', 'xss_clean|trim|required|greater_than[-1]')
                        ->set_rules('type', 'Type', 'xss_clean|trim|required')
                        ->set_rules('units', 'Units', 'xss_clean|trim|required')
                        ->set_rules('min', 'Maximum Amount', 'xss_clean|trim|required|greater_than_equal_to[-1]')
                        ->set_rules('max', 'Min Amount', 'xss_clean|trim|required|callback_max_min');
                    if ($this->form_validation->run() == true) {


                        //entering the forex rates in the database
                        $values = array(
                            'country' => $this->input->post('country'),
                            'branch_id' => $this->input->post('branch'),
                            'type' => $this->input->post('type'),
                            'units' => $this->input->post('units'),
                            'unit_value' => $this->input->post('unit_value'),
                            'min' => $this->input->post('min'),
                            'max' => $this->input->post('max') == '*' ? 10000000 : $this->input->post('max'),


                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()
                        );
                        $this->db->insert('commission', $values);
                        $data['message'] = 'Commission has been entered Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        // $this->load->view($root.'forex_rates',$data);

                    }
                    else {
                        $this->load->view($root . 'commission/add_commission', $data);
                    }
                    $this->load->view($root . 'commission/commission', $data);

                    break;

                case 'edit_commission':
                    $id = $data['id'] = $id / date('Y');

                    $this->form_validation
                        ->set_rules('country', 'Country', 'xss_clean|trim|required')
                        ->set_rules('branch', 'Branch', 'xss_clean|trim|required')
                        ->set_rules('unit_value', 'Value', 'xss_clean|trim|required|greater_than[0]')
                        ->set_rules('type', 'Type', 'xss_clean|trim|required')
                        ->set_rules('units', 'Units', 'xss_clean|trim|required')
                        ->set_rules('min', 'Maximum Amount', 'xss_clean|trim|required|greater_than_equal_to[-1]')
                        ->set_rules('max', 'Min Amount', 'xss_clean|trim|required|callback_max_min');
                    if ($this->form_validation->run() == true) {


                        //entering the forex rates in the database
                        $values = array(
                            'country' => $this->input->post('country'),
                            'branch_id' => $this->input->post('branch'),
                            'type' => $this->input->post('type'),
                            'units' => $this->input->post('units'),
                            'unit_value' => $this->input->post('unit_value'),
                            'min' => $this->input->post('min'),
                            'max' => $this->input->post('max') == '*' ? 10000000 : $this->input->post('max'),

                            'updated_by' => $this->session->userdata('id'),
                            'updated_on' => time()
                        );
                        $this->db->where('id', $id)->update('commission', $values);
                        $data['message'] = 'Commission has been Updated Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        // $this->load->view($root.'forex_rates',$data);

                    }
                    else {
                        $this->load->view($root . 'commission/edit_commission', $data);
                    }

                    $this->load->view($root . 'commission/commission', $data);
                    break;

                case 'delete_commission':
                    $id = $data['id'] = $id / date('Y');
                    if ($this->delete($id, 'commission') == true) {
                        $data['message'] = 'Commission has been Deleted Successfully';
                        $data['alert'] = 'success';
                    }
                    else {
                        $data['message'] = 'An Error has Occurred while deleting a record';
                        $data['alert'] = 'danger';
                    }
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'commission/commission', $data);
                    break;
                ///////////// This is the end of section for commission/////////////////////////////////

                ///////////////This is the Section for th Taxes of the System///////////////////////
                case 'taxes':
                    $this->load->view($root . 'taxes/taxes', $data);
                    break;

                case 'add_taxes':
                    $this->form_validation
                        ->set_rules('country', 'Country', 'xss_clean|trim|required')
//                        ->set_rules('branch', 'Branch', 'xss_clean|trim|required')
                        ->set_rules('name', 'name', 'xss_clean|trim|required|callback_tax_name')
                        ->set_rules('type', 'type', 'xss_clean|trim|required')
                        ->set_rules('unit_value', 'Value', 'xss_clean|trim|required|greater_than[-1]')
                        ->set_rules('taxed', 'Taxed', 'xss_clean|trim|required')
                        ->set_rules('units', 'Units', 'xss_clean|trim|required');

                    if ($this->form_validation->run() == true) {


                        //entering the forex rates in the database
                        $values = array(
                            'country' => $this->input->post('country'),
                            'name' => $this->input->post('name'),
                            'type' => $this->input->post('type'),
                            'taxed' => $this->input->post('taxed'),
                            'units' => $this->input->post('units'),
                            'unit_value' => $this->input->post('unit_value'),


                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()
                        );
                        $this->db->insert('taxes', $values);
                        $data['message'] = 'Tax has been entered Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        // $this->load->view($root.'forex_rates',$data);

                    }
                    else {
                        $this->load->view($root . 'taxes/add_taxes', $data);
                    }
                    $this->load->view($root . 'taxes/taxes', $data);

                    break;

                case 'edit_taxes':
                    $id = $data['id'] = $id / date('Y');

                    $this->form_validation
                        ->set_rules('country', 'Country', 'xss_clean|trim|required')
//                        ->set_rules('branch', 'Branch', 'xss_clean|trim|required')
                        ->set_rules('name', 'name', 'xss_clean|trim|required|callback_tax_name')
                        ->set_rules('name', 'name', 'xss_clean|trim|required')
                        ->set_rules('unit_value', 'Value', 'xss_clean|trim|required|greater_than[-1]')
                        ->set_rules('taxed', 'Taxed', 'xss_clean|trim|required')
                        ->set_rules('units', 'Units', 'xss_clean|trim|required');

                    if ($this->form_validation->run() == true) {


                        //entering the forex rates in the database
                        $values = array(
                            'country' => $this->input->post('country'),
                            'name' => $this->input->post('name'),
                            'type' => $this->input->post('type'),
                            'taxed' => $this->input->post('taxed'),
                            'units' => $this->input->post('units'),
                            'unit_value' => $this->input->post('unit_value'),

                            'updated_by' => $this->session->userdata('id'),
                            'updated_on' => time()
                        );
                        $this->db->where('id', $id)->update('taxes', $values);
                        $data['message'] = 'Tax has been Updated Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        // $this->load->view($root.'forex_rates',$data);

                    }
                    else {
                        $this->load->view($root . 'taxes/edit_taxes', $data);
                    }

                    $this->load->view($root . 'taxes/taxes', $data);
                    break;

                case 'delete_taxes':
                    $id = $data['id'] = $id / date('Y');
                    if ($this->delete($id, 'taxes') == true) {
                        $data['message'] = 'Commission has been Deleted Successfully';
                        $data['alert'] = 'success';
                    }
                    else {
                        $data['message'] = 'An Error has Occurred while deleting a record';
                        $data['alert'] = 'danger';
                    }
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'taxes/taxes', $data);
                    break;
                ///////////// This is the end of section for Taxes/////////////////////////////////

                default:
                    $this->load->view($page_level . 'reports/reports', $data);
                    break;
            }

            $this->load->view($page_level . 'footer_table', $data);

        }

    function convert($currency_input, $currency_from, $currency_to)
        {
            $yql_base_url = "http://query.yahooapis.com/v1/public/yql";
            $yql_query = 'select * from yahoo.finance.xchange where pair in ("' . $currency_from . $currency_to . '")';
            $yql_query_url = $yql_base_url . "?q=" . urlencode($yql_query);
            $yql_query_url .= "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
            $yql_session = curl_init($yql_query_url);
            curl_setopt($yql_session, CURLOPT_RETURNTRANSFER, true);
            $yqlexec = curl_exec($yql_session);
            $yql_json = json_decode($yqlexec, true);
            $currency_output = (float)$currency_input * $yql_json['query']['results']['rate']['Rate'];

            return $currency_output;
        }

    function tax_name($str)
        {


            $str = $str;
            $country = $this->input->post('country');
            $type = $this->input->post('type');

            $tax = $this->db->where(array('name' => $str, 'country' => $country, 'type' => $type))->from('taxes')->count_all_results();

            if ($tax == 1) {
                $this->form_validation->set_message('tax_name', ' The %s already exista');
                return false;
            }
            else {
                return true;
            }

        }

    function max_min($str)
        {


            $max = $str;
            $min = $this->input->post('min');
            if (($max < $min) && $max != '*') {
                $this->form_validation->set_message('max_min', ' The %s cannot be greater than the Maximum Amount (' . $min . ' - ' . $max . ')');
                return false;
            }
            else {
                return true;
            }

        }

//this is the function for sending a message

    function phone($phone)
    {
        if (strstr($phone, '+')) {
            $phone = substr($phone, 1);
            $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
            return $mobile;
        } else {
            $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
            return $mobile;
        }
    }

    function code($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function SendSMS($sender, $destination, $message, $type = '')
    {
        $email = 'denis@timesolut.com';
        $password = 'shawnluvu';
        $url = 'http://caltonmobile.com/calton/api.php?';
        $parameters = 'username=[EMAIL]&password=[PASSWORD]&contacts=[DESTINATION]&message=[MESSAGE]&sender=[SENDERID]';
        $parameters = str_replace('[EMAIL]', $email, $parameters);
        $parameters = str_replace('[PASSWORD]', urlencode($password), $parameters);
        $parameters = str_replace('[DESTINATION]', $destination, $parameters);
        $parameters = str_replace('[MESSAGE]', urlencode($message), $parameters);
        $parameters = str_replace('[SENDERID]', urlencode($sender), $parameters);
        $post_url = $url . $parameters;
        $response = file($post_url);
        $this->db->insert('outbox_messages', array(
            'created' => time(),
            'sender' => $sender,
            'receiver' => $destination,
            'message' => $message,
            'type' => $type,
            'mobile' => $this->session->userdata('mobile')

        ));
        return $response[0];
    }

    public function getNetwork($mobile)
    {
        $substring = substr($mobile, 0, 5);

        if ($substring == "25678" || $substring == "25677" || $substring == "25639") {
            return "MTN_UGANDA";

        } else if ($substring == "25675") {

            return "AIRTEL_UGANDA";

        } else if ($substring == "25670") {
            return "WARID_UGANDA";

        } else if ($substring == "25679") {

            return "ORANGE_UGANDA";

        } else if ($substring == "25671") {

            return "UTL_UGANDA";
        } else {
            return "RWANDACELL";
        }
    }

    public function gettoken()
    {
        $token = 'VIRTU' . $this->GetSMSCode(6);
        while (1) {
            if ($this->db->select('id')->from('top_up_transactions')->where(array('token' => $token))->get()->num_rows() == 0) {
                break;
            } else {
                $token = $this->GetSMSCode(6);
            }

        }

        return $token;
    }

    function pass_check($str)
    {
        $pass = $this->db->select('password')->from('users')->where(array('username' => $this->session->userdata('username'), 'password' => $this->hashValue($str)))->get()->row();
        if (!isset($pass->password)) {
            $this->form_validation->set_message('pass_check', ' The %s is incorrect Please type a Correct Password ');
            return false;
        } else {
            return true;
        }
    }

    //this is the email verification

    function code_check($str)
    {
        $code = $this->db->where(array('user_id' => $this->session->userdata('id'), 'code' => $str))->from('account_verification')->select('code')->get()->row();
        if (isset($code->code)) {
            return true;
        } else {
            $this->form_validation->set_message('code_check', 'The Verification Code you Provided is Wrong');
            return false;
        }

    }

    function email_verification()
    {

        $data['title'] = 'Email Verification';
        $data['subtitle'] = 'profile';
        $this->load->helper('text');
        //this is the query for the orders this month
        $this_month = strtotime(date('Y-m'));
        $total_order = $this->db->query('SELECT DISTINCT(shopping_code) FROM transactions where created_on >= ' . $this_month);
        $data['to'] = $total_order->result();
        // this is the query for new orders
        $new_orders = $this->db->query('SELECT DISTINCT(shopping_code) FROM transactions where transaction_status ="pending" ');
        $data['no'] = $new_orders->result();
        //this is the number of items which are completed
        $data['completed'] = $this->db->where('transaction_status', 'completed')->from('transactions')->count_all_results();
        $data['users'] = $this->db->count_all('users');
        // this is the begining of form validation
        $this->form_validation->set_rules('code', 'Code', 'xss_clean|required|trim|callback_code_check');
        if ($this->form_validation->run() == true) {
            $this->db->where('id', $this->session->userdata('id'))->update('users', array('verified' => 1));
            $this->session->set_userdata(array('verified' => 1));
            $data['alert'] = 'success';
            $data['message'] = 'Your Account Has been Verified';
            $email_msg = "Dear " . $this->session->userdata('fname') . " " . $this->session->userdata('lname') . " \n\r\n We Thank you for using Our Services\n\r\nYour Account Has Been Verified";
            $this->sendemail($this->session->userdata('email'), 'Email Verification', $email_msg);
        }
        $this->load->view('admin/header', $data);
        $this->load->view('admin/dashboard/dashboard');
        $this->load->view('admin/footer');


    }

    function sendemail($reciever, $subject, $message)
    {

        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['useragent'] = 'Virtual Wallet';
        $config['smtp_host'] = 'www.timesolut.com';
        $config['smtp_user'] = 'no-reply@timesolut.com';
        $config['smtp_pass'] = 'b],PLIa~1y7h';
        $this->email->initialize($config);
        $this->email->from('no-reply@virtualwallet.com', 'Virtual Wallet');
        $this->email->to($reciever);
        $this->email->subject('Virtual Wallet | ' . $subject . ' Time >> ' . strftime("%T", time()));
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        }


    }

    function resend_code()
    {
        $data['title'] = 'email_verification';
        $data['subtitle'] = 'profile';
        $data['message'] = 'Code Has Been sent on your email (' . $this->session->userdata('email') . ')';
        $data['alert'] = 'success';
        $code = $this->db->where('user_id', $this->session->userdata('id'))->select('code')->from('account_verification')->get()->row();
        $email_msg = "Dear " . $this->session->userdata('fname') . " " . $this->session->userdata('lname') . " \n\r\n We Thank you for using Our Services\n\r\n Your Verification Code: " . $code->code;

        $this->sendemail($this->session->userdata('email'), 'Email Verification', $email_msg);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/dashboard/home');
        $this->load->view('admin/footer');

    }


    //This is the profile function

    function update_email()
    {
        $data['title'] = 'update_email';
        $data['subtitle'] = 'profile';
        $this->form_validation->set_rules('email', 'Email', 'xss_clean|trim|required|valid_email|is_unique[users.email]');
        if ($this->form_validation->run() == true) {
            $this->db->where('id', $this->session->userdata('id'))->update('users', array('email' => $this->input->post('email')));
            $data['alert'] = 'success';
            $data['message'] = 'The Email Has been updated successfully';
            $this->session->set_userdata(array('email' => $this->input->post('email')));
            header('Refresh: 3; url=' . base_url('index.php/admin/resend_code/'));

        }
        $this->load->view('admin/header', $data);
        $this->load->view('admin/dashboard/home');
        $this->load->view('admin/footer');
    }

    function profile($type = null)
    {
        $data['title'] = $this->uri->segment(2);
        $data['subtitle'] = $type == null ? 'user_profile' : $type;


        $this->load->view($this->uri->slash_segment(1) . 'header', $data);
        if ($type == null) {
            $this->load->view($this->uri->slash_segment(1) . 'profile/user_profile');
        } elseif ($type == 'info') {
            $this->form_validation->set_rules('email', 'Email', 'valid_email|xss_clean|trim')
                ->set_rules('phone', 'Phone', 'xss_clean|trim|max_length[13]|min_length[10]')
                ->set_rules('password', 'Current password', 'xss_clean|trim|callback_pass_check')
                ->set_rules('fullname', 'Full Name', 'xss_clean|trim');
            if ($this->form_validation->run() == false) {

                $this->load->view($this->uri->slash_segment(1) . 'profile/user_profile', $data);
            } else {
                $this->db->where('id', $this->session->userdata('id'))
                    ->update('users', array(
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone'),
                        'full_name' => $this->input->post('fullname')

                    ));
                $values = array(
                    'fullname' => $this->input->post('fullname'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email')
                );


                $this->session->set_userdata($values);
                $data['message'] = 'You have successfully Updated your Profile';
                $data['alert'] = 'success';
                $this->load->view('alert', $data);
                $this->load->view($this->uri->slash_segment(1) . 'profile/user_profile', $data);

            }
        } elseif ($type == 'change_password') {


            $this->form_validation->set_rules('current_password', 'Current password', 'xss_clean|trim|callback_pass_check')->set_rules('new_pass', 'New Password', 'required|trim|xss_clean|matches[rpt_pass]')->set_rules('rpt_pass', 'Repeat Password', 'required|xss_clean|trim');
            if ($this->form_validation->run() == false) {

                $this->load->view($this->uri->slash_segment(1) . 'profile/user_profile', $data);
            } else {
                $this->db->where('id', $this->session->userdata('id'))->update('users', array('password' => $this->hashValue($this->input->post('new_pass'))));
                $data['message'] = 'You have successfully Changed your password <br/>';
                $data['alert'] = 'success';
                $this->load->view('alert', $data);
                $this->load->view($this->uri->slash_segment(1) . 'profile/user_profile', $data);
            }
        } elseif ($type == 'change_avatar') {

            // this is the function which uploads the profile image
            $this->form_validation->set_rules('image', 'Image', 'xss_clean|trim')->set_rules('pa', 'Pas', 'xss_clean|trim');
            //this is the company name

            $cname = $this->session->userdata('fullname');
            //managing of the images
            $path = $config['upload_path'] = './uploads/profile/' . underscore($cname) . '/';
            $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
            $config['max_size'] = '200';
            $config['max_width'] = '1920';
            $config['max_height'] = '850';
            $this->upload->initialize($config);


            if (!is_dir($path)) //create the folder if it's not already exists
            {
                mkdir($path, 0777, TRUE);
            }
            if ($this->form_validation->run() == true && $this->upload->do_upload('image') == true) {

                $values = array(
                    'photo' => $path . $this->upload->file_name,
                    'updated_on' => time(),
                    'updated_by' => $this->session->userdata('id')
                );
                $this->db->where('id', $this->session->userdata('id'))->update('users', $values);
                $this->session->set_userdata(array('photo' => $path . $this->upload->file_name));
                $data['message'] = 'Image has been updated Successfully';
                $data['alert'] = 'success';
                $this->load->view('alert', $data);

            } else {
                $data['error'] = $this->upload->display_errors();
                $this->load->view($this->uri->slash_segment(1) . 'profile/user_profile', $data);

            }

        }
        $this->load->view($this->uri->slash_segment(1) . 'footer_table');
    }



    function currency_converter()
    {
        $data = array(
            'title' => 'currency_converter',
            'subtitle' => '',
            'link_details' => 'Account overview'
        );
        $this->load->view('admin/header', $data);
        $this->form_validation->set_rules('amount', 'Amount', 'xss_clean|trim|required');
        if ($this->form_validation->run() == true) {
            //this is for google
            $data['output'] = $this->convertCurrency($this->input->post('amount'), 'UGX', 'USD');
            //this is for yahoo
            $data['output2'] = $this->convert($this->input->post('amount'), 'UGX', 'USD');
        }
        $this->load->view('admin/currency_converter/currency_converter', $data);
        $this->load->view('admin/footer', $data);
    }

    //This google currency Converter

    function convertCurrency($amount, $from, $to)
    {
        $url = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
        $data = $this->url_get_contents($url);
        // $data = file_get_contents($url);
        preg_match("/<span class=bld>(.*)<\/span>/", $data, $converted);
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
        return round($converted, 3);
    }

    //This is yahoo currency Converter

    function url_get_contents($url)
    {
        if (function_exists('curl_exec')) {
            $conn = curl_init($url);
            curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($conn, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
            $url_get_contents_data = (curl_exec($conn));
            curl_close($conn);
        } elseif (function_exists('file_get_contents')) {
            $url_get_contents_data = file_get_contents($url);
        } elseif (function_exists('fopen') && function_exists('stream_get_contents')) {
            $handle = fopen($url, "r");
            $url_get_contents_data = stream_get_contents($handle);
        } else {
            $url_get_contents_data = false;
        }
        return $url_get_contents_data;
    }

    public function session_logs($method = null, $page = null)
    {
        if ($this->isloggedin()) {
            $data['view'] = 'session_logs';
            $data['sublink'] = 'session_logs';
            $data['link_details'] = 'session_logs';

            $data['page'] = strlen($page) > 0 ? $page : 0;


            $this->load_admin($data);
        } else {
            $this->logout();
        }


    }

    public function sms_routes($method = null, $id = null)
    {
        if ($this->isloggedin()) {
            //$data = array();
            //$data['controller'] = 'sms_routes';
            $data['view'] = 'sms_routes';
            $data['sublink'] = 'sms_routes';
            $data['link_details'] = 'manage_sms_routes';


            switch ($method) {
                case 'delete':


                    $this->db->where(array('id' => $id / date('Y')))->delete('sms_routes');
                    redirect('admin/sms_routes', 'refresh');
                    // echo '<META http-equiv=refresh content=0;URL='.base_url().'index.php/admin/sms_routes/>';
                    break;
                case 'activate': {
                    $this->db->update('sms_routes', array('status' => 0));
                    $this->db->update('sms_routes', array('status' => 1), array('id' => $id / date('Y')));
                }
                    //   echo '<META http-equiv=refresh content=0;URL='.base_url().'index.php/admin/sms_routes/>';
                    redirect('admin/sms_routes', 'refresh');
                    break;

                default: {

                    //   print_r($_REQUEST);
                    if (isset($_REQUEST['route'])) {


                        if ($this->db->select('id')->from('sms_routes')->where('route_name = "' . $_REQUEST['route_name'] . '" or parameters = "' . $_REQUEST['route'] . '"')->get()->num_rows() == 0) {
                            $values = array('route_name' => $_REQUEST['route_name'],
                                'parameters' => $_REQUEST['route'],

                                'created_at' => date('Y-m-d H:i:s'));

                            $data['alert'] = 'alert-info';
                            $data['message'] = 'The SMS route added successfully';

                            $this->db->insert('sms_routes', $values);
                        } else {

                            $data['alert'] = 'alert-danger';
                            $data['message'] = 'The SMS route you are adding already exists';


                        }


                    }


                }
                    break;

            }


            $this->load_admin($data);


        } else {

            $this->logout();
        }


    }



    public function account_settings()
    {
        if ($this->isloggedin()) {

            $data['controller'] = 'account_settings';
            $data['view'] = 'account_settings';
            $data['sublink'] = 'account_settings';
            $data['link_details'] = 'account_settings';

            if (isset($_REQUEST['first_name'])) {
                if ($_REQUEST['newpassword'] == $_REQUEST['newpasswordagain']) {
                    if ($this->db->select('id')->from('users')->where(array('id' => $this->session->userdata('id'), 'password' => sha1($_REQUEST['oldpassword'])))->get()->num_rows() > 0) {
                        $phno = $_REQUEST['mobile'];
                        $mobile = strlen($phno) == 10 ? '256' . substr($phno, 1) : $phno;
                        $values = array('fname' => $_REQUEST['first_name'],
                            'lname' => $_REQUEST['last_name'],
                            'mobile' => $mobile,
                            'password' => sha1($_REQUEST['newpassword']));

                        $this->session->set_userdata($values);

                        $this->db->update('users', $values, array('id' => $this->session->userdata('id')));

                        $data['alert'] = 'alert-info';
                        $data['message'] = 'Account edited successfully, Thank you';
                    } else {
                        $data['alert'] = 'alert-danger';
                        $data['message'] = 'Stop! Wrong password used';

                    }

                } else {
                    $data['alert'] = 'alert-warning';
                    $data['message'] = "Access denied, Passwords didn't match";


                }

            }


            $this->load_admin($data);

        } else {

            $this->logout();

        }


    }

    public function administration($method = null, $id = null)
    {
        if ($this->isloggedin()) {
            $data['view'] = 'administration';
            $data['sublink'] = $method == null ? 'user_accounts' : $method;
            $data['link_details'] = 'Manage User Accounts';
            $data['message'] = '';
            $data['alert'] = '';
            $data['id'] = $id / date('Y');

            switch ($method) {

                case 'block':
                    $this->db->update('users', array('status' => 2), array('id' => $id / date('Y')));
                    redirect('admin/administration/user_profile/' . $id, 'refresh');
                    break;

                case 'unblock':
                    $this->db->update('users', array('status' => 0), array('id' => $id / date('Y')));
                    redirect('admin/administration/user_profile/' . $id, 'refresh');
                    break;
                case 'open':
                    $this->db->update('users', array('status' => 1), array('id' => $id / date('Y')));
                    redirect('admin/administration/user_profile/' . $id, 'refresh');
                    break;

                case 'delete':
                    $id = $id / date('Y');
                    $this->db->where(array('id' => $id))->delete('users');
                    redirect('admin/administration', 'refresh');
                    //$this->change_destination('admin/useraccounts/viewuseraccounts',0);
                    break;

                case 'edit':
                    if (isset($_REQUEST['f_name'])) {
                        $phno = $this->input->post('mobile');
                        $mobile = strlen($phno) == 10 ? '256' . substr($phno, 1) : $phno;
                        $values = array(
                            'f_name' => $this->input->post('f_name'),
                            'l_name' => $this->input->post('l_name'),
                            'mobile' => $mobile,
                            'email' => $this->input->post('email'),
                            'username' => $this->input->post('username'),
                            'password' => $this->input->post('password'),
                            'dept' => $this->input->post('dept')
                        );

                        $this->db->update('users', $values, array('id' => $id / date('Y')));

                    }
                    break;

                case 'add_user':

                    $data['sublink'] = 'add_user_account';
                    $data['details'] = 'Add a new user Account';

                    if (isset($_REQUEST['fname'])) {

                        //$exists = $this->dbmodel->getAttributeFromTable('users','id',array('username'=>$this->input->post('username')));

                        if ($this->db->select('id')->from('users')->where(array('username' => $_REQUEST['username']))->get()->num_rows() == 0) {

                            if ($this->input->post('password') != $this->input->post('repassword')) {
                                $data['alert'] = 'alert-warning';
                                $data['message'] = 'Password mismatch occured';

                            } else {
                                $phno = $this->input->post('mobile');
                                $mobile = strlen($phno) == 10 ? '256' . substr($phno, 1) : $phno;
                                $values = array(
                                    'fname' => $this->input->post('fname'),
                                    'lname' => $this->input->post('lname'),
                                    'mobile' => $mobile,

                                    'username' => $this->input->post('username'),
                                    'password' => sha1($this->input->post('password')),
                                    'role' => 'event_manager',
                                    'account_type' => 'event_manager',
                                    'status' => 1,
                                    'created_at' => date('Y-m-d H:i:s')
                                );

                                $this->db->insert('users', $values);
                                $data['alert'] = 'alert-success';
                                $data['message'] = 'A new user has been added successfully';

                            }
                        } else {
                            $data['alert'] = 'alert-danger';
                            $data['message'] = 'Access denied. User Account already exists';
                        }

                    }
                    break;


            }


            $this->load_admin($data);

        } else {

            $this->logout();
        }

    }

    function reformat_date($date)
    {
        $newDate = explode(' ', $date);

        $date_p = $newDate[0];
        $time = $newDate[1];

        $date_p_splitted = explode('-', $date_p);

        $day = str_pad($date_p_splitted[0], 2, "0", STR_PAD_LEFT);
        $month = $date_p_splitted[1];
        $year = $date_p_splitted[2];

        $date = $year . '-' . $month . '-' . $day . ' ' . $time;

        return $date;
    }

    public function change_destination($destination, $duration = 2)
    {
        echo '<META http-equiv=refresh content=' . $duration . ';URL=' . base_url() . 'index.php/' . $destination . '>';
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */