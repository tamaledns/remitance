
<script>
    var PortletAjax = function () {

        var handlePortletAjax = function () {
            //custom portlet reload handler
            $('#my_portlet .portlet-title a.reload').click(function(e){
                e.preventDefault();  // prevent default event
                e.stopPropagation(); // stop event handling here(cancel the default reload handler)
                // do here some custom work:
                App.alert({
                    'type': 'danger',
                    'icon': 'warning',
                    'message': 'Custom reload handler!',
                    'container': $('#my_portlet .portlet-body')
                });
            })
        }

        return {
            //main function to initiate the module
            init: function () {
                handlePortletAjax();
            }

        };

    }();

    jQuery(document).ready(function() {
        PortletAjax.init();
    });
</script>


<!-- END FOOTER -->
<!--[if lt IE 9]>
<script src="<?php echo base_url() ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/jquery-notific8/jquery.notific8.min.js" type="text/javascript"></script>-->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url() ?>assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>