<div class="scroller" style="height:100px" data-rail-visible="1">

    <table class="table table-striped table-bordered table-hover">

        <thead>
        <tr>

            <th> Min </th>
            <th> Max </th>
            <th>Units</th>
            <th> Unit Value </th>

        </tr>
        </thead>
        <tbody>
        <?php
        $no=1;
        foreach($this->db->select('a.*,b.country')->from('commission a')->join('selected_countries b','a.country=b.a2_iso')->order_by('a.country','asc')->get()->result() as $c): ?>
            <tr>

                <td> <?php echo number_format($c->min) ?> </td>
                <td> <?php echo $c->max ?> </td>
                <td><?php echo $c->units ?></td>
                <td> <?php echo $c->unit_value ?> </td>


            </tr>
            <?php
            $no++;
        endforeach; ?>

        </tbody>
    </table>


</div>
<script>
    jQuery(document).ready(function()
    {
        App.initAjax();
    });
</script>