<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase">Select Recipient &nbsp;</span>
                </div>
                <div class="actions pull-left">

                    <?php echo anchor($this->page_level.'users/new',' <i class="fa fa-plus"></i> New Remitter','class="btn green-jungle btn-sm"'); ?>

                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-scrollable table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th> Name </th>
                        <th> Country </th>
                        <th> Phone </th>
                        <th> Branch </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->db->select('id, full_name, city, country, phone, email, gender,dob,branch_id,user_type,status')->from('users')
                                      ->where(array('id !='=>$this->session->userdata('id'),'user_type'=>5))->get()->result() as $user): ?>
                        <tr>
                            <td>
                                <?php echo anchor($this->page_level.'transfer/new/'.$user->id*date('Y'), ucwords($user->full_name),'title="New Transfer"') ?>

                            </td>

                            <td> <?php
                                $ct= $this->db->select('country')->from('country')->where('a2_iso',$user->country)->get()->row();
                                echo $ct->country ?> </td>


                            <td> <?php echo $user->phone ?> </td>
                            <td> <?php
                                $br=$this->db->select('branch_name')->from('branch')->where('id',$user->branch_id)->get()->row();
                                echo isset($br->branch_name)?$br->branch_name:'N/A' ?> </td>

                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->