<!-- BEGIN DASHBOARD STATS 1-->

<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php

                    $this->db->select_sum('sent_amount_usd');
                    $this->db->where(array('created_on >= '=>strtotime($fdate),'status !='=>'canceled'));
                    isset($last_day)?$this->db->where('created_on <= ',strtotime($last_day)):'';
                    $ts=$this->db->from('transactions')->get()->row();
                    $ts= number_format($ts->sent_amount_usd,2);
                    ?>
                    <span data-counter="counterup" data-value="<?php echo $ts; ?>">0</span>
                </div>
                <div class="desc"> Transfers(USD)  </div>
            </div>
            <?php echo anchor($this->page_level.'transfer','View More   <i class="m-icon-swapright m-icon-white"></i>','class="more"') ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $this->db->select_sum('received_amount_usd');
                    $this->db->from('transactions')->where(array('status'=>'cashed_out'));
                    $this->db->where('created_on >= ',strtotime($fdate));
                    isset($last_day)?$this->db->where('created_on <= ',strtotime($last_day)):'';
                    $ts= $this->db->get()->row();
                    ?>
                    <span data-counter="counterup" data-value="<?php echo number_format($ts->received_amount_usd,2); ?>">0</span>
                </div>
                <div class="desc"> Total Cashouts(USD) </div>
            </div>
            <?php echo anchor($this->page_level.'cashout','View More  <i class="m-icon-swapright m-icon-white"></i>','class="more"') ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat yellow">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $this->db->select_sum('commission_usd');
                    $this->db->from('transactions');
                    $this->db->where('created_on >= ',strtotime($fdate));
                    isset($last_day)?$this->db->where('created_on <= ',strtotime($last_day)):'';
                    $ts= $this->db->get()->row();
                    ?>
                    <span data-counter="counterup" data-value="<?php echo number_format($ts->commission_usd,2); ?>">0</span>
                </div>
                <div class="desc"> Commission(USD) </div>
            </div>
            <?php echo anchor($this->page_level.'transfer','View More  <i class="m-icon-swapright m-icon-white"></i>','class="more"') ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $this->db->select_sum('other_charges_usd');
                    $this->db->from('transactions');
                    $this->db->where('created_on >= ',strtotime($fdate));
                    isset($last_day)?$this->db->where('created_on <= ',strtotime($last_day)):'';
                    $ts= $this->db->get()->row();
                    ?>
                    <span data-counter="counterup" data-value="<?php echo number_format($ts->other_charges_usd,2); ?>">0</span>
                </div>
                <div class="desc"> Taxes(USD) </div>
            </div>
            <?php echo anchor($this->page_level.'transfer','View More  <i class="m-icon-swapright m-icon-white"></i>','class="more"') ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->



<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-body">
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>

</div>

<div class="row">
    <?php $this->load->view($this->page_level.'dashboard/transactions'); ?>


        <?php $this->load->view($this->page_level.'dashboard/recent_activities') ?>

</div>

<div class="row">
    <?php $this->load->view($this->page_level.'dashboard/top') ?>
</div>

<?php //$this->load->view($this->page_level.'dashboard/horizontal_bar'); ?>