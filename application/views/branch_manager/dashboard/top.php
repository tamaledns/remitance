
<link href="<?php echo base_url() ?>assets/horizontal_bar/build/css/style.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/horizontal_bar/build/css/horizBarChart.css" media="all" rel="stylesheet" type="text/css" />

    <div class="col-md-6 col-sm-6">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-green-haze hide"></i>
                    <span class="caption-subject font-green bold uppercase">Countries</span>
                    <span class="caption-helper hidden">tasks summary...</span>
                </div>

            </div>
            <div class="portlet-body">
                <div class="task-content">
                    <div id="3d_pie" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <div class="task-footer">
                    <div class="btn-arrow-link pull-right">

                        <?php echo anchor($this->page_level.'reports/branch','See All Records') ?>
                        <i class="icon-arrow-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <!-- Begin: life time stats -->
        <div class="portlet light bordered">

            <div class="portlet-body" style="height: 490px;">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">

                        <li  class="active">
                            <a href="#tb" data-toggle="tab"> Top Branch </a>
                        </li>

                        <li >
                            <a href="#tt" data-toggle="tab"> Top Teller </a>
                        </li>




                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tb" style="height: 400px;overflow: hidden;">
<!--                            <div class="scrolle" style="height: 40px;" data-always-visible="1" data-rail-visible1="1">-->
                            <div class="portlet light tasks-widget bordered">

                                <div class="portlet-body">
                                    <div class="task-content">

                                        <div class="wrapper">
                                            <section>

                                                <!-- Code Start -->
                                                <div class="chart-horiz">
                                                    <!-- Actual bar chart -->
                                    <ul class="chart" style=" width: 85%; overflow: visible;">
                                        <?php
                                        $this->db->select('branch_id,sum(sent_amount_usd) as co')->from('transactions');
                                        $this->db->where('created_on >= ',strtotime($fdate));
                                        isset($last_day)?$this->db->where('created_on <= ',strtotime($last_day)):'';
                                        $q= $this->db->group_by('branch_id')->order_by('sum(sent_amount_usd)','desc')->limit(15)->get()->result();
                                        $no=1; foreach($q as $br): ?>

                                        <li class="current" title="<?php
                                        $x= $this->db->select('branch_name,country,branch_code')->from('branch')->where('id',$br->branch_id)->get()->row();
                                        echo isset($x->branch_name)?$x->branch_name.' - '.$x->country:'N/A'

                                        ?>"><span class="bar" data-number="<?php echo $br->co ?>"></span>
                                            <br/> <br/>
                                            <span class="number"><?php echo number_format($br->co) ?></span>
                                        </li>
                                        <?php endforeach; ?>
<!--                                        <li class="current" title="Label 2"><span class="bar" data-number="28500"></span><span class="number">28,500</span></li>-->
<!--                                        <li class="current" title="Label 3"><span class="bar" data-number="128000"></span><span class="number">128,000</span></li>-->
<!--                                        <li class="current" title="Label 4"><span class="bar" data-number="134000"></span><span class="number">134,000</span></li>-->
                                    </ul>
                                                </div>
                                                <!-- Code End -->

                                            </section>

                                        </div>


                                        </div>
                                    </div>
                                    <div class="task-footer">
                                        <div class="btn-arrow-link pull-right">

                                            <?php echo anchor($this->page_level.'reports/branch','See All Records') ?>
                                            <i class="icon-arrow-right"></i>
                                        </div>
                                    </div>
                                </div>
<!--                            </div>-->
                        </div>
                        <div class="tab-pane" id="tt">
                            <div class="scroller" style="height: 400px;" data-always-visible="1" data-rail-visible1="1">
                            <div class="portlet light tasks-widget bordered">

                                <div class="portlet-body">
                                    <div class="task-content">

                                            <!-- START TASK LIST -->
                                            <ul class="task-list">
                                                <?php
                                                $this->db->select('created_by,branch_id,sum(sent_amount_usd) as co')->from('transactions');
                                                $this->db->where('created_on >= ',strtotime($fdate));
                                                isset($last_day)?$this->db->where('created_on <= ',strtotime($last_day)):'';

                                                $tt=$this->db->group_by('created_by')->order_by('sum(sent_amount_usd)','desc')->limit(15)->get()->result();

                                                $no=1; foreach($tt as $br): ?>
                                                    <li>
                                                        <div class="task-checkbox">
                                                            <?php echo $no; ?> </div>
                                                        <div class="task-title">
                                    <span class="task-title-sp">
                                        <?php

                                        $u= $this->db->select('full_name')->from('users')->where('id',$br->created_by)->get()->row();
                                        echo isset($u->full_name)?$u->full_name:'N/A';
                                        $x= $this->db->select('branch_name')->from('branch')->where('id',$br->branch_id)->get()->row();

                                        echo isset($x->branch_name)?' ('.$x->branch_name.')':'';
                                        ?>
                                    </span>
                                        <span class="label label-sm label-success pull-right"><?php echo number_format($br->co) ?>
                                            <span class="task-bell">
                                                            <i class="fa fa-dollar"></i>
                                                        </span>
                                            </span>
                                                        </div>

                                                    </li>
                                                    <?php $no++; endforeach; ?>

                                            </ul>
                                            <!-- END START TASK LIST -->
                                        </div>
                                    </div>
                                    <div class="task-footer">
                                        <div class="btn-arrow-link pull-right">

                                            <?php echo anchor($this->page_level.'reports/teller','See All Records') ?>
                                            <i class="icon-arrow-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>