<table class="table table-striped table-advance table-hover">
    <thead>
    <tr>
        <th colspan="3">
            <input type="checkbox" class="mail-checkbox mail-group-checkbox">
            <div class="btn-group input-actions">
                <a class="btn btn-sm blue btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> Actions
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-pencil"></i> Mark as Read </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-ban"></i> Spam </a>
                    </li>
                    <li class="divider"> </li>
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-trash-o"></i> Delete </a>
                    </li>
                </ul>
            </div>
        </th>
        <th class="pagination-control" colspan="3">
            <span class="pagination-info"> 1-30 of 789 </span>
            <a class="btn btn-sm blue btn-outline">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="btn btn-sm blue btn-outline">
                <i class="fa fa-angle-right"></i>
            </a>
        </th>
    </tr>
    </thead>
    <tbody>


    <?php foreach($message as $msg): ?>
        <tr data-messageid="<?php echo $msg->id ?>"  data-messageType="inbox">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox"> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> <?php echo $msg->phone ?> </td>
            <td class="view-message">  <?php echo word_limiter($msg->message, 10); ?>  </td>
            <td class="view-message inbox-small-cells"><?php echo $msg->replied ?> </td>
            <td class="view-message text-right" style="white-space: nowrap;"> <?php echo trending_date_time($msg->created_on); ?> </td>
        </tr>
    <?php endforeach; ?>


    </tbody>
</table>