<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">

        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <li class=" hidden <?php echo $title=='dashboard'?'active':''; ?>"><?php echo anchor($this->page_level.'','<i class="icon-home"></i> <span class="title">Dashboard</span>') ?></li>

            <!--                This is the menu for transfer  -->

            <!--                this is the menu for the settings-->


            <li class="<?php echo $title=='transfer'||$title=='cashout'?'active':''; ?>"><?php echo anchor($this->page_level.'transfer','<i class=" icon-paper-plane"></i> <span class="title">Transactions</span>') ?></li>


            <li class="<?php echo $title=='cashout'?'active':''; ?>"><?php echo anchor($this->page_level.'cashout','<i class=" icon-credit-card"></i> <span class="title">Cashout</span>') ?></li>
            <li class=" <?php echo $title=='users'?'active':''; ?>"><?php echo anchor($this->page_level.'users','<i class="icon-users"></i> <span class="title">Users</span>') ?></li>

            <li class="<?php echo $title=='approve_transactions'?'active':''; ?>"><?php echo anchor($this->page_level.'approve_transactions','<i class=" icon-check"></i> <span class="title">Approvals</span>') ?></li>

            <li class="<?php echo $title=='reports'?'active':'' ?>">
                <a href="javascript:;">
                    <i class="icon-notebook"></i>
                    <span class="title">Reports</span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">

                    <li class="<?php echo $subtitle=='teller'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/teller','<i class="icon-users"></i> <span class="title">Teller Report</span>') ?></li>
                    <li class="<?php echo $subtitle=='branch'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/branch','<i class="icon-layers"></i> <span class="title">Branch Report</span>') ?></li>
                    <li class="<?php echo $subtitle=='cashout'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/cashout','<i class="icon-credit-card"></i> <span class="title">Cashout</span>') ?></li>





                </ul>
            </li>


            <li  class=" <?php echo $title=='profile'?'active':'' ?>">
                <a href="javascript:;">
                    <i class="icon-user"></i>
                    <span class="title">Profile</span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="<?php echo $subtitle=='user_profile'?'active':''; ?>"><?php echo anchor($this->page_level.'profile','<i class="icon-pencil"></i> <span class="title">My Profile</span>') ?></li>
                    <li class="<?php echo $subtitle=='change_password'?'active':''; ?>"><?php echo anchor($this->page_level.'profile/change_password','<i class="icon-pencil"></i> <span class="title">Change Password</span>') ?></li>



                </ul>
            </li>


            <!--                this is the menu for the settings-->
            <li  class=" hidden  <?php echo $title=='settings'?'active':'' ?>">
                <a href="javascript:;">
                    <i class="icon-settings"></i>
                    <span class="title">Settings</span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="<?php echo $subtitle=='country'?'active':''; ?>"><?php echo anchor($this->page_level.'country','<i class="icon-globe"></i> <span class="title">Country</span>') ?></li>
                    <li class="<?php echo $subtitle=='currency'?'active':''; ?>"><?php echo anchor($this->page_level.'currency','<i class="icon-credit-card"></i> <span class="title">Currency</span>') ?></li>
                    <li class="<?php echo $subtitle=='alerts'?'active':''; ?>"><?php echo anchor($this->page_level.'alerts','<i class="icon-energy"></i> <span class="title">Alerts</span>') ?></li>
                    <li class="<?php echo $subtitle=='sms_email_gw'?'active':''; ?>"><?php echo anchor($this->page_level.'sms_email_gw','<i class="icon-envelope-letter"></i> <span class="title">SMS/Email Gateway</span>') ?></li>




                </ul>
            </li>





        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>