<div class="row">



    <div class="col-md-12 <?php echo (isset($hide)?'':'alerts'); ?>" id="">
        <div  class="note note-<?php echo isset($alert)?$alert:'info'; ?> note-shadow fade in">
            <!--        <button type="button" class="close" data-dismiss="alert"></button>-->

            <p style="text-align: center;">
                <?php echo $message; ?>
                <?php if($alert=='success'){
                    echo ' <i class="fa fa-check"></i>';
                }elseif($alert=='danger'){
                    // echo ' <i class="fa fa-ban"></i>';
                }elseif($alert=='warning'){
                    echo ' <i class="fa fa-warning"></i>';
                }else{
                    echo ' <i class="fa fa-info"></i>';
                } ?>

            </p>

        </div>
    </div>
</div>
<!--class="note note-success note-shadow"-->