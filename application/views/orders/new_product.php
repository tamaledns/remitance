<div class="row">

    <div class="col-md-12 ">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase">Add Item</span>
                </div>
                <div class="actions">

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('') ?>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="item" value="<?php echo set_value('item') ?>">
                                    <label for="form_control_1">Item Name <?php echo form_error('item','<span style=" color:red;">','</span>') ?></label>
                                    <span class="help-block">Add the Name of the item</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="supplier" value="<?php echo set_value('supplier') ?>" id="form_control_1">
                                    <label for="form_control_1">Supplier Name  <?php echo form_error('supplier','<span style=" color:red;">','</span>') ?></label>
                                    <span class="help-block">Add Supplier were you Bought the Item From</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" name="category" >
                                        <option value="" <?php echo set_select('category', '', TRUE); ?> ></option>

                                        <?php foreach($this->db->select('id,category_name')->from('categories')->get()->result() as $cat){ ?>
                                            <option value="<?php echo $cat->id  ?>"  <?php echo set_select('category', $cat->id); ?> ><?php echo $cat->category_name ?></option>
                                        <?php } ?>

                                    </select>
                                    <label for="form_control_1">Category <?php echo form_error('category','<span style=" color:red;">','</span>') ?></label>
                                    <span class="help-block">Choose the Category for the Item</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">

                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <div class="input-group right-addon">

                                        <input type="number" name="qty_per_unit" class="form-control" value="<?php echo set_value('qty_per_unit') ?>">
                                        <span class="input-group-addon">.Pcs</span>
                                        <label for="form_control_1"> Quantity Per Unit <?php echo form_error('qty_per_unit','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <div class="input-group  right-addon">

                                        <input type="text" name="unit_price" class="form-control" value="<?php echo set_value('unit_price')?>">
                                        <span class="input-group-addon">Shs</span>
                                        <label for="form_control_1"> Unit Price <?php echo form_error('unit_price','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <div class="input-group right-addon">

                                        <input type="number" name="units_in_stock" class="form-control" value="">
                                        <span class="input-group-addon">.Pcs</span>
                                        <label for="form_control_1"> Units in Stock <?php echo form_error('units_in_stock','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                            </div>

                        </div>


                    </div>
                    <div class="form-actions noborder">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
               <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>
</div>