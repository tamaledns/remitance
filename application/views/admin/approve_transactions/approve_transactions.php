<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Approvals</span>
                </div>
                <div class="actions hidden">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                        <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                            <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                        <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                            <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                    <tr>

                        <th width="1">
                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                        <th> Sender </th>
                        <th> Recipient </th>
                        <th title="Sent Amount"> Gross Amt </th>

                        <th title="Amount Paid"> Gross Amt ( <i class="fa fa-usd"></i> )</th>


                        <th>Destination</th>
                        <th> Branch </th>
                        <th>Date Sent</th>
                        <th> Status </th>

                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $t=$this->db->select()->from('transactions')->where(array('branch_id'=>$this->session->userdata('branch_id')))->or_where(array('status'=>'not_approved','status'=>'hold'))->order_by('id','desc')->limit('200')->get()->result();
                    foreach($t as $trans): ?>
                        <tr>
                            <td>
                                <input type="checkbox" class="checkboxes" value="1" /> </td>
                            <td title="Click to see Senders Details">
                                <?php
                                $user=$this->db->select('full_name')->from('users')->where('id',$trans->sender_id)->get()->row();
                                echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'), (isset($user->full_name)?ucwords(word_limiter($user->full_name,2)):'N/A')) ?>

                            </td>
                            <td title="Click to see Recievers Transaction Details"> <?php
                                echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'), ucwords(word_limiter($trans->receiver_name,2)));
                                ?> </td>
                            <td align="right" title="Sent Amount"> <?php echo number_format($trans->sent_amount,2) ?> </td>

                            <td align="right" title="Amount Paid"> <?php echo number_format($trans->sent_amount_usd,2) ?> </td>


                            <td>  <?php
                                $c=$this->db->select('country')->from('country')->where('a2_iso',$trans->receiver_country)->get()->row();
                                echo isset($c->country)?word_limiter($c->country,2):'N/A'; ?>
                            </td>
                            <td>  <?php
                                $bra=$this->db->select('id,branch_name')->from('branch')->where('id',$trans->branch_id)->get()->row();
                                echo isset($bra->id)? word_limiter($bra->branch_name,3):'N/A'; ?> </td>

                            <td><?php echo date('d-m-Y',$trans->created_on) ?></td>
                            <td>


                                <?php if($trans->status=='not_approved'){ ?>

                                    <div class="btn-group">
                                        <a class="btn default btn-sm" style="width:120px;" href="javascript:;" data-toggle="dropdown">
                                            <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <?php if( ($trans->created_by!=$this->session->userdata('id'))){ ?>
                                                <li>
                                                    <?php echo  ($trans->status!='not_approved')? anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Cashout'):'' ?>
                                                </li>
                                            <?php } ?>
                                            <li>
                                                <?php echo ($trans->status=='not_approved')? anchor($this->page_level.$this->page_level2.'approve/'.$trans->id*date('Y'),'  <i class="fa fa-check"></i> Approve','onclick="return confirm(\'You are about to Approve this Transactions ?\')"'):''; ?>
                                            </li>
                                            <li>
                                                <?php echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Transaction Details') ?>
                                            </li>

                                            <li>
                                                <?php
                                                echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'),'<i class="fa fa-user"></i>View Profile');
                                                ?>
                                            </li>
                                            <li>
                                            <li>

                                                <?php echo $trans->status=='hold'?anchor($this->page_level.$this->page_level2.'remove_hold/'.$trans->id*date('Y'),'  <i class="icon-control-play"></i> Remove Onhold'): anchor($this->page_level.$this->page_level2.'hold/'.$trans->id*date('Y'),'  <i class="icon-control-pause"></i> Put Onhold') ?>
                                            </li>


                                        </ul>
                                    </div>
                                <?php }elseif($trans->status=='hold'){ ?>



                                    <div class="btn-group">
                                        <a class="btn dark btn-sm" title="<?php echo $trans->hold_reason; ?>" style="width:120px;" href="javascript:;" data-toggle="dropdown">
                                            <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <?php if( ($trans->created_by!=$this->session->userdata('id'))){ ?>
                                                <li>
                                                    <?php echo  ($trans->status!='not_approved')? anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Cashout'):'' ?>
                                                </li>
                                            <?php } ?>
                                            <li>
                                                <?php echo ($trans->status=='not_approved')? anchor($this->page_level.$this->page_level2.'approve/'.$trans->id*date('Y'),'  <i class="fa fa-check"></i> Approve','onclick="return confirm(\'You are about to Approve this Transactions ?\')"'):''; ?>
                                            </li>
                                            <li>
                                                <?php echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Transaction Details') ?>
                                            </li>

                                            <li>
                                                <?php
                                                echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'),'<i class="fa fa-user"></i>View Profile');
                                                ?>
                                            </li>
                                            <li>
                                            <li>

                                                <?php echo $trans->status=='hold'?anchor($this->page_level.$this->page_level2.'remove_hold/'.$trans->id*date('Y'),'  <i class="icon-control-play"></i> Remove Onhold'): anchor($this->page_level.$this->page_level2.'hold/'.$trans->id*date('Y'),'  <i class="icon-control-pause"></i> Put Onhold') ?>
                                            </li>


                                        </ul>
                                    </div>

                                <?php }else{ ?>

                                    <?php if( $trans->created_by==$this->session->userdata('id')){ ?>

                                        <div class="btn-group">
                                            <a class="btn yellow-gold btn-sm" style="width:120px;" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <?php if( ($trans->created_by!=$this->session->userdata('id'))){ ?>
                                                    <li>
                                                        <?php echo  ($trans->status!='not_approved')? anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Cashout'):'' ?>
                                                    </li>
                                                <?php } ?>
                                                <li>
                                                    <?php echo ($trans->status=='not_approved')? anchor($this->page_level.$this->page_level2.'approve/'.$trans->id*date('Y'),'  <i class="fa fa-check"></i> Approve','onclick="return confirm(\'You are about to Approve this Transactions ?\')"'):''; ?>
                                                </li>
                                                <li>
                                                    <?php echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Transaction Details') ?>
                                                </li>

                                                <li>
                                                    <?php
                                                    echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'),'<i class="fa fa-user"></i>View Profile');
                                                    ?>
                                                </li>
                                                <li>
                                                <li>

                                                    <?php echo $trans->status=='hold'?anchor($this->page_level.$this->page_level2.'remove_hold/'.$trans->id*date('Y'),'  <i class="icon-control-play"></i> Remove Onhold'): anchor($this->page_level.$this->page_level2.'hold/'.$trans->id*date('Y'),'  <i class="icon-control-pause"></i> Put Onhold') ?>
                                                </li>


                                            </ul>
                                        </div>
                                        title="<?php echo $trans->hold_reason; ?>"
                                    <?php }else{ ?>


                                        <div class="btn-group">
                                            <a class="btn green-jungle btn-sm" style="width:120px;" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <?php if( ($trans->created_by!=$this->session->userdata('id'))){ ?>
                                                    <li>
                                                        <?php echo  ($trans->status!='not_approved')? anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Cashout'):'' ?>
                                                    </li>
                                                <?php } ?>
                                                <li>
                                                    <?php echo ($trans->status=='not_approved')? anchor($this->page_level.$this->page_level2.'approve/'.$trans->id*date('Y'),'  <i class="fa fa-check"></i> Approve','onclick="return confirm(\'You are about to Approve this Transactions ?\')"'):''; ?>
                                                </li>
                                                <li>
                                                    <?php echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Transaction Details') ?>
                                                </li>

                                                <li>
                                                    <?php
                                                    echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'),'<i class="fa fa-user"></i>View Profile');
                                                    ?>
                                                </li>
                                                <li>
                                                <li>

                                                    <?php echo $trans->status=='hold'?anchor($this->page_level.$this->page_level2.'remove_hold/'.$trans->id*date('Y'),'  <i class="icon-control-play"></i> Remove Onhold'): anchor($this->page_level.$this->page_level2.'hold/'.$trans->id*date('Y'),'  <i class="icon-control-pause"></i> Put Onhold') ?>
                                                </li>


                                            </ul>
                                        </div>
                                    <?php } ?>

                                <?php } ?>
                            </td>


                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>