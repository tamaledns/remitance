<script>
    var AppInbox = function () {

        var content = $('.inbox-content');
        var listListing = '';

        var loadInbox = function (el, name) {
            var url = '<?php echo $this->page_level2.'inbox' ?>';
            var title = el.attr('data-title');
            listListing = name;

            App.blockUI({
                target: content,
                overlayColor: 'none',
                animate: true
            });

            toggleButton(el);

            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                dataType: "html",
                success: function(res)
                {
                    toggleButton(el);

                    App.unblockUI('.inbox-content');

                    $('.inbox-nav > li.active').removeClass('active');
                    el.closest('li').addClass('active');
                    $('.inbox-header > h1').text(title);

                    content.html(res);

                    if (Layout.fixContentHeight) {
                        Layout.fixContentHeight();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError)
                {
                    toggleButton(el);
                },
                async: false
            });

            // handle group checkbox:
            jQuery('body').on('change', '.mail-group-checkbox', function () {
                var set = jQuery('.mail-checkbox');
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    $(this).attr("checked", checked);
                });
            });
        }
        var loadOutbox = function (el, name) {
            var url = '<?php echo $this->page_level2.'outbox' ?>';
            var title = el.attr('data-title');
            listListing = name;

            App.blockUI({
                target: content,
                overlayColor: 'none',
                animate: true
            });

            toggleButton(el);

            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                dataType: "html",
                success: function(res)
                {
                    toggleButton(el);

                    App.unblockUI('.inbox-content');

                    $('.inbox-nav > li.active').removeClass('active');
                    el.closest('li').addClass('active');
                    $('.inbox-header > h1').text(title);

                    content.html(res);

                    if (Layout.fixContentHeight) {
                        Layout.fixContentHeight();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError)
                {
                    toggleButton(el);
                },
                async: false
            });

            // handle group checkbox:
            jQuery('body').on('change', '.mail-group-checkbox', function () {
                var set = jQuery('.mail-checkbox');
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    $(this).attr("checked", checked);
                });
            });
        }

        var loadMessage = function (el, name, resetMenu) {
            var url = '<?php echo $this->page_level2.'app_inbox_view' ?>';

            App.blockUI({
                target: content,
                overlayColor: 'none',
                animate: true
            });

            toggleButton(el);

            var message_id = el.parent('tr').attr("data-messageid");

            var message_Type = el.parent('tr').attr("data-messageType");

            $.ajax({
                type: "GET",
                cache: false,
                url: url+'/'+message_id+'/'+message_Type,
                dataType: "html",
               // data: {'message_id': message_id},
                success: function(res)
                {
                    App.unblockUI(content);

                    toggleButton(el);

                    if (resetMenu) {
                        $('.inbox-nav > li.active').removeClass('active');
                    }
                    $('.inbox-header > h1').text('View Message');

                    content.html(res);
                    Layout.fixContentHeight();
                },
                error: function(xhr, ajaxOptions, thrownError)
                {
                    toggleButton(el);
                },
                async: false
            });
        }

        var initWysihtml5 = function () {
            $('.inbox-wysihtml5').wysihtml5({
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }

        var initFileupload = function () {

            $('#fileupload').fileupload({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: '../assets/global/plugins/jquery-file-upload/server/php/',
                autoUpload: true
            });

            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    url: '../assets/global/plugins/jquery-file-upload/server/php/',
                    type: 'HEAD'
                }).fail(function () {
                    $('<span class="alert alert-error"/>')
                        .text('Upload server currently unavailable - ' +
                            new Date())
                        .appendTo('#fileupload');
                });
            }
        }

        var loadCompose = function (el) {
            var url = '<?php echo base_url('index.php/'.$this->page_level.$this->page_level2.'app_inbox_compose')  ?>';

            App.blockUI({
                target: content,
                overlayColor: 'none',
                animate: true
            });

            toggleButton(el);

            // load the form via ajax
            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                dataType: "html",
                success: function(res)
                {
                    App.unblockUI(content);
                    toggleButton(el);

                    $('.inbox-nav > li.active').removeClass('active');
                    $('.inbox-header > h1').text('Compose');

                    content.html(res);

//                    initFileupload();
                    //initWysihtml5();

                    $('.inbox-wysihtml5').focus();
                    Layout.fixContentHeight();
                },
                error: function(xhr, ajaxOptions, thrownError)
                {
                    toggleButton(el);
                },
                async: false
            });
        }
        var loadCompose_sms = function (el) {
            // var url = 'app_inbox_compose.html';
            var url = '<?php echo base_url('index.php/'.$this->page_level.$this->page_level2.'app_inbox_compose/sms')  ?>';

            App.blockUI({
                target: content,
                overlayColor: 'none',
                animate: true
            });

            toggleButton(el);

            // load the form via ajax
            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                dataType: "html",
                success: function(res)
                {
                    App.unblockUI(content);
                    toggleButton(el);

                    $('.inbox-nav > li.active').removeClass('active');
                    $('.inbox-header > h1').text('Compose');

                    content.html(res);

                    // initFileupload();
                    //  initWysihtml5();

                    $('.inbox-wysihtml5').focus();
                    Layout.fixContentHeight();
                    App.initUniform();
                },
                error: function(xhr, ajaxOptions, thrownError)
                {
                    toggleButton(el);
                },
                async: false
            });
        }

        var loadReply = function (el) {
            var messageid = $(el).attr("data-messageid");
            var url = '<?php echo base_url('index.php/'.$this->page_level.$this->page_level2.'app_inbox_reply/')  ?>';

            App.blockUI({
                target: content,
                overlayColor: 'none',
                animate: true
            });

            toggleButton(el);

            // load the form via ajax
            $.ajax({
                type: "GET",
                cache: false,
                url: url+'/'+messageid,
                dataType: "html",
                success: function(res)
                {
                    App.unblockUI(content);
                    toggleButton(el);

                    $('.inbox-nav > li.active').removeClass('active');
                    $('.inbox-header > h1').text('Reply');

                    content.html(res);
                    $('[name="message"]').val($('#reply_email_content_body').html());

                    handleCCInput(); // init "CC" input field

//                    initFileupload();
                    //initWysihtml5();
                    Layout.fixContentHeight();
                },
                error: function(xhr, ajaxOptions, thrownError)
                {
                    toggleButton(el);
                },
                async: false
            });
        }

        var handleCCInput = function () {
            var the = $('.inbox-compose .mail-to .inbox-cc');
            var input = $('.inbox-compose .input-cc');
            the.hide();
            input.show();
            $('.close', input).click(function () {
                input.hide();
                the.show();
            });
        }

        var handleBCCInput = function () {

            var the = $('.inbox-compose .mail-to .inbox-bcc');
            var input = $('.inbox-compose .input-bcc');
            the.hide();
            input.show();
            $('.close', input).click(function () {
                input.hide();
                the.show();
            });
        }

        var toggleButton = function(el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        }

        return {
            //main function to initiate the module
            init: function () {

                // handle compose btn click
                $('.inbox').on('click', '.compose-btn', function () {
                    loadCompose($(this));
                });
                // handle compose btn click
                $('.inbox').on('click', '.compose-btn-sms', function () {
                    loadCompose_sms($(this));
                });

                // handle discard btn
                $('.inbox').on('click', '.inbox-discard-btn', function(e) {
                    e.preventDefault();
                    loadInbox($(this), listListing);
                });

                // handle reply and forward button click
                $('.inbox').on('click', '.reply-btn', function () {
                    loadReply($(this));
                });

                // handle view message
                $('.inbox').on('click', '.view-message', function () {
                    loadMessage($(this));
                });
//
//                // handle inbox listing
//                $('.inbox-nav > li > a').click(function () {
//                    loadInbox($(this), 'inbox');
//                });
//                // handle inbox listing
//                $('.inbox-nav > li > a').on('click','.outbox',function () {
//                    loadOutbox($(this), 'outbox');
//                });

                $('.outbox-menu').click(function(){
                    loadOutbox($(this), 'outbox');
                });
                $('.inbox-menu').click(function(){
                    loadInbox($(this), 'inbox');
                });

                //handle compose/reply cc input toggle
                $('.inbox-content').on('click', '.mail-to .inbox-cc', function () {
                    handleCCInput();
                });

                //handle compose/reply bcc input toggle
                $('.inbox-content').on('click', '.mail-to .inbox-bcc', function () {
                    handleBCCInput();
                });

                //handle loading content based on URL parameter
                if (App.getURLParameter("a") === "view") {
                    loadMessage();
                } else if (App.getURLParameter("a") === "compose") {
                    loadCompose();
                } else {


                   <?php  if($this->input->post()){ ?>
                    loadCompose();
                    <?php }else{ ?>
                    $('.inbox-menu').click();
                    <?php } ?>
                    <?php //$this->input->post()?"loadCompose();":"$('.inbox-menu').click();"; ?>

                    //$('.inbox-menu').click();
                    //$('.inbox-nav > li:first > a').click();
                   // loadInbox($(this), 'inbox');
                }

            }

        };

    }();

    jQuery(document).ready(function() {
        AppInbox.init();
    });
</script>