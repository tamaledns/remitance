<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">

                    <?php echo form_open($this->page_level.$this->page_level2.$this->uri->slash_segment(3),'class="form-inline"') ?>

                    <div class="form-group">

                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" value="<?php echo $this->input->post('from') ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" value="<?php echo $this->input->post('to')?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_error('country','<label style="color:#ff0000;">','</label>'); ?>
                        <select class="form-control" name="country" style="width: 110px;">
                            <option value="" <?php echo set_select('country', '', TRUE); ?> >Country</option>
                            <?php foreach($this->db->select('a2_iso,country')->from('selected_countries')->get()->result() as $cty): ?>
                                <option value="<?php echo $cty->a2_iso ?>" <?php echo set_select('country', $cty->a2_iso,$cty->a2_iso==$this->input->post('country')?TRUE:''); ?> ><?php echo $cty->country ?></option>
                            <?php endforeach; ?>



                        </select>
                    </div>

                    <div class="form-group">
                        <?php echo form_error('status','<label style="color:#ff0000;">','</label>'); ?>

                        <select class="form-control" name="status" style="width: 100px;" >
                            <option value="" <?php echo set_select('status', '', TRUE); ?> >Status</option>
                            <?php foreach($status as $st){ ?>
                                <option value="<?php echo $st ?>" <?php echo set_select('status', $st,$st==$this->input->post('status')?TRUE:''); ?> ><?php echo humanize($st) ?></option>

                            <?php  } ?>
                            <option value="above_5000" <?php echo set_select('status', 'above_5000','above_5000'==$this->input->post('status'?TRUE:'')); ?> >Above 5000$</option>


                        </select>
                    </div>

                    <button type="submit" class="btn green"><i class="fa fa-sliders"></i> Filter</button>
                    <?php echo form_close(); ?>
                </div>


            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <!--id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, -->
                    <thead>
                    <tr>
                        <th> Txn ID </th>
                        <th> Sender </th>
                        <th> Recipient </th>


                        <th> Amt&nbsp;Received </th>

                        <th> Phone </th>
                        <th>Sent&nbsp;by</th>
                        <th>Received&nbsp;at</th>

                        <th>Date</th>
                        <th> Status </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach($t as $trans): ?>
                        <tr>
                            <td>
                                <?php echo $trans->transaction_id; ?>
                            </td>
                            <td title="View Sender Profile Details">
                                <?php
                                $user=$this->db->select('full_name')->from('users')->where('id',$trans->sender_id)->get()->row();
                                echo  isset($user->full_name)? anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'), ucwords($user->full_name)):'N/A'; ?>

                            </td>
                            <td > <?php echo ucwords($trans->receiver_name) ?> </td>


                            <td align="right"> <?php echo number_format($trans->received_amount) ?> </td>

                            <td>  <?php echo $trans->receiver_phone ?> </td>

                            <td>  <?php

                                $c=$this->db->select('branch_name,branch_code')->from('branch')->where('id',$trans->branch_id)->get()->row();
                                echo isset($c->branch_code)?$c->branch_code:'N/A'; ?>
                            </td>
                            <td>  <?php
                                $r=$this->db->select('branch_name,branch_code')->from('branch')->where('id',$trans->receiver_branch)->get()->row();
                                echo isset($r->branch_code)? word_limiter($r->branch_code,2):'N/A'; ?>
                            </td>


                            <td><?php echo date('d-m-Y',$trans->created_on) ?></td>
                            <td>

                                <?php if($trans->status=='cashed_out'){ ?>
                                    <div class="btn green-jungle btn-sm" style="width:100px;"><?php echo humanize($trans->status) ?></div>
                                <?php } elseif($trans->status=='canceled'){ ?>
                                    <div class="btn red btn-sm" style="width:100px;"><?php echo humanize($trans->status) ?></div>

                                <?php } elseif($trans->status=='hold'){ ?>
                                    <div class="btn dark  btn-sm" style="width:100px;"><?php echo humanize($trans->status) ?></div>
                                <?php } elseif($trans->status=='not_approved'){ ?>
                                    <div class="btn default btn-sm" style="width:100px;"><?php echo humanize($trans->status) ?></div>

                                <?php }else{ ?>

                                    <div class="btn btn-info btn-sm" style="width:100px;"><?php echo humanize($trans->status) ?></div>

                                <?php } ?>
                            </td>


                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->