<script>

    $("#cntry").change(function () {
        $("#netamount_alert").html("");


    });

    //this is the fucntion which selects the wright commission from the database
    $("#amount").bind('keyup', function () {
        var v = $(this).val();
        var c = $("#cntry").val();
        var f = $("#sender_currency").val();
        var amount = $("#amount").val();

        if (c != '' && v != '' && f != '') {

            $.ajax({
                type: 'GET',
//            url:ur/url/file+'?v='+v,
                url: '<?php echo base_url("index.php/ajax_api/commission")?>/' + c + '/' + v + '/' + f,
                success: function (d) {

                    $("#computed_commission").val(d);


                    if (c != '' && d != '') {

                        $.ajax({
                            type: 'GET',
                            beforeSend: function () {

                                $("#other_charges").val('Please wait....');

                            },
                            url: '<?php echo base_url("index.php/ajax_api/tax_percentage")?>/'+c+'/' + d,
                            success: function (h) {

                                $("#other_charges").val(h);

                                var net_amount = parseFloat(amount) - parseFloat(d) - parseFloat(h);
                                $("#net_amount").val(net_amount);


                            }

                        });

                    }else{
                        $("#other_charges").val(0);
                    }


                }


            });

        } else {
            $("#computed_commission").val("Loading...");
        }
    });

    //this is the function which calculates the amount when the amount is changed
    $("#net_amount").bind('keyup', function () {

        var v = $(this).val();
        var c = $("#cntry").val();
        var f = $("#sender_currency").val();

        if (f != '' && v != '' && c != '') {

            $.ajax({
                type: 'GET',
//            url:ur/url/file+'?v='+v,
                url: '<?php echo base_url("index.php/ajax_api/reverse_commission")?>/' + v + '/' + c + '/' + f,
                success: function (d) {
                    $("#netamount_alert").html('');
                    // $("#amount").val(d);

                    var result = d - v;

                    var k = (result >= 0? result:0);

                    $("#computed_commission").val(k);

                    if (c != '' && k != '') {

                        $.ajax({
                            type: 'GET',
                            beforeSend: function () {

                                $("#other_charges").val('Please wait....');

                            },
                            url: '<?php echo base_url("index.php/ajax_api/tax_percentage")?>/'+c+'/' + k,
                            success: function (h) {

                                $("#other_charges").val(h);

//                                parseFloat(k)
                                var amount = parseFloat(d) + parseFloat(h);
                                $("#amount").val(amount);


                            }

                        });

                    }else{
                        $("#other_charges").val(0);
                    }


                }


            });

        } else {
            $("#netamount_alert").html('<span class="text text-danger"><b>Please Fill in The Required fields</b></span>');
        }
    });


    //this is the function which selects the country rates from the database
    $("#branch").change(function () {

        var f = $("#cntry").val();
        var b = $("#branch").val();

        if (f != '' && b != '') {

            $.ajax({
                type: 'GET',
                beforeSend: function () {
                    $("#commission_rates").html('<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> Please Wait...</div>');
                },
//            url:ur/url/file+'?v='+v,
                url: '<?php echo base_url("index.php/ajax_api/country_rates")?>/' + f + '/' + b,
                success: function (d) {

                    $("#commission_rates").html(d);

                }

            });

        } else {
            $("#commission_rates").html('<span class="text text-info">No Recipient Country or branch Selected</span>');
        }
    });


</script>
