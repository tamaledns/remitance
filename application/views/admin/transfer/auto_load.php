
<!--id, full_name, city, password, username, region, country, phone, email, gender,-->
<?php $user=$this->db->select()->from('users')->where('id',$id)->get()->row(); ?>

<input type="hidden" value="<?php echo $user->id ?>" name="user_id">

<div class="form-group form-md-line-input">
    <label class="col-md-4 control-label" for="form_control_1">Full Name</label>
    <div class="col-md-8">
        <input type="text" required title="Enter Senders Full Names" id="typeahead_example_1" value="<?php echo $user->full_name ?>" name="sender_name" class="form-control" placeholder="Enter Senders Full Names" />
        <div class="form-control-focus" style="color: red"><?php echo form_error('sender_name') ?> </div>

    </div>
</div>


<div class="form-group form-md-line-input">
    <label class="col-md-4 control-label" for="form_control_1">Email</label>
    <div class="col-md-8">
        <input type="email" class="form-control"  name="sender_email" value="<?php echo $user->email ?>" id="form_control_1" placeholder="Senders Email address">
        <div class="form-control-focus" style="color: red"><?php echo form_error('sender_email') ?> </div>
        <span class="help-block"></span>
    </div>
</div>
<div class="form-group form-md-line-input">
    <label class="col-md-4 control-label" for="form_control_1">Zip Code / Phone Number</label>
    <div class="col-md-3">
        <select class="form-control" name="sender_zip_code"  >
            <option value="<?php echo $user->zip_code ?>" <?php echo set_select('sender_zip_code', $user->zip_code, TRUE); ?> ><?php echo $user->zip_code ?></option>

            <?php foreach($this->db->select('dialing_code,a2_iso')->from('country')->order_by('a2_iso','asc')->get()->result() as $cat){ ?>
                <option value="<?php echo $cat->dialing_code  ?>"  <?php echo set_select('sender_zip_code', $cat->dialing_code); ?> ><?php echo $cat->a2_iso.'('.$cat->dialing_code.')' ?></option>
            <?php } ?>

        </select>
        <label for="form_control_1"> <?php echo form_error('sender_zip_code','<span style=" color:red;">','</span>') ?></label>
        <span class="help-block">Zip code</span>
    </div>
    <div class="col-md-5">
        <input maxlength="10" type="text" class="form-control" max="10" required name="sender_phone" value="<?php echo $user->phone ?>" id="form_control_1" placeholder="Phone Number(e.g 07xxxxxx)">
        <div class="form-control-focus" style="color: red"><?php echo form_error('sender_phone') ?> </div>
        <span class="help-block"></span>
    </div>
</div>






