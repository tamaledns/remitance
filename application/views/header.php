<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title><?php echo humanize($title) ?> | <?php echo $this->site_options->title('site_name') ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="Dennis Tamale(tamaledns@gmail.com)" name="author"/>

    <!-- BEGIN PAGE FIRST SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/global/plugins/pace/pace.min.js" type="text/javascript"></script>
    <!-- END PAGE FIRST SCRIPTS -->
    <!-- BEGIN PAGE TOP STYLES -->
    <link href="<?php echo base_url() ?>assets/global/plugins/pace/themes/pace-theme-flash.css<?php //echo ($title=='transfer'&&isset($subtitle)?'pace-theme-bounce.css':'pace-theme-flash.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE TOP STYLES -->

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <?php if($title=='dashboard'){ ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
    <?php }else{ ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url() ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />


        <!-- END PAGE LEVEL PLUGINS -->
    <?php } ?>

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?php echo base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?php echo base_url() ?>assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/layouts/layout4/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo base_url() ?>assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url($this->site_options->title('site_logo')) ?>" /> </head>
<!-- END HEAD -->

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top" style="background-color: <?php echo $this->site_options->title('site_color_code') ?> !important;">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo base_url() ?>"><img src="<?php echo base_url($this->site_options->title('site_logo')) ?>" height="45"  alt="logo" class="logo-default" /></a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <div class="page-top">

            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
            <form class="search-form hidden" action="page_general_search_2.html" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control input-sm" placeholder="Search..." name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                </div>
            </form>
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">


                    <!--                    This is the menu for the current branch-->
                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-home"></i>
						<span class="badge" style="background-color: inherit !important;">
						<?php echo $this->session->userdata('branch_name'); ?> </span>
                        </a>
                    </li>
                    <li class="separator "></li>

                    <!--                    This is the menu for the current price amount-->
                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-wallet"></i>
						<span class="badge" style="background-color: inherit !important;">
						<?php echo number_format($this->custom_library->branch_balance($this->session->userdata('branch_id')),2) ?> USD  </span>
                        </a>
                    </li>
                    <li class="separator "></li>
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark hidden" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
						<span class="badge badge-success">
						7 </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3><span class="bold">12 pending</span> notifications</h3>
                                <a href="extra_profile.html">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">just now</span>
										<span class="details">
										<span class="label label-sm label-icon label-success">
										<i class="fa fa-plus"></i>
										</span>
										New user registered. </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">9 days</span>
										<span class="details">
										<span class="label label-sm label-icon label-danger">
										<i class="fa fa-bolt"></i>
										</span>
										Storage server failed. </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->

                    <li class="separator hidden">
                    </li>
                    <!-- BEGIN INBOX DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark hidden" id="header_inbox_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-envelope-open"></i>
						<span class="badge badge-danger">
						4 </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>You have <span class="bold">7 New</span> Messages</h3>
                                <a href="inbox.html">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                    <li>
                                        <a href="inbox.html?a=view">
										<span class="photo">
										<img src="<?php echo base_url() ?>assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Lisa Wong </span>
										<span class="time">Just Now </span>
										</span>
										<span class="message">
										Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END INBOX DROPDOWN -->
                    <li class="separator hidden">
                    </li>
                    <!-- BEGIN TODO DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li  class="dropdown dropdown-extended dropdown-tasks dropdown-dark hidden" id="header_task_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-calendar"></i>
						<span class="badge badge-primary">
						3 </span>
                        </a>
                        <ul class="dropdown-menu extended tasks">
                            <li class="external">
                                <h3>You have <span class="bold">12 pending</span> tasks</h3>
                                <a href="page_todo.html">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                    <li>
                                        <a href="javascript:;">
										<span class="task">
										<span class="desc">New release v1.2 </span>
										<span class="percent">30%</span>
										</span>
										<span class="progress">
										<span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">40% Complete</span></span>
										</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END TODO DROPDOWN -->

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile"><i class="icon-user"></i>
                            <?php echo ucwords($this->session->userdata('fullname')); ?> </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img alt="" class="img-circle" src="<?php echo base_url($this->session->userdata('photo')) ?>"/>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>

                                <?php echo anchor($this->page_level.'profile','<i class="icon-user"></i> My Profile'); ?>
                            </li>

                            <?php if(isset($menu)){ ?>

                                <li>
                                    <a href="page_calendar.html">
                                        <i class="icon-calendar"></i> My Calendar </a>
                                </li>
                                <li>
                                    <a href="inbox.html">
                                        <i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
								3 </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="page_todo.html">
                                        <i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
								7 </span>
                                    </a>
                                </li>
                            <?php } ?>
                            <li class="divider">
                            </li>

                            <li>

                                <?php echo anchor($this->page_level.'logout','<i class="icon-key"></i> Logout'); ?>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view($this->page_level.'page-sidebar-wrapper') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper ">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div hidden class="page-head hidden-print">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Buttons Datatable
                        <small>buttons extension demos</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->

            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb hidden-print" >
                <li>

                    <?php echo anchor( $this->uri->segment(1, 0), humanize($this->session->userdata('branch_name'))) ?>
                    <i class="fa fa-circle"></i>
                </li>
                <li>

                    <?php echo anchor($this->uri->slash_segment(1).$title,humanize($title));

                    //echo $this->session->userdata('country');?>

                </li>
                <li>
                    <?php echo strlen($subtitle)>0?' <i class="fa fa-circle"></i>':''; ?>
                    <?php echo humanize($subtitle); ?>
                </li>

                <li class="pull-right">


                    <?php if($title=='dashboard'){ ?>

                        <div>

                            <?php //echo humanize($this->session->userdata('branch_name')); ?>
                            <?php echo form_open($this->page_level.$title) ?>
                            <!--                            <label for="topbar-multiple" class="control-label pr10 fs11 text-muted">Reporting Period --><?php ////echo $fdate ?><!--  --><?php ////echo ' : last day'. isset($last_day)?$last_day:''; ?><!--</label>-->
                            <!--                    multiple selection has been removed multiple="multiple"-->
                            <select style="background: rgba(255,255,255,0.2)" name="filter" class="form-control input-small input-sm hidde" onchange="this.form.submit()" >
                                <optgroup label="Filter By:">
                                    <option value="1" <?php echo set_select('filter', 1); ?> >This Month</option>
                                    <option value="2" <?php echo set_select('filter', 2,TRUE); ?> >Last 30 Days</option>
                                    <option value="3" <?php echo set_select('filter', 3); ?>>Last 60 Days</option>
                                    <option value="4" <?php echo set_select('filter', 4); ?>>Last 90 Days</option>
                                    <option value="5" <?php echo set_select('filter', 5); ?>>Last Month</option>
                                    <option value="6" <?php echo set_select('filter', 6); ?>>Last Year</option>
                                </optgroup>
                            </select>

                            <?php form_close() ?>


                        </div>

                    <?php  }else{ ?>

                        <div id="time" class="text-success" align="center">

                            <script type="text/javascript">
                                var int=self.setInterval("clock()",10);
                                function clock() {
                                    var d=new Date();
                                    var t=d.toLocaleTimeString();
                                    var m=new Date();
                                    var y=m.toDateString();
                                    document.getElementById("time").innerHTML=t+" &nbsp;"+y;

                                }
                            </script>



                        </div>
                    <?php } ?>



                </li>

            </ul>
            <!-- BEGIN PAGE BASE CONTENT -->

            <!--            --><?php //print_r($this->session->userdata) ?>

