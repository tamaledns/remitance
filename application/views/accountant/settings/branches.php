<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">



                <div class="caption font-dark">
                    <i class="icon-home font-dark"></i>
                    <span class="caption-subject bold uppercase">Branches&nbsp;</span>
                </div>
                <div class="actions pull-left">

            <?php echo anchor($this->page_level.$this->page_level2.'new_branch',' <i class="fa fa-plus"></i> New Branch','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Name </th>
                        <th> Country </th>
<!--                        <th> State </th>-->
                        <th> Street </th>
                        <th> Telephone </th>
                        <th> Branch Code </th>
                        <th width="70"> Ctry Status </th>
                        <th width="70"> Status </th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    //id, branch_name, country, state, street, phone, website, others, created_by, created_on, updated_by, updated_on, id, id
                    foreach($this->db->select('id, branch_name, country, state, street, phone, branch_code, others,status,country_status')->from('branch')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo ucfirst($c->branch_name) ?> </td>
                            <td> <?php
                                $country=$this->db->select('country')->from('country')->where('a2_iso',$c->country)->get()->row();
                                echo $country->country ?> </td>
<!--                            <td> --><?php
//                                $st=$this->db->select('title')->from('state')->where('state',$c->state)->get()->row();
//
//                                echo isset($st->title)?$st->title:'N/A'; ?><!-- </td>-->
                            <td> <?php echo ucfirst($c->street) ?> </td>
                            <td> <?php echo $c->phone ?> </td>
                            <td><?php echo $c->branch_code; ?></td>
                            <td><?php echo $c->country_status=='1'?'<div class="btn btn-sm green-jungle" style="width: 70px;">Active</div>':'<div class="btn btn-sm btn-danger"  style="width: 70px;" data-original-title="" title="For these Branches to access the System you Countries have to to unblocked First">Blocked</div>' ?></td>
                            <td><?php echo $c->status=='active'?'<div class="btn btn-sm green-jungle" style="width: 70px;">Active</div>':'<div class="btn btn-sm btn-danger" style="width: 70px;">Blocked</div>' ?></td>
                            <td><div class="btn-group">
                                    <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>

                                            <?php echo anchor($this->page_level.$this->page_level2.'edit_branch/'.$c->id*date('Y'),'  <i class="fa fa-edit"></i> Edit') ?>
                                        </li>
                                        <li>

                                            <?php echo anchor($this->page_level.$this->page_level2.'delete_branch/'.$c->id*date('Y'),'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                        </li>
                                        <li>

                                            <?php echo $c->status=='blocked'? anchor($this->page_level.$this->page_level2.'unblock_branch/'.$c->id*date('Y'),'  <i class="fa fa-check"></i> Unblock'): anchor($this->page_level.$this->page_level2.'block_branch/'.$c->id*date('Y'),'  <i class="fa fa-ban"></i> Block' ,'onclick="return confirm(\'You are about to block this Branch from accessing the System \')"') ?>
                                        </li>

                                    </ul>
                                </div></td>
                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->