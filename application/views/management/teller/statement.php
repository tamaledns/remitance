<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<?php $branch= $this->db->select('a.full_name,b.country,balance')->from('users a')->join('country b','a.country=b.a2_iso')->where(array('a.id'=>$id))->get()->row(); ?>

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">




                <div class="caption font-dark caption-subject font-green bold uppercase" >




                    <!--This is the beginfng of the filters  -->


                    <?php echo form_open('','class="form-inline"') ?>

                    <span class="visible-print">
                        Teller Name : <?php echo $branch->full_name ?>
                        - <?php echo $branch->country ?>
                    </span>

<span class="hidden-print">
                    <div class="form-group">

                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" value="<?php echo set_value('from') ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" value="<?php echo set_value('to')?>">
                        </div>
                    </div>

    <button type="submit" class="btn green"><i class="fa fa-sliders"></i> Filter</button>
    </span>
                    <?php echo form_close(); ?>




                </div>
                <div class="tools">
                    <span class="hidden-print">
                <?php echo anchor($this->page_level.$this->page_level2.'credit/'.$id*date('Y'),'<i class="icon-plus"></i> Credit','class="btn btn-sm green" style="height: 30px !important;"'); ?>
                <?php echo anchor($this->page_level.$this->page_level2.'debit/'.$id*date('Y'),'<i class="icon-plus"></i> Debit','class="btn btn-sm btn-info" style="height: 30px !important;"'); ?>
                    </span>

                    <div class="btn-group  hidden-print">



                        <a class="btn blue  btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print </a>



                        <?php echo anchor($this->page_level.'export_excel/teller_statement/'.$first_day.'/'.$last_day.'/'.$id*date('Y'),'<i class="fa fa-file-excel-o"></i> Export','class="btn green  btn-outline "') ?>




                    </div>



                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Date </th>
                        <th style="width:50px;" > Type </th>
                        <th> Description </th>
                        <th> Debit </th>
                        <th> Credit </th>
                        <th> Balance </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                   $statement=$this->db->select('a.*,b.full_name')->from('teller_statements a')
                       ->join('users b','a.created_by=b.id')
                       ->where(
                           array(
                               'a.teller_id'=>$id,
                               'a.created_on >=' => $first_day,
                        'a.created_on <=' => $last_day
                           )
                       )->order_by('a.id','desc')->get()->result();

                    $no=1;
                    $credit=0;
                    $debit=0;
                    foreach($statement as $c): ?>
                   <tr>
                       <td><?php echo $no; ?></td>
                       <td> <?php echo trending_date($c->created_on) ?> </td>
                       <td><div  style="width:50px;" class="btn btn-xs btn-<?php echo $c->type=='credit'?'success':'info'; ?>"> <?php echo humanize($c->type) ?></div> </td>
                       <td> <?php echo $c->description ?> by <?php echo $c->full_name ?> </td>
                       <td> <span style="float: right;"> <?php echo number_format($c->debit,2) ?></span> </td>
                       <?php $debit=$debit+$c->debit ?>
                       <td> <span style="float: right;"><?php echo  number_format($c->credit,2) ?></span></td>
                       <?php $credit=$credit+$c->credit ?>
                       <td ><span style="float: right;"> <?php echo number_format($c->balance,2) ?> </span></td>

                   </tr>
                        <?php         $no++;  endforeach; ?>

                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4">

                        </td>
                        <td  style="padding: 8px;"><span style="float: right;"><?php echo number_format($debit,2) ?></span></td>
                        <td style="padding: 8px;"><span style="float: right;"><?php echo number_format($credit,2) ?></span></td>
                        <td  style="padding: 8px;"><span style="float: right;"><?php echo number_format($branch->balance,2) ?></span></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->



<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->