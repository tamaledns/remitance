<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />



<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark caption-subject  bold uppercase">

                    <?php echo form_open($this->page_level.$this->page_level2.$this->uri->slash_segment(3),'class="form-inline"') ?>
                    <?php  echo isset($first_day)? humanize($title).'  From : '.date('d M Y',$first_day).' - '.date('d M Y',$last_day):humanize($subtitle); ?>
                    <div class="form-group">

                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" value="<?php echo set_value('from') ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" value="<?php echo set_value('to')?>">
                        </div>
                    </div>

                    <button type="submit" class="btn green"><i class="fa fa-sliders"></i> Filter</button>
                    <?php echo form_close(); ?>
                </div>

                <div class="actions pull-left">

                    <?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
<!--id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, -->
                    <thead>
                    <tr>
                        <th width="3">#</th>
                        <th> Sender </th>
                        <th> Recipient </th>


                        <th> Amt Received </th>

                        <th> Phone </th>

                        <th>Sender Country</th>
                        <th> Branch </th>
                        <th width="90">Date</th>
                        <th> Status </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $no=1;
                    foreach($t as $trans): ?>

                      <tr>

                          <td><?php echo $no; ?></td>
                            <td title="View Sender Profile Details">
                                <?php
                                $user=$this->db->select('full_name')->from('users')->where('id',$trans->sender_id)->get()->row();
                                   ?>
                        <?php echo anchor($this->page_level.$this->page_level2.'process/'.$trans->id*date('Y'),isset($user->full_name)?ucwords($user->full_name):'N/A') ?>

                            </td>
                            <td > <?php echo ucwords($trans->receiver_name) ?> </td>


                            <td align="right"> <?php echo number_format($trans->received_amount) ?> </td>

                            <td>  <?php echo $trans->receiver_phone ?> </td>

                            <td>  <?php
                                $c=$this->db->select('country')->from('country')->where('a2_iso',$trans->sender_country)->get()->row();
                                echo isset($c->country)?$c->country:'N/A'; ?>
                            </td>

                            <td>  <?php
                                $bra=$this->db->select('id,branch_name')->from('branch')->where('id',$trans->branch_id)->get()->row();
                                echo isset($bra->id)? $bra->branch_name:'N/A'; ?> </td>
                            <td><?php echo date('d-m-Y',$trans->created_on) ?></td>

                          <td>

                              <?php if($trans->status=='cashed_out'){ ?>


                                  <div class="btn-group">
                                      <a class="btn green-jungle btn-sm" style="width:130px;" href="javascript:;" data-toggle="dropdown">
                                          <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i class="fa fa-angle-down"></i>
                                      </a>
                                      <ul class="dropdown-menu pull-right">
                                          <?php if( ($trans->created_by!=$this->session->userdata('id'))){ ?>
                                              <li>
                                                  <?php echo  ($trans->status!='not_approved')? anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Cashout'):'' ?>
                                              </li>
                                          <?php } ?>

                                          <li>
                                              <?php echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Transaction Details') ?>
                                          </li>
                                          <li>
                                              <?php
                                              echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'),'<i class="fa fa-user"></i>View Profile');
                                              ?>
                                          </li>
                                          <li>

                                              <?php echo anchor($this->page_level.'cashout/decline/'.$trans->id*date('Y'),'  <i class="fa fa-trash-o"></i> Decline','onclick="return confirm(\'You are about to Decline this Transactions ?\')"') ?>
                                          </li>


                                      </ul>
                                  </div>
                              <?php }elseif($trans->status=='not_approved'){ ?>


                                  <div class="btn-group">
                                      <a class="btn default btn-sm" style="width:130px;" href="javascript:;" data-toggle="dropdown">
                                          <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i class="fa fa-angle-down"></i>
                                      </a>
                                      <ul class="dropdown-menu pull-right">
                                          <?php if( ($trans->created_by!=$this->session->userdata('id'))){ ?>
                                              <li>
                                                  <?php echo  ($trans->status!='not_approved')? anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Cashout'):'' ?>
                                              </li>
                                          <?php } ?>

                                          <li>
                                              <?php echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Transaction Details') ?>
                                          </li>
                                          <li>
                                              <?php
                                              echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'),'<i class="fa fa-user"></i>View Profile');
                                              ?>
                                          </li>
                                          <li>

                                              <?php echo anchor($this->page_level.'cashout/decline/'.$trans->id*date('Y'),'  <i class="fa fa-trash-o"></i> Decline','onclick="return confirm(\'You are about to Decline this Transactions ?\')"') ?>
                                          </li>


                                      </ul>
                                  </div>
                              <?php }else{ ?>

                                  <?php if($trans->created_by==$this->session->userdata('id'))
                                  { ?>




                                      <div class="btn-group">
                                          <a class="btn yellow-gold btn-sm" style="width:130px;" href="javascript:;" data-toggle="dropdown">
                                              <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i class="fa fa-angle-down"></i>
                                          </a>
                                          <ul class="dropdown-menu pull-right">
                                              <?php if( ($trans->created_by!=$this->session->userdata('id'))){ ?>
                                                  <li>
                                                      <?php echo  ($trans->status!='not_approved')? anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Cashout'):'' ?>
                                                  </li>
                                              <?php } ?>

                                              <li>
                                                  <?php echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Transaction Details') ?>
                                              </li>
                                              <li>
                                                  <?php
                                                  echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'),'<i class="fa fa-user"></i>View Profile');
                                                  ?>
                                              </li>
                                              <li>

                                                  <?php echo anchor($this->page_level.'cashout/decline/'.$trans->id*date('Y'),'  <i class="fa fa-trash-o"></i> Decline','onclick="return confirm(\'You are about to Decline this Transactions ?\')"') ?>
                                              </li>


                                          </ul>
                                      </div>

                                  <?php  }
                                  else
                                  { ?>


                                      <div class="btn-group">
                                          <a class="btn btn-primary btn-sm" style="width:130px;" href="javascript:;" data-toggle="dropdown">
                                              <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i class="fa fa-angle-down"></i>
                                          </a>
                                          <ul class="dropdown-menu pull-right">
                                              <?php if( ($trans->created_by!=$this->session->userdata('id'))){ ?>
                                                  <li>
                                                      <?php echo  ($trans->status!='not_approved')? anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Cashout'):'' ?>
                                                  </li>
                                              <?php } ?>

                                              <li>
                                                  <?php echo anchor($this->page_level.'cashout/process/'.$trans->id*date('Y'),'  <i class="fa fa-money"></i> Transaction Details') ?>
                                              </li>
                                              <li>
                                                  <?php
                                                  echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'),'<i class="fa fa-user"></i>View Profile');
                                                  ?>
                                              </li>
                                              <li>

                                                  <?php echo anchor($this->page_level.'cashout/decline/'.$trans->id*date('Y'),'  <i class="fa fa-trash-o"></i> Decline','onclick="return confirm(\'You are about to Decline this Transactions ?\')"') ?>
                                              </li>


                                          </ul>
                                      </div>


                                  <?php } ?>

                              <?php } ?>
                          </td>


                      </tr>
                    <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->



<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->