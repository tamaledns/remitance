<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">

        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <li class="<?php echo $title=='dashboard'?'active':''; ?>"><?php echo anchor($this->page_level.'','<i class="icon-home"></i> <span class="title">Dashboard</span>') ?></li>
            <li class="<?php echo $title=='users'?'active':''; ?>"><?php echo anchor($this->page_level.'users','<i class="icon-users"></i> <span class="title">Users</span>') ?></li>

            <li class="<?php echo $title=='transfer'||$title=='cashout'?'active':''; ?>"><?php echo anchor($this->page_level.'transfer','<i class=" icon-paper-plane"></i> <span class="title">Transactions</span>') ?></li>

            <li class="<?php echo $title=='cashout'?'active':''; ?>"><?php echo anchor($this->page_level.'cashout','<i class=" icon-credit-card"></i> <span class="title">Cashout</span>') ?></li>

            <li class="<?php echo $title=='approve_transactions'?'active':''; ?>"><?php echo anchor($this->page_level.'approve_transactions','<i class=" icon-check"></i> <span class="title">Approvals</span>') ?></li>
            <li class="<?php echo $title=='reports'?'active':'' ?>">
                <a href="javascript:;">
                    <i class="icon-notebook"></i>
                    <span class="title">Reports</span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="<?php echo $subtitle==''?'active':''; ?>"><?php echo anchor($this->page_level.'reports','<i class="icon-folder"></i> <span class="title">All</span>') ?></li>
                    <li class="<?php echo $subtitle=='daily'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/daily','<i class="icon-calendar"></i> <span class="title">Daily Report</span>') ?></li>
                    <li class="<?php echo $subtitle=='weekly'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/weekly','<i class="icon-notebook"></i> <span class="title">Weekly Report</span>') ?></li>
                    <li class="<?php echo $subtitle=='monthly'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/monthly','<i class="icon-notebook"></i> <span class="title">Monthly Report</span>') ?></li>
                    <li hidden class="<?php echo $subtitle=='quarterly'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/quarterly','<i class="icon-notebook"></i> <span class="title">Quarterly Report</span>') ?></li>
                    <li class="<?php echo $subtitle=='above_5000'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/above_5000','<i class="fa fa-money"></i> <span class="title">Large Transactions</span>') ?></li>
                    <li class="<?php echo $subtitle=='on_hold'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/on_hold','<i class="icon-control-pause"></i> <span class="title">On Hold Report</span>') ?></li>
                    <li class="<?php echo $subtitle=='teller'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/teller','<i class="icon-users"></i> <span class="title">Teller Report</span>') ?></li>
                    <li class="<?php echo $subtitle=='branch'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/branch','<i class="icon-layers"></i> <span class="title">Branch Report</span>') ?></li>
                    <li class="<?php echo $subtitle=='transfer'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/transfer','<i class="icon-paper-plane"></i> <span class="title">Transfer</span>') ?></li>
                    <li class="<?php echo $subtitle=='cashout'?'active':''; ?>"><?php echo anchor($this->page_level.'reports/cashout','<i class="icon-credit-card"></i> <span class="title">Cashout</span>') ?></li>



                </ul>
            </li>



            <!--                this is the menu for the settings-->
            <li class="<?php echo $title=='settings'?'active':'' ?>">
                <a href="javascript:;">
                    <i class="icon-settings"></i>
                    <span class="title">Settings</span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="<?php echo $subtitle=='countries'||$subtitle=='new_country'||$subtitle=='ban_country'||$subtitle=='delete_country'||$subtitle=='unblock_country'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/countries','<i class="icon-globe"></i> <span class="title">Countries</span>') ?></li>
                    <li class="<?php echo $subtitle=='branches'||$subtitle=='new_branch'||$subtitle=='edit_branch'||$subtitle=='delete_branch'||$subtitle=='block_branch'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/branches','<i class="icon-home"></i> <span class="title">Branches</span>') ?></li>
                    <li class="<?php echo $subtitle=='currency'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/currency','<i class="icon-credit-card"></i> <span class="title">Currency</span>') ?></li>
                    <li class="<?php echo $subtitle=='add_forex_rate'||$subtitle=='forex_rate'||$subtitle=='delete_exchange_rate'||$subtitle=='edit_exchange_rate'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/forex_rate','<i class="fa fa-money"></i> <span class="title">Forex Rate</span>') ?></li>
                    <li class="<?php echo $subtitle=='commission'||$subtitle=='add_commission'||$subtitle=='delete_commission'||$subtitle=='edit_commission'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/commission','<i class="fa fa-money"></i> <span class="title">Commission</span>') ?></li>
                    <li class="hidden <?php echo $subtitle=='alerts'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/alerts','<i class="icon-energy"></i> <span class="title">Alerts</span>') ?></li>
                    <li class="hidden <?php echo $subtitle=='sms_email_gw'?'active':''; ?>"><?php echo anchor($this->page_level.'settings/sms_email_gw','<i class="icon-envelope-letter"></i> <span class="title">SMS/Email Gateway</span>') ?></li>




                </ul>
            </li>








            <li class="<?php echo $title=='profile'?'active':'' ?>">
                <a href="javascript:;">
                    <i class="icon-user"></i>
                    <span class="title">Profile</span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="<?php echo $subtitle=='user_profile'?'active':''; ?>"><?php echo anchor($this->page_level.'profile','<i class="icon-pencil"></i> <span class="title">My Profile</span>') ?></li>
                    <li class="<?php echo $subtitle=='change_password'?'active':''; ?>"><?php echo anchor($this->page_level.'profile/change_password','<i class="icon-pencil"></i> <span class="title">Change Password</span>') ?></li>




                </ul>
            </li>
            <li class="<?php echo $title=='logs'?'active':''; ?>"><?php echo anchor($this->page_level.'logs','<i class="icon-list"></i> <span class="title">Logs</span>') ?></li>



        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>