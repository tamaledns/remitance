
<script src="<?php echo base_url() ?>assets/highcharts/highcharts.js"></script>
<!--<script src="https://code.highcharts.com/highcharts-3d.js"></script>-->

<!--<script src="--><?php //echo base_url() ?><!--assets/highcharts/highcharts-3d.js"></script>-->
<script src="<?php echo base_url() ?>assets/highcharts/exporting.js"></script>
<script src="<?php echo base_url() ?>assets/highcharts/themes/dark-green.js"></script>
<?php
$begin = new DateTime( $fdate );
$ldate=isset($last_day)?date($last_day):date('Y-m-d');
$end = new DateTime( $ldate );

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end->modify( '+1 day' ));


?>


<script type="text/javascript">
    $(function () {
        $('#container').highcharts({
            chart: {
                zoomType: 'x'
            },

            title: {
                text: ' Transaction Trend from <?php echo date("d F Y",strtotime($fdate))  ?> <?php echo isset($last_day)? ' - '.date("d F Y",strtotime($last_day)):''; ?>',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: [

                    <?php
                    foreach ( $period as $dt ):

                        echo "'".date('D, d M', strtotime($dt->format( "y-m-d" )))."',";

                    endforeach;
                    ?>
                ]

            },
            yAxis: {
                title: {
                    text: 'Amount(USD)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [
                {
                name: 'USD',
                data: [
                    <?php
                    foreach ( $period as $dt ):
                        $a=$this->db->select('sum(sent_amount_usd) as amt')->from('transactions')->where(array('created_on >'=>strtotime($dt->format("y-m-d" )),'created_on <='=>strtotime($dt->format("y-m-d 24:59:58" ))))->get()->row();
                        echo round($a->amt,2).",";

                    endforeach;
                    ?>
                ]
            }
//                , {
//                name: 'New York',
//                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
//            }, {
//                name: 'Berlin',
//                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
//            }, {
//                name: 'London',
//                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
//            }
            ]
        });
    });
</script>

<!--$ctry=$this->db->select('country,a2_iso')->from('selected_countries')->where(array('a2_iso'=>'AE'))->or_where(array('a2_iso'=>'CD'))->or_where(array('a2_iso'=>'UG'))->get()->result();-->
<?php
$this->db->select('a.country,a.a2_iso,sum(b.sent_amount_usd) as sent_amount_usd')
    ->from('selected_countries a')
    ->join('transactions b','a.a2_iso=b.sender_country');
$this->db->where('b.created_on >= ',strtotime($fdate));
isset($last_day)?$this->db->where('b.created_on <= ',strtotime($last_day)):'';
$ctry=$this->db->group_by('b.sender_country')->get()->result(); ?>


<script type="text/javascript">
    $(function () {
        $('#3d_pie').highcharts({
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45
                }
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: false
                    },
                    cursor: 'pointer',
                    allowPointSelect: true,
                    innerSize: 100,
                    showInLegend: true,
                    depth: 45

                }
            },
            series: [{
                name: 'Total Amount',
                data: [

                    <?php


                    foreach($ctry as $c):


                    ?>

                    ['<?php echo $c->country.' : $'.number_format($c->sent_amount_usd,2) ?>', <?php echo round($c->sent_amount_usd,2); ?>],

                   <?php endforeach; ?>


                ]
            }]
        });
    });
</script>


<!--<script src="--><?php //echo base_url() ?><!--assets/horizontal_bar/build/js/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>assets/horizontal_bar/build/js/jquery.horizBarChart.min.js"></script>

<script>
    $(document).ready(function(){
        $('.chart').horizBarChart({
            selector: '.bar',
            speed: 1000
        });
    });
</script>

