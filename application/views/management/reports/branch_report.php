<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/multiselect/css/multi-select.css">
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <?php //print_r($this->input->post()); ?>
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <?php echo form_open($this->page_level.$this->page_level2.$this->uri->slash_segment(3),'class="form-inline"') ?>
            <div class="portlet-title">

                <div class="caption font-dark caption-subject font-green bold uppercase  col-md-10">



                    <span class="hidden-print">Branch Report</span>
                   <span class="visible-print"> <?php  echo isset($first_day)? humanize($subtitle).' Report From  '.date('d M Y',$first_day).' - '.date('d M Y',$last_day):humanize($subtitle); ?></span>

                    <div class="form-group  hidden-print">

                        <div class="input-group input-group-sm input-medium date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" value="<?php echo date('Y-m-d',$first_day) ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" value="<?php echo date('Y-m-d',$last_day) ?>">
                        </div>

                        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title sbold red">Select Branch and Press Button</h4>
                                    </div>
                                    <div class="modal-body">





                                        <?php

                                        $branches=$this->db->select('id,branch_name,country,balance')->from('branch')->get()->result();
                                        foreach($branches as $t){
                                            $options[$t->id] = $t->branch_name.'-'.$t->country;
                                        }

                                        $attr=array(
                                            'id'=>'pre-selected-options'
                                        );
                                        //                        id="pre-selected-options"
                                        echo form_multiselect('branches[]', $options,$this->input->post('branches'),' id="pre-selected-options"');
                                        ?>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Select</button>
                                        <!--                                        <button type="button" class="btn green">Save changes</button>-->
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>


                        <a class="btn yellow-gold btn-sm btn-outline sbold" data-toggle="modal" href="#basic"><i class="fa fa-bank"></i> Branches </a>
                    </div>

                    <div class="form-group">
                        <?php echo form_error('compare_branch','<label style="color:#ff0000;">','</label>'); ?>

                        <?php $branches=$this->db->select('id,branch_name')->from('branch')->get()->result() ?>
                        <select class="form-control input-sm tooltips" data-title="Compare with this Branch"  name="compare_branch"  >
                            <option value="" <?= set_select('compare_branch', '', TRUE); ?> >Compare Branch</option>

                            <?php foreach($branches as $st){ ?>

                                <option value="<?= $st->id ?>" <?=  ($this->input->post('compare_branch')==$st->id?'selected':''); ?> ><?= humanize($st->branch_name) ?></option>

                            <?php  } ?>



                        </select>
                    </div>

                    <button type="submit" name="filter" class="btn btn-sm green hidden-print"><i class="fa fa-sliders"></i> Filter</button>

                </div>
                <div class="tools col-md-2">
                <div class="btn-group  hidden-print">
                    <button class="btn blue btn-sm btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print </button>
                </div>
                    <div class="btn-group  hidden-print">

                        <?php //echo anchor($this->page_level.'export_excel/branch_report/'.$first_day.'/'.$last_day.'/'.$selected_branches,'<i class="fa fa-file-excel-o"></i> Export','class="btn green btn-sm  btn-outline "'); ?>

                        <button name="export" value="export_excel" type="submit" class="btn green btn-sm  btn-outline "><i class="fa fa-file-excel-o"></i> Export</button>

                    </div>
                    </div>


            </div>
            <?php echo form_close(); ?>
            <div class="portlet-body">
                <table class="table table-striped table-condensed table-bordered table-hover" id="sample">
                    <!--Branch, Country, Transfer, Cashout, Commission, View details -->
                    <thead>
                    <tr>
                        <th> Branch </th>
                        <th> Country </th>
                        <th> Sent Amt<br/> <span style="font-size: xx-small">USD</span> </th>
                        <th> Received Amt<br/> <span style="font-size: xx-small">USD</span> </th>
                        <th> Cashedout<br/> <span style="font-size: xx-small">USD</span></th>
                        <th>Pending <br/><span style="font-size: xx-small">USD</span></th>
                        <th>Commission <br/><span style="font-size: xx-small">USD</span></th>
                        <th>Balance <br/><span style="font-size: xx-small">USD</span></th>
                        <th style="width:8px;" class="hidden-print"></th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach($branch as $t): ?>


                        <tr>
                            <td title="View Sender Profile Details">
                                <?php

                                echo ( isset($t->branch_name)?ucwords($t->branch_name):'N/A') ?>

                            </td>

                            <td style="white-space: nowrap;">
                                <?php $this->db->select('country')->from('country')->where('a2_iso',$t->country);
                                $ctry= $this->db->get()->row();
                                echo character_limiter(ucwords($ctry->country),20);
                                ?>
                            </td>



                            <td align="right"> <?php
                                $this->db->select_sum('sent_amount_usd')->from('transactions')->where(array('branch_id'=>$t->id));

                                strlen($compare_branch)>0?$this->db->where('receiver_branch',$compare_branch):'';

                                $this->db->where(
                                    array
                                    (
                                        'created_on >=' => $first_day,
                                        'created_on <=' => $last_day,
                                    )

                                );
                                $sent_amount=$this->db->get()->row();
                                echo number_format($sent_amount ->sent_amount_usd,2) ?>
                            </td>

                            <td align="right">
                                <?php
                                $this->db->select_sum('received_amount_usd')->from('transactions')->where('receiver_branch',$t->id);
                                strlen($compare_branch)>0?$this->db->where('branch_id',$compare_branch):'';
                                $this->db->where(
                                    array
                                    (
                                        'created_on >=' => $first_day,
                                        'created_on <=' => $last_day,
                                    )

                                );
                                $transfer=$this->db->get()->row();
                                echo number_format($transfer->received_amount_usd,2)
                                ?>
                            </td>

                            <td align="right"> <?php
                                $this->db->select_sum('received_amount_usd')->from('transactions')->where(array('receiver_branch'=>$t->id,'status'=>'cashed_out'));
                                strlen($compare_branch)>0?$this->db->where('branch_id',$compare_branch):'';
                                $this->db->where(
                                    array
                                    (
                                        'created_on >=' => $first_day,
                                        'created_on <=' => $last_day,
                                    )

                                );
                                $cashout=$this->db->get()->row();
                                echo number_format($cashout->received_amount_usd,2) ?>
                            </td>

                            <td align="right"> <?php
                                $this->db->select_sum('received_amount_usd')->from('transactions')->where(array('receiver_branch'=>$t->id,'status'=>'pending'));
                                strlen($compare_branch)>0?$this->db->where('branch_id',$compare_branch):'';
                                $this->db->where(
                                    array
                                    (
                                        'created_on >=' => $first_day,
                                        'created_on <=' => $last_day,
                                    )

                                );
                                $pending=$this->db->get()->row();
                                echo number_format($pending->received_amount_usd,2) ?>
                            </td>

                            <td align="right"> <?php
                                $this->db->select_sum('commission_usd')->from('transactions')
                                    ->where(array('branch_id'=>$t->id,'status'=>'cashed_out'));
                                strlen($compare_branch)>0?$this->db->where('receiver_branch',$compare_branch):'';
                                $this->db->where(
                                    array
                                    (
                                        'created_on >=' => $first_day,
                                        'created_on <=' => $last_day,
                                    )

                                );
                                $comm=$this->db->get()->row();
                                echo number_format($comm->commission_usd,2) ?>
                            </td>

                            <td>
                                <?php


                                echo number_format($t->balance,2);

                                ?>
                            </td>

                            <td  class="hidden-print">



                                <div class="btn-group">
                                    <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">

                                        <li>
                                            <?php echo anchor($this->page_level.$this->page_level2.$this->uri->slash_segment(3).'details/'.$t->id*date('Y'),'  <i class="icon-eye"></i> View Details'); ?>
                                        </li>
                                        <li>
                                            <?php echo anchor($this->page_level.'branch/credit/'.$t->id*date('Y'),'  <i class="icon-plus"></i> Credit'); ?>
                                        </li>
                                        <li>
                                            <?php echo anchor($this->page_level.'branch/debit/'.$t->id*date('Y'),'  <i class="icon-close"></i> Debit'); ?>
                                        </li>
                                        <li>
                                            <?php echo anchor($this->page_level.'branch/statement/'.$t->id*date('Y'),'  <i class="icon-list"></i> View Statement'); ?>
                                        </li>



                                    </ul>
                                </div>
                            </td>




                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->




<script src="<?php echo base_url() ?>assets/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript">
    // run pre selected options
    $('#pre-selected-options').multiSelect();

    //    $('#dataen').click(function(){
    //
    //        var p=$('#entrants_select');
    ////        alert('');
    //        if((p).is(":hidden"))
    //        {
    //            p.slideDown();
    //        }else{
    //            p.slideUp();
    //        }
    //
    //    });
</script>
