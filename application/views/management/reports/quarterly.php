<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <h4 class="bold"><?php echo humanize($subtitle) ?> Report</h4>
                        </div>
                    </div>
                    <div class="col-md-6 hidden-print">
                        <div class="btn-group pull-right">
                            <button class="btn green  btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print
                            </button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
               
                <table class="table table-scrollable table-bordered table-hover" cellspacing="0" cellpadding="0">
                  
                  <tr>
                    <td colspan="6" >FXQ_LAND001</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td colspan="5">STATEMENT OF ASSETS AND LIABILITIES, AND INCOME STATEMENT </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td colspan="5">TEMPLATE - VERSION 1.0</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>INSTITUTION NAME:</td>
                    <td>wealth Money Transfer</td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>DAY</td>
                    <td align="right">31</td>
                    <td></td>
                    <td>Institution Code:</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>MONTH</td>
                    <td align="right">12</td>
                    <td></td>
                    <td>Financial Year:</td>
                    <td align="right">2015</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>YEAR</td>
                    <td align="right">2015</td>
                    <td></td>
                    <td>Start Date:</td>
                    <td align="right">01/10/2015</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>PERIOD ENDING</td>
                    <td>31.12.2015</td>
                    <td></td>
                    <td>End Date:</td>
                    <td align="right">31/12/2015</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Officer's Name:</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Title:</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Telephone number:</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right">31</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>BALANCE SHEET</td>
                    <td>Shs '000'</td>
                    <td></td>
                    <td align="right">2,058</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>A</td>
                    <td>ASSETS</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">1</td>
                    <td>Cash </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (a) Local Currency</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (b) Foreign Currency</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">2</td>
                    <td>Balances at Bank</td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (a) Local Currency</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (b) Foreign Currency</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">3</td>
                    <td>Debtors </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">4</td>
                    <td>Prepayments </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">5</td>
                    <td>Fixed Assets </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">6</td>
                    <td>Other Assets </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">7</td>
                    <td>Total Assets</td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>B</td>
                    <td>LIABILITIES</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">8</td>
                    <td>Borrowings </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">9</td>
                    <td>Directors&rsquo; loans</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">10</td>
                    <td>Creditors</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">11</td>
                    <td>Other Payables &amp; accruals</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">12</td>
                    <td>Tax payable</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">13</td>
                    <td>Total Liabilities</td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>C</td>
                    <td>CAPITAL &amp; RESERVES </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">14</td>
                    <td>Paid up Capital </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">15</td>
                    <td>Current year profits /(Losses)</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">16</td>
                    <td>Retained Profits /(Losses)</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">17</td>
                    <td>Other Reserves </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">18</td>
                    <td>Total Capital &amp; Reserves</td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">19</td>
                    <td>Total Capital , Reserves &amp; Liabilities </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">20</td>
                    <td>Total Liabilities , Capital &amp; Reserves</td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>INCOME STATEMENT</td>
                    <td> Shs. '000' </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>A</td>
                    <td>INCOME </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">1</td>
                    <td>Sales of Currency </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (a) Sales to the Public</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (b) Sales to other Forex    Bureaus</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (c) Sales to Commerical    banks </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">2</td>
                    <td>Cost of sales of currency </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (a) Opening Stock</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (b) Purchases </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>     (c) Closing Stock</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">3</td>
                    <td>Gross Profit</td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">4</td>
                    <td>Other income </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>Foreign Exchange revaluation Gain/(Loss)</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>Interest Income </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>Commission from money remittances</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">5</td>
                    <td>Total Income </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>B</td>
                    <td>EXPENSES</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">6</td>
                    <td>Interest Expenses</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">7</td>
                    <td>Director's Emoluments </td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">8</td>
                    <td>Salaries and Wages</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">9</td>
                    <td>Other Expenses</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">10</td>
                    <td>Total Expenses</td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">11</td>
                    <td>PROFIT BEFORE TAX </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">12</td>
                    <td>TAX</td>
                    <td></td>
                    <td align="right">1</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">13</td>
                    <td>PROFIT AFTER TAX </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>C</td>
                    <td>PERFORMANCE INDICATORS </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">14</td>
                    <td> Returns on Assets </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">15</td>
                    <td> Return on shareholders funds </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td align="right">16</td>
                    <td> Expenses to Income  </td>
                    <td> -   </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>Certified Correct :</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>Date : _______________________</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->