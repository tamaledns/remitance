<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase">Commission &nbsp;</span>
                </div>
                <div class="actions pull-left">

            <?php echo anchor($this->page_level.$this->page_level2.'add_commission',' <i class="fa fa-plus"></i> Add Commission','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Country </th>
                        <th> Branch </th>
                        <th> Type </th>
                        <th> Min </th>
                        <th> Max </th>
                        <th>Units</th>
                        <th> Unit Value </th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->db->select('a.*,b.country,c.branch_name')->from('commission a')->join('selected_countries b','a.country=b.a2_iso')->join('branch c','c.id=a.branch_id','left')->order_by('a.country','asc')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo $c->country ?> </td>
                            <td> <?php echo $c->branch_name ?> </td>
                            <td><?php echo $c->type ?></td>
                            <td> <?php echo number_format($c->min) ?> </td>
                            <td> <?php echo $c->max==10000000?'*':$c->max ?> </td>
                            <td><?php echo $c->units ?></td>
                            <td> <?php echo $c->unit_value; ?> </td>

                            <td><div class="btn-group">
                                    <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>

                                            <?php echo anchor($this->page_level.$this->page_level2.'delete_commission/'.$c->id*date('Y'),'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                        </li>
                                        <li>
                        <?php echo anchor($this->page_level.$this->page_level2.'edit_commission/'.$c->id*date('Y'),'  <i class="fa fa-edit"></i> Edit'); ?>
                                        </li>

                                    </ul>
                                </div></td>
                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->