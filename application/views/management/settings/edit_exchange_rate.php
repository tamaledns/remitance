<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-money font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> Currency Converter</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('') ?>
                <?php $fx=$this->db->select()->from('forex_settings')->where('id',$id)->get()->row(); ?>
                <div class="form-body">
                    <?php echo form_error('to','<row style=" color:red;">','</row>') ?>
                    <div class="row">
                        <?php $co=$this->db->select('currency_alphabetic_code,currency_name,country')->from('country')->order_by('currency_alphabetic_code','asc')->get()->result() ?>

                        <div class="col-md-4">
                            <?php $from=$this->db->select('currency_name')->from('country')->where('currency_alphabetic_code',$fx->from_currency)->get()->row(); ?>
                            <?php $to=$this->db->select('currency_name')->from('country')->where('currency_alphabetic_code',$fx->to_currency)->get()->row(); ?>
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="from"  >
                                    <option value="<?php echo $fx->from_currency ?>" <?php echo set_select('from', $fx->from_currency, TRUE); ?> >From <?php echo $fx->from_currency .' ('.$from->currency_name.')'?></option>

                                    <?php foreach( $co as $from){ ?>
                                        <option value="<?php echo $from->currency_alphabetic_code  ?>"  <?php echo set_select('from', $from->currency_alphabetic_code); ?> ><?php echo $from->currency_name.' ('.$from->country.')'; ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1"><b>.</b> <?php echo form_error('from','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="to"  >
                                    <option value="<?php echo $fx->to_currency ?>" <?php echo set_select('to', $fx->to_currency, TRUE); ?> >To <?php echo $fx->to_currency .' ('.$to->currency_name.')'?></option>

                                    <?php foreach($co as $to){ ?>
                                        <option value="<?php echo $to->currency_alphabetic_code  ?>"  <?php echo set_select('to', $to->currency_alphabetic_code); ?> ><?php echo $to->currency_name.' ('.$to->country.')' ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1">To </label>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group form-md-line-input has-success">
                                <input type="text" class="form-control" name="amount" value="<?php echo $fx->to_amount ?>" placeholder="Amount">
                                <label for="form_control_1">Amount  <?php echo form_error('amount','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="country"  >
                                    <option value="" <?php echo set_select('from', '', TRUE); ?> >Country</option>

                                    <?php foreach( $this->db->select()->from('selected_countries')->get()->result() as $sc){ ?>
                                        <option value="<?php echo $sc->a2_iso  ?>"  <?php echo set_select('from', $sc->a2_iso); ?> ><?php echo $sc->country; ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1"><b>Country</b> <?php echo form_error('from','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>



                    </div>

                </div>
                <div class="form-actions">
                    <button type="submit" class="btn blue">Submit</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>