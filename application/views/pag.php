<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info hidden-print" id="sample_2_info" role="status" aria-live="polite">Showing <?php echo isset($per_page)?$per_page:''; ?> of <?php echo isset($number)?$number:''; ?> entries</div>
    </div>
    <div class="col-md-7 col-sm-12 ">
        <div class="dataTables_paginate hidden-print paging_simple_numbers pull-right" id="sample_2_paginate">
            <ul class="pagination">
            <?php echo isset($pg)?$pg:''; ?>
            </ul>
        </div>
    </div>
</div>