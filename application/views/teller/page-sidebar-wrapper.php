<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">

        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <li class="<?php echo $title=='transfer'||$title=='cashout'?'active':''; ?>"><?php echo anchor($this->page_level.'transfer','<i class=" icon-paper-plane"></i> <span class="title">Transactions</span>') ?></li>

            <li  class=" <?php echo $title=='profile'?'active':'' ?>">
                <a href="javascript:;">
                    <i class="icon-user"></i>
                    <span class="title">Profile</span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="<?php echo $subtitle=='user_profile'?'active':''; ?>"><?php echo anchor($this->page_level.'profile','<i class="icon-pencil"></i> <span class="title">My Profile</span>') ?></li>
                    <li class="<?php echo $subtitle=='change_password'?'active':''; ?>"><?php echo anchor($this->page_level.'profile/change_password','<i class="icon-pencil"></i> <span class="title">Change Password</span>') ?></li>

                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>