
<input type="hidden" value="" name="user_id">


<div class="form-group form-md-line-input">
    <label class="col-md-4 control-label" for="form_control_1">Full Name</label>
    <div class="col-md-8">
        <input type="text" required title="Enter Senders Full Names" id="typeahead_example_1" value="<?php echo set_value('sender_name') ?>" name="sender_name" class="form-control" placeholder="Enter Senders Full Names" />
        <div class="form-control-focus" style="color: red"><?php echo form_error('sender_name') ?> </div>

    </div>
</div>


<div class="form-group form-md-line-input">
    <label class="col-md-4 control-label" for="form_control_1">Email</label>
    <div class="col-md-8">
        <input type="email" class="form-control" name="sender_email" value="<?php echo set_value('sender_email') ?>" id="form_control_1" placeholder="Sender Email address">
        <div class="form-control-focus" style="color: red"><?php echo form_error('sender_email') ?> </div>
        <span class="help-block"></span>
    </div>
</div>
<div class="form-group form-md-line-input">
    <label class="col-md-4 control-label" for="form_control_1">Zip Code / Phone Number</label>
    <div class="col-md-3">
        <select class="form-control" name="sender_zip_code"  >
            <option value="" <?php echo set_select('sender_zip_code', '', TRUE); ?> >Zip Code</option>

            <?php foreach($this->db->select('dialing_code,a2_iso')->from('country')->order_by('a2_iso','asc')->get()->result() as $cat){ ?>
                <option value="<?php echo $cat->dialing_code  ?>"  <?php echo set_select('zip_code', $cat->dialing_code); ?> ><?php echo $cat->a2_iso.'('.$cat->dialing_code.')' ?></option>
            <?php } ?>

        </select>
        <label for="form_control_1"> <?php echo form_error('sender_zip_code','<span style=" color:red;">','</span>') ?></label>
        <span class="help-block">Choose the Zip code for your country</span>
    </div>
    <div class="col-md-5">
        <input maxlength="10" type="text" class="form-control" max="10" required name="sender_phone" value="<?php echo set_value('sender_phone') ?>" id="form_control_1" placeholder="User Phone Number(e.g 07xxxxxx)">
        <div class="form-control-focus" style="color: red"><?php echo form_error('sender_phone') ?> </div>
        <span class="help-block"></span>
    </div>
</div>


