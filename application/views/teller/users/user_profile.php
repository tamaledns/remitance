<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>

<link href="<?php echo base_url() ?>assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<?php $prof=$this->db->select()->from('users')->where('id',$id)->get()->row();

?>
<div class="col-md-12">
    <!-- BEGIN PROFILE SIDEBAR -->
    <div class="profile-sidebar" style="width:250px;">
        <!-- PORTLET MAIN -->
        <div class="portlet light profile-sidebar-portlet">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
                <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/profile_placeholder.png' ?>" class="img-responsive" alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                   <?php echo ucwords($prof->full_name); ?>
                </div>

            </div>
            <!-- END SIDEBAR USER TITLE -->


        </div>
        <!-- END PORTLET MAIN -->

    </div>
    <!-- END BEGIN PROFILE SIDEBAR -->
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase"> <?php echo $prof->full_name; ?> Profile Account &nbsp;</span>
                        </div>
                        <div class="actions pull-left">

                            <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle btn-sm"'); ?>
                            <?php echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-warning btn-sm"'); ?>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="<?php echo $subtitle=='edit'||$subtitle=='info'?'active':''; ?>">
                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                            </li>
                            <li class="<?php echo $subtitle=='change_avatar'?'active':''; ?>">
                                <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                            </li>
                            <li <?php echo $subtitle=='change_password'?'active':''; ?>>
                                <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                            </li>

                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane <?php echo $subtitle=='edit'||$subtitle=='info'?'active':''; ?>" id="tab_1_1">
                                <?php echo form_open($this->page_level.$this->page_level2.'edit/'.$prof->id*date('Y')) ?>
                                    <div class="form-group">
                                        <label class="control-label">Username </label>
                                        <input type="text" readonly placeholder="<?php echo $prof->username; ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Full Names</label><?php echo form_error('fullname','<label style="color:red;">','</label>') ?>
                                        <input type="text" name="fullname" placeholder=" <?php echo ucwords($prof->full_name); ?>" value="<?php echo ucwords($prof->full_name); ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email </label><?php echo form_error('email','<label style="color:red;">','</label>') ?>
                                        <input type="email" name="email" placeholder="Add your email" value="<?php echo $prof->email ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Phone </label> <?php echo form_error('phone','<label style="color:red;">','</label>') ?>
                                        <input type="text" name="phone" placeholder="Add Your Mobile Phone" value="<?php echo $prof->phone; ?>" class="form-control"/>
                                    </div>
                                <div class="form-group">
                                    <label class="control-label">Branch </label> <?php echo form_error('branch','<label style="color:red;">','</label>') ?>
                                    <div class="form-group">
                                        <?php $bra=$this->db->select('branch_name')->from('branch')->where('id',$prof->branch_id)->get()->row(); ?>
                                        <select class="form-control" name="branch" required>
                                            <option value="<?php echo isset($bra->branch_name)?$prof->branch_id:'' ?>" <?php echo set_select('branch', isset($bra->branch_name)?$prof->branch_id:'', TRUE); ?> ><?php echo  isset($bra->branch_name)?$bra->branch_name:'N/A' ?></option>
                                            <?php foreach($this->db->select('id,branch_name,country,street')->from('branch')->where('id !=',$prof->branch_id)->get()->result() as $br): ?>
                                            <option value="<?php echo $br->id ?>" <?php echo set_select('branch', $br->id); ?> ><?php echo $br->branch_name ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                </div>
<!--                                This is the for the branch Select-->
                                <?php if($this->session->userdata('user_type')!='2'){ ?>
                                <div class="form-group">
                                    <label class="control-label">Role </label> <?php echo form_error('role','<label style="color:red;">','</label>') ?>
                                    <div class="form-group">
                                        <?php $rol=$this->db->select('title')->from('user_type')->where('id',$prof->user_type)->get()->row();

                                        ?>
                                        <select class="form-control" name="role">

                                            <?php
                                            $ut=$this->session->userdata('user_type');

                                            $this->db->select('id,title')->from('user_type');
                                            $ut==1||$ut==3?'':$this->db->where('id',2);
                                            $role=$this->db->order_by('id','asc')->get()->result();
                                            foreach($role as $role): ?>
                                                <option value="<?php echo $role->id ?>" <?php echo set_select('role', $role->id,($role->id==$prof->user_type?TRUE:'')); ?> ><?php echo humanize($role->title) ?></option>
                                            <?php  endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="control-label">Access Control</label>
                                    <div class="md-radio-inline ">
                                        <div class="md-radio">
                                            <input type="radio" <?php echo $prof->access=='c'?'checked':'' ?> id="c" name="access" value="c" <?php echo set_radio('access','c'); ?> class="md-radiobtn">
                                            <label for="c">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Country </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" <?php echo $prof->access=='b'?'checked':'' ?> id="b" name="access"  value="b" <?php echo set_radio('access','b'); ?> class="md-radiobtn">
                                            <label for="b">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Branch </label>
                                        </div>

                                    </div>

                                </div>


                                    <div class="margiv-top-10">
                                        <hr/>
                                        <button  class="btn green-haze" type="submit"> Update Changes </button>
                                        <button type="reset" class="btn default">
                                            Cancel </button>
                                    </div>
                                <?php  } ?>
                                <?php echo form_close(); ?>
                            </div>
                            <!-- END PERSONAL INFO TAB -->
                            <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane <?php echo $subtitle=='change_avatar'?'active':''; ?>" id="tab_1_2">
                                <p>
                                    You Can Make Changes to the photo.
                                </p>
                                <?php echo form_open_multipart($this->page_level.$this->page_level2.'change_avatar/'.$prof->id*date('Y'))?>
                                    <div class="form-group">

                                        <?php if( isset($error)){?>
                                            <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                                        <?php } echo form_error('image','<label style="color:red;">','</label>') ?>

                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/assets/img/avatars/placeholder.png' ?>" alt=""/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="image" required>
																</span>
                                                <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>

                                    </div>

                                     <?php echo form_error('pa','<label style="color:red;">','</label>') ?>
                                    <input type="hidden" name="pa"  placeholder="What is your current password ?"  class="form-control"/>

                                    <div class="margin-top-10">

                                        <button type="submit" class="btn green-jungle">Upload <i class="fa fa-upload"></i></button>
                                        <a href="#" class="btn default">
                                            Cancel </a>
                                    </div>
                                <?php echo form_close() ?>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                            <!-- CHANGE PASSWORD TAB -->
                            <div class="tab-pane <?php echo $subtitle=='change_password'?'active':''; ?>" id="tab_1_3">
                                <?php echo form_open($this->page_level.$this->page_level2.'change_password/'.$prof->id*date('Y')) ?>
                                    <div class="form-group hidden">
                                        <label class="control-label">Current Password </label><?php echo form_error('current_password','<label style="color: red;">','</label>') ?>
                                        <input name="current_password" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">New Password </label><?php echo form_error('new_pass','<label style="color: red;">','</label>') ?>
                                        <input name="new_pass" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Re-type New Password </label><?php echo form_error('rpt_pass','<label style="color: red;">','</label>') ?>
                                        <input name="rpt_pass" autocomplete="off" type="password" class="form-control"/>
                                    </div>
                                    <div class="margin-top-10">
                                        <button type="submit" class="btn green-haze">
                                            Change Password </button>
                                        <button type="reset" class="btn default">
                                            Cancel </button>
                                    </div>
                               <?php echo form_close(); ?>
                            </div>
                            <!-- END CHANGE PASSWORD TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->
</div>

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
