<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark caption-subject font-green bold uppercase" >



                    <?php echo form_open('','class="form-inline"') ?>
                    <?php   echo isset($first_day)? humanize($subtitle).' Report From : '.date('d M Y',$first_day).' - '.date('d M Y',$last_day):humanize($subtitle); ?>
                    <div class="form-group  hidden-print">

                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" value="<?php echo set_value('from') ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" value="<?php echo set_value('to')?>">
                        </div>
                    </div>

                    <button type="submit" class="btn green  hidden-print"><i class="fa fa-sliders"></i> Filter</button>
                    <?php echo form_close(); ?>
                </div>
                <div class="tools">
                <div class="btn-group  hidden-print">

                    <button class="btn blue  btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print </button>


                </div>
                    <div class="btn-group  hidden-print">




                        <?php echo anchor($this->page_level.'export_excel/branch_report/'.$first_day.'/'.$last_day,'<i class="fa fa-file-excel-o"></i> Export','class="btn green  btn-outline "'); ?>

                    </div>
                    </div>


            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample">
                    <!--Name, Branch, Transfer (total), Cashout (total), Commision, View detail -->
                    <thead>
                    <tr>
                        <th> Name </th>
                        <th> Branch </th>


                        <th> TT Transfer(USD) </th>

                        <th> TT Cashout</th>
                        <th>TT Commission</th>
                        <th class="hidden-print"></th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach($tellers as $t): ?>

                        <?php

                        $period = array(

                            'created_on >=' => $first_day,
                            'created_on <=' => $last_day
                        )

                        ?>

                        <tr>
                            <td title="View Sender Profile Details">
                                <?php

                                echo ( isset($t->full_name)?ucwords($t->full_name):'N/A') ?>

                            </td>
                            <td>  <?php
                                $bn=$this->db->select('branch_name')->from('branch')->where('id',$t->branch_id)->get()->row();
                                echo isset($bn->branch_name)? word_limiter($bn->branch_name,4):'N/A' ?> </td>




                            <td align="right"> <?php
                               $transfer=$this->db->select_sum('sent_amount_usd')->from('transactions')->where( $period )->where(array( 'created_by'=>$t->id))->get()->row();
                                echo number_format($transfer->sent_amount_usd) ?> </td>
                            <td align="right"> <?php
                                $cashout=$this->db->select_sum('received_amount_usd')->from('transactions')->where($period)->where(array('status'=>'cashed_out','updated_by'=>$t->id))->get()->row();
                                echo number_format($cashout->received_amount_usd) ?> </td>
                            <td align="right"> <?php
                                $comm=$this->db->select_sum('commission_usd')->from('transactions')->where($period)->where('created_by',$t->id)->get()->row();
                                echo number_format($comm->commission_usd,3) ?> </td>


                            <td class="hidden-print">

                                <?php echo anchor($this->page_level.$this->page_level2.$this->uri->slash_segment(3).'details/'.$t->id*date('Y'),'<i class="icon-eye"></i> View Details','class="btn green-jungle btn-sm"') ?>
                            </td>


                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->