<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">


                <div class="caption font-dark caption-subject font-green uppercase" >


                    <!--This is the beginfng of the filters  -->


                    <?php
                    $uri_extend= strlen($this->uri->segment(4))&& strlen($this->uri->segment(5))?$this->uri->slash_segment(4).$this->uri->slash_segment(5):'';

                    $form_attributes=array(
                        'class'=>'form-inline',
                    );

                    echo form_open($this->page_level.$this->page_level2.$this->uri->slash_segment(3).$uri_extend,$form_attributes) ?>



                    <!--                    these are the titles and headers    -->
                    <?php echo $subtitle=='above_5000'||$subtitle=='on_hold'||$subtitle=='branch'||$subtitle=='teller'?'':anchor($this->page_level.$this->page_level2.$this->uri->slash_segment(3).'export','<i class="icon-printer"></i> CB Report','class="btn  btn-sm green-jungle hidden-print"') ?>
                    <?php

                    //echo isset($first_day)? humanize($subtitle).' Report From : '.date('d M Y',$first_day).' - '.date('d M Y',$last_day):humanize($subtitle); ?>

                    <?php if($subtitle=='branch'){
                        $name=$this->db->select('branch_name')->from('branch')->where('id',$id)->get()->row();
                        echo '  '. ucwords($name->branch_name);
                    }
                    elseif($subtitle=='teller'){

                        $name=$this->db->select('full_name')->from('users')->where('id',$id)->get()->row();
                        echo '  '. ucwords($name->full_name);

                    } ?>

                    <!--                    these are the end of the title and headers-->

<span class="hidden-print">


                      <div class="form-group">

                          <?php echo form_error('filter_type','<label style="color:#ff0000;">','</label>'); ?>

                          <select class="form-control input-sm" name="filter_type" style="width: 80px;" >

                              <option value="date_sent" <?php echo set_select('filter_type', 'date_sent',TRUE); ?> >Sent</option>
                              <option value="date_cashout" <?php echo set_select('filter_type', 'date_cashout'); ?> >Cashout</option>


                          </select>
                      </div>

                    <div class="form-group">

                        <div class="input-group input-group-sm input-medium date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" value="<?php echo $this->input->post('from') ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" value="<?php echo $this->input->post('to')?>">
                        </div>
                    </div>

                    <div class="form-group <?php echo $this->uri->segment(4)=='details'?'hidden':''; ?>" >
                        <?php echo form_error('country','<label style="color:#ff0000;">','</label>'); ?>
                        <select class="form-control input-sm" name="country" style="width: 100px;">
                            <option value="" <?php echo set_select('country', '', TRUE); ?> >Country</option>
                            <?php foreach($this->db->select('a2_iso,country')->from('selected_countries')->get()->result() as $cty): ?>
                                <option value="<?php echo $cty->a2_iso ?>" <?php echo set_select('country', $cty->a2_iso); ?> ><?php echo $cty->country ?></option>
                            <?php endforeach; ?>



                        </select>
                    </div>

                    <div class="form-group">

                        <?php echo form_error('status','<label style="color:#ff0000;">','</label>'); ?>

                        <select class="form-control input-sm" name="status" style="width: 90px;" >
                            <option value="" <?php echo set_select('status', '', TRUE); ?> >Status</option>
                            <?php foreach($status as $st){ ?>
                                <option value="<?php echo $st ?>" <?php echo set_select('status', $st); ?> ><?php echo humanize($st) ?></option>

                            <?php  } ?>
                            <option value="above_5000" <?php echo set_select('status', 'above_5000'); ?> >Above 5000$</option>


                        </select>
                    </div>

    <?php if($this->uri->segment(4)=='details'){ ?>
                    <div class="form-group">
                        <?php echo form_error('compare_branch','<label style="color:#ff0000;">','</label>'); ?>

                        <?php $branches=$this->db->select('id,branch_name')->from('branch')->get()->result() ?>
                        <select class="form-control input-sm" name="compare_branch" >
                            <option value="" <?php echo set_select('compare_branch', '', TRUE); ?> >Branch</option>

                            <?php foreach($branches as $st){ ?>
                                <option value="<?php echo $st->id ?>" <?php echo set_select('compare_branch', $st->id); ?> ><?php echo humanize($st->branch_name) ?></option>

                            <?php  } ?>



                        </select>
                    </div>
    <?php } ?>

                    <button type="submit" class="btn btn-sm green"><i class="fa fa-sliders"></i> Filter</button>
    </span>
                    <?php echo form_close(); ?>

                </div>
                <div class="tools">
                    <div class="btn-group   hidden-print">

                        <button class="btn blue btn-sm  btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print </button>


                    </div>

                    <div class="btn-group  hidden-print">


                        <?php

                        $export_link= $this->input->post('from')?'/custom_date/'.strtotime($this->input->post('from')).'/'.strtotime($this->input->post('to').'23:59:59'):$this->uri->slash_segment(3,'both');

                        echo $subtitle=='branch'||$subtitle=='teller'?'': anchor($this->page_level.'export'.$export_link,'<i class="fa fa-file-excel-o"></i> Export','class="btn green btn-sm btn-outline "'); ?>

                    </div>
                </div>


                <row class="visible-print">
<!--                    'created_on >=' => $first_day,-->
<!--                    'created_on <=' => $last_day,-->
                    <h4 class="bold"><?php echo humanize($subtitle) ?> Report  <?php echo $subtitle=='weekly'||$subtitle=='Monthly'? 'From '.date('d-m-Y',$first_day).' - '.date('d-m-Y',$last_day):'';  ?></h4>
                </row>

            </div>
            <div class="portlet-body">








                <?php if(count($t)==0){
                    $data=array(
                        'alert'=>'info',
                        'message'=>'No results for this Period Select another Period'
                    );

                    $this->load->view('alert',$data);
                }else{ ?>

                    <?php $this->load->view($page_level . $this->page_level2.'branch_summary') ?>


                    <table class="table table-striped table-bordered table-hover" id="sample">
                        <!--id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, -->
                        <thead>
                        <tr>
                            <th style="width: 8px;"> # </th>
                            <th> Txn ID </th>
                            <th> Sender </th>
                            <th> Recipient </th>


                            <th>Net Amt </th>

                            <th> Commission </th>
                            <?php echo isset($details)?' <th>Gross Amt </th>':'' ?>
                            <th>Tax</th>

                            <th>Source</th>
                            <th>Dest</th>

                            <th width="100">Date</th>
                            <th> Status </th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $no=1;
                        foreach($t as $trans): ?>
                            <tr>
                                <td>
                                    <?php echo $no; $no++; ?>
                                </td>
                                <td>
                                    <?php echo $trans->transaction_id ?>
                                </td>
                                <td title="View Sender Profile Details">
                                    <?php
                                    $user=$this->db->select('full_name')->from('users')->where('id',$trans->sender_id)->get()->row();
                                    echo ( isset($user->full_name)?ucwords($user->full_name):'N/A') ?>

                                </td>
                                <td > <?php echo ucwords($trans->receiver_name) ?> </td>


                                <td align="right"> <span style="font-size: xx-small"><?php echo $trans->recipient_currency ?></span> <?php echo number_format($trans->received_amount,2) ?> </td>
                                <td align="right">  <?php echo number_format($trans->commission_usd,3) ?> </td>

                                <?php if(isset($details)){ ?>
                                    <td align="right"> <span style="font-size: xx-small"><?php echo $trans->currency ?></span> <?php echo number_format($trans->sent_amount,2) ?> </td>
                                <?php } ?>

                                <td><?php echo $trans->other_charges ?></td>

                                <td>  <?php

                                    //this is the branch for the sender
                                    $c=$this->db->select('branch_name,branch_code')->from('branch')->where('id',$trans->branch_id)->get()->row();
                                    echo isset($c->branch_code)?$trans->sender_country.'-'.$c->branch_code:'N/A'; ?>
                                </td>
                                <td style="white-space: nowrap;">  <?php
                                    //this is the branch for the reciever
                                    $r=$this->db->select('branch_name,branch_code')->from('branch')->where('id',$trans->receiver_branch)->get()->row();
                                    echo isset($r->branch_code)? $trans->receiver_country.'-'.$r->branch_code:'N/A'; ?>
                                </td>



                                <td><?php echo date('d-m-Y',$trans->created_on) ?></td>


                                <td>

                                    <span class="hidden-print">
                                    <?php  $link=base_url('index.php/'.$this->page_level.'/cashout/process/'.$trans->id*date('Y'))?>

                                    <?php if($trans->status=='cashed_out'){ ?>
                                        <a href="<?php echo $link ?>" class="btn green-jungle btn-sm tooltips" data-original-title="Click to view Details" style="width:100px;"><?php echo humanize($trans->status) ?></a>
                                    <?php } elseif($trans->status=='canceled'){ ?>
                                        <a href="<?php echo $link ?>" class="btn red btn-sm tooltips" style="width:100px;" data-original-title="<?php echo  $trans->other_reason ?>"><?php echo humanize($trans->status) ?></a>

                                    <?php } elseif($trans->status=='hold'){ ?>
                                        <a href="<?php echo $link ?>" class="btn dark  btn-sm tooltips" data-original-title="<?php echo  $trans->hold_reason ?>" style="width:100px;"><?php echo humanize($trans->status) ?></a>
                                    <?php } elseif($trans->status=='not_approved'){ ?>
                                        <a href="<?php echo $link ?>" class="btn default btn-sm tooltips" data-original-title="Click to view Details" style="width:100px;"><?php echo humanize($trans->status) ?></a>

                                    <?php }else{ ?>

                                        <a href="<?php echo $link ?>" class="btn btn-info btn-sm tooltips" data-original-title="Click to view Details" style="width:100px;"><?php echo humanize($trans->status) ?></a>

                                    <?php } ?>

                                        </span>
                                    <span class="visible-print" style="white-space: nowrap;"> <?php echo humanize($trans->status) ?></span>

                                </td>


                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> Sender </th>
                            <th> Recipient </th>

                            <?php echo isset($details)?' <th>Sent Amt </th>':'' ?>
                            <th>Net Amt </th>

                            <th> Commission </th>
                            <th>Tax</th>
                            <th>Source</th>
                            <th>Dest</th>

                            <th width="100">Date</th>
                            <th> Status </th>

                        </tr>
                        </thead>
                    </table>

                <?php } ?>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->