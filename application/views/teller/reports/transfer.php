<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($title); ?>&nbsp;</span>
                </div>
                <div class="actions pull-left hidden-print">

                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools">
                    <div class="btn-group  hidden-print">
                        <button class="btn blue btn-sm  btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print </button>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <th> Sender </th>
                        <th> Recipient </th>
                        <th> Sent Amt </th>
                        <th> Commission </th>
                        <th> Amt Received </th>

                        <th> Phone </th>
                        <th hidden>Secret Code</th>
                        <th>Sender Country</th>
                        <th>Reciever Country</th>
                        <th> Branch </th>

<!--                        <th>Action</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach($t as $trans): ?>
                        <tr>
                            <td>
                                <span class="hidden-print">
                                <?php
                                $user=$this->db->select('full_name')->from('users')->where('id',$trans->sender_id)->get()->row();
                                echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'), isset($user->full_name)?ucwords($user->full_name):'N/A') ?>
                                </span>
                                <span class="visible-print"><?php echo isset($user->full_name)?ucwords($user->full_name):'N/A' ?></span>

                            </td>
                            <td > <?php echo isset($trans->receiver_name)?ucwords($trans->receiver_name):'N/A'; ?> </td>
                            <td align="right"> <?php echo number_format($trans->sent_amount) ?> </td>
                            <td align="right"> <?php echo number_format($trans->commission,3) ?> </td>
                            <td align="right"> <?php echo number_format($trans->received_amount) ?> </td>

                            <td>  <?php echo $trans->receiver_phone ?> </td>
                            <td hidden><?php echo $trans->receiver_phone ?></td>
                            <td>  <?php
                                $c=$this->db->select('country')->from('country')->where('a2_iso',$trans->sender_country)->get()->row();
                                echo isset($c->country)?$c->country:'N/A'; ?>
                            </td>
                            <td>  <?php
                                $c=$this->db->select('country')->from('country')->where('a2_iso',$trans->receiver_country)->get()->row();
                                echo isset($c->country)?$c->country:'N/A'; ?>
                            </td>
                            <td>  <?php echo $trans->branch_id ?> </td>
                            <td>

                                <?php if($trans->status=='cashed_out'){ ?>
                                <div class="btn green-jungle btn-sm" style="width:110px;"><?php echo humanize($trans->status) ?></div>
                                <?php }else{ ?>

                                    <div class="btn btn-info btn-sm" style="width:110px;"><?php echo humanize($trans->status) ?></div>

                                <?php } ?>
                                 </td>

                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->


<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->