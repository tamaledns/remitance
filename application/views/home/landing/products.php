<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Services</li>
        </ul>

        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12 col-sm-12">
                <h1>About Us</h1>
                <div class="content-page">
                    <div class="row">
                        <!-- BEGIN SERVICE BLOCKS -->
                        <div class="col-md-7">
                            <div class="row margin-bottom-20">
                                <div class="col-md-6">
                                    <div class="service-box-v1">
                                        <div><i class="fa fa-thumbs-up color-grey"></i></div>
                                        <h2>Harum quidem</h2>
                                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="service-box-v1">
                                        <div><i class="fa fa-cloud-download color-grey"></i></div>
                                        <h2>Denim robatl</h2>
                                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-bottom-20">
                                <div class="col-md-6">
                                    <div class="service-box-v1">
                                        <div><i class="fa fa-dropbox color-grey"></i></div>
                                        <h2>Solnh mateore</h2>
                                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="service-box-v1">
                                        <div><i class="fa fa-gift color-grey"></i></div>
                                        <h2>Molestia pore</h2>
                                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-bottom-20">
                                <div class="col-md-6">
                                    <div class="service-box-v1">
                                        <div><i class="fa fa-comments color-grey"></i></div>
                                        <h2>Harum quidem</h2>
                                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="service-box-v1">
                                        <div><i class="fa fa-globe color-grey"></i></div>
                                        <h2>Denim robatl</h2>
                                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END SERVICE BLOCKS -->

                        <!-- BEGIN VIDEO AND TESTIMONIALS -->
                        <div class="col-md-5">
                            <!-- BEGIN VIDEO -->
                            <iframe height="270" allowfullscreen="" style="width:100%; border:0" src="http://player.vimeo.com/video/56974716?portrait=0" class="margin-bottom-10"></iframe>
                            <!-- END VIDEO -->

                            <!-- BEGIN TESTIMONIALS -->
                            <div class="testimonials-v1 testimonials-v1-another-color">
                                <h2>Clients Testimonials</h2>
                                <div id="myCarousel1" class="carousel slide">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                        <div class="active item">
                                            <blockquote><p>Denim you probably haven't heard of. Lorem ipsum dolor met consectetur adipisicing sit amet, consectetur adipisicing elit, of them jean shorts sed magna aliqua. Lorem ipsum dolor met consectetur adipisicing sit amet do eiusmod dolore.</p></blockquote>
                                            <div class="carousel-info">
                                                <img class="pull-left" src="../../assets/frontend/pages/img/people/img1-small.jpg" alt="">
                                                <div class="pull-left">
                                                    <span class="testimonials-name">Lina Mars</span>
                                                    <span class="testimonials-post">Commercial Director</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <blockquote><p>Raw denim you Mustache cliche tempor, williamsburg carles vegan helvetica probably haven't heard of them jean shorts austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p></blockquote>
                                            <div class="carousel-info">
                                                <img class="pull-left" src="../../assets/frontend/pages/img/people/img5-small.jpg" alt="">
                                                <div class="pull-left">
                                                    <span class="testimonials-name">Kate Ford</span>
                                                    <span class="testimonials-post">Commercial Director</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <blockquote><p>Reprehenderit butcher stache cliche tempor, williamsburg carles vegan helvetica.retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p></blockquote>
                                            <div class="carousel-info">
                                                <img class="pull-left" src="../../assets/frontend/pages/img/people/img2-small.jpg" alt="">
                                                <div class="pull-left">
                                                    <span class="testimonials-name">Jake Witson</span>
                                                    <span class="testimonials-post">Commercial Director</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Carousel nav -->
                                    <a class="left-btn" href="#myCarousel1" data-slide="prev"></a>
                                    <a class="right-btn" href="#myCarousel1" data-slide="next"></a>
                                </div>
                            </div>
                            <!-- END TESTIMONIALS -->
                        </div>
                        <!-- END BEGIN VIDEO AND TESTIMONIALS -->
                    </div>

                    <!-- BEGIN BLOCKQUOTE BLOCK -->
                    <div  class="row quote-v1 margin-bottom-30">
                        <div class="col-md-9">
                            <span>For News and ADs subscription </span>
                        </div>
                        <div class="col-md-3 text-right">

                            <?php echo anchor('home/signup','<i class="fa fa-rocket margin-right-10"></i> Register with us','class="btn-transparent"') ?>
                        </div>
                    </div>
                    <!-- END BLOCKQUOTE BLOCK -->

                </div>
            </div>
            </oiv>
        </div>
    </div>
</div>