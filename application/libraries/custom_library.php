<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php

class custom_library
{
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
    }


    function dennis()
    {
        //$this->CI
        $users = $this->CI->db->select()->from('users')->get()->result();
        return ($users);
    }

    /**
     * @param $branch_id
     * @param $amount
     * @param $transaction_type
     * @param string $description
     * @return bool
     */

    function update_statement($branch_id, $amount, $transaction_type, $description = '', $txn_id = null)
    {

        $current_user = $this->CI->session->userdata('id');
        $session_id = $this->CI->session->userdata('session_id');


        $branch = $this->CI->db->select('balance')->from('branch')->where(array('id' => $branch_id))->get()->row();


        if (count($branch) == 1) {

            $credit = 0;
            $debit = 0;

            if ($transaction_type == 'credit') {

                $credit = $amount;
                $balance = $branch->balance + $amount;

            } elseif ($transaction_type == 'debit') {

                $debit = $amount;
                $balance = $branch->balance - $amount;

            }


            $check = array(
                'type' => $transaction_type,
                'debit' => $debit,
                'credit' => $credit,
                'branch_id' => $branch_id,
                'txn_id' => $txn_id,
                'created_by' => $current_user,
            );

            $record = $this->CI->db->where($check)->from('branch_statements')->count_all_results();

            if ($record == 0) {


                // 00a922295d960bda7c7630797bfa44e5
                $values = array(
                    'type' => $transaction_type,
                    'debit' => $debit,
                    'credit' => $credit,
                    'balance' => $balance,
                    'branch_id' => $branch_id,
                    'description' => $description,
                    'session_id' => '',
                    'txn_id' => $txn_id,
                    'created_on' => time(),
                    'created_by' => $current_user,

                );

                if ($this->CI->db->insert('branch_statements', $values)) {

                    $this->CI->db->where(array('id' => $branch_id))->update('branch', array('balance' => $balance, 'updated_on' => time(), 'updated_by' => $current_user));

                    return true;
                }
            } else {
                return true;
            }


        } else {

            return false;
        }


    }

    /**
     * @param $teller_id
     * @param $amount
     * @param $transaction_type
     * @param string $description
     * @return bool
     */

    function update_teller_statement($teller_id, $amount, $transaction_type, $description = '', $txn_id)
    {

        $current_user = $this->CI->session->userdata('id');
        $session_id = $this->CI->session->userdata('session_id');


        $branch = $this->CI->db->select('balance')->from('users')->where(array('id' => $teller_id))->get()->row();


        if (count($branch) == 1) {

            $credit = 0;
            $debit = 0;

            if ($transaction_type == 'credit') {

                $credit = $amount;
                $balance = $branch->balance + $amount;

            } elseif ($transaction_type == 'debit') {

                $debit = $amount;
                $balance = $branch->balance - $amount;

            }

            $check = array(
                'type' => $transaction_type,
                'debit' => $debit,
                'credit' => $credit,
                'teller_id' => $teller_id,
                'created_by' => $current_user,
                'txn_id' => $txn_id
            );

            $record = $this->CI->db->where($check)->from('teller_statements')->count_all_results();

            if ($record == 0) {

                $values = array(
                    'type' => $transaction_type,
                    'debit' => $debit,
                    'credit' => $credit,
                    'balance' => $balance,
                    'teller_id' => $teller_id,
                    'description' => $description,
                    'session_id' => '',
                    'txn_id' => $txn_id,
                    'created_on' => time(),
                    'created_by' => $current_user,

                );

                if ($this->CI->db->insert('teller_statements', $values)) {

                    $this->CI->db->where(array('id' => $teller_id))->update('users', array('balance' => $balance, 'updated_on' => time(), 'updated_by' => $current_user));

                    return true;
                }
            } else {
                return true;
            }


        } else {

            return false;
        }


    }


    function branch_balance($branch_id)
    {

        $branch = $this->CI->db->select('balance')->from('branch')->where(array('id' => $branch_id))->get()->row();

        if (count($branch) == 1) {

            $balance = $branch->balance;

            return $balance;
        } else {
            return false;
        }
    }

    function user_balance($user_id)
    {

        $user = $this->CI->db->select('balance')->from('users')->where(array('id' => $user_id))->get()->row();

        if (count($user) == 1) {

            $balance = $user->balance;

            return $balance;
        } else {
            return false;
        }
    }

    function send_without_money($branch_id)
    {

        $branch = $this->CI->db->select('balance')->from('branch')->where(array('id' => $branch_id))->get()->row();

        $send_to_branch_without_money = $this->CI->db->select('site_options')->where('option_name', 'send_to_branch_without_money')->get()->row();

        if (count($branch) == 1) {


            $balance = $branch->balance;
            return $balance;

        } else {

            return false;
        }
    }


    function sendHTMLEmail2($to, $subject, $message)
    {


        $mailto = $to;
        // $file="thanks.htm";
        $pcount = 0;
        $gcount = 0;
        $subject = $subject;
        $b = time();
        $pstr = $message;//$this->email_template($message);
        $gstr = $message;//$this->email_template($message);
        $from = "noreply@deltaits.net";


        $headers = sprintf("From: GEF<noreply@opm.co.ug>\r\n");
        $headers .= sprintf("MIME-Version: 1.0\r\n");
        $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");

        while (list($key, $val) = each($_POST)) {
            $pstr = $pstr . "$key : $val \n ";
            ++$pcount;

        }
        while (list($key, $val) = each($_GET)) {
            $gstr = $gstr . "$key : $val \n ";
            ++$gcount;

        }
        if ($pcount > $gcount) {
            $message_body = $message;//$pstr;
            // $message_body = $pstr;
            mail($mailto, $subject, $message_body, "From:" . $from);
            //$send = @mail($to, $subject, $body, $headers);

            $this->db->insert('outbox', array('to_user' => $mailto, 'subject' => $subject, 'm_type' => 'Email', 'message' => $message_body, 'created_on' => time(),));
            //  include("$file");
            return true;
        } else {
            $message_body = $gstr;

            if (!mail($mailto, $subject, $message_body, "From:" . $from)) {
//                die ("Not sent");
                return false;
            } else {
                // include("$file");
                // print $b;
                return true;
            }
        }

    }


    function email_template($body)
    {
        $logo = 'http://citiexpress.net/Ariane/wp-content/uploads/2016/05/Coat_of_arms_of_the_Republic_of_Uganda.svg.png';
        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				<html style="-webkit-text-size-adjust: none;-ms-text-size-adjust: none;">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Government Evaluation Facility | Email</title>
				<style type="text/css">
				html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
				@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
					*[class="table_width_100"] {
						width: 96% !important;
					}
					*[class="border-right_mob"] {
						border-right: 1px solid #dddddd;
					}
					*[class="mob_100"] {
						width: 100% !important;
					}
					*[class="mob_center"] {
						text-align: center !important;
						padding: 0 !important;
					}
					*[class="mob_center_bl"] {
						float: none !important;
						display: block !important;
						margin: 0px auto;
					}
					.iage_footer a {
						text-decoration: none;
						color: #929ca8;
					}
					img.mob_display_none {
						width: 0px !important;
						height: 0px !important;
						display: none !important;
					}
					img.mob_width_50 {
						width: 40% !important;
						height: auto !important;
					}
					img.mob_width_80 {
						width: 80% !important;
						height: auto !important;
					}
					img.mob_width_80_center {
						width: 80% !important;
						height: auto !important;
						margin: 0px auto;
					}
					.img_margin_bottom {
						font-size: 0;
						height: 25px;
						line-height: 25px;
					}
				}
				.table_width_100 {
					width: 680px;
				}
				</style>
				</head>

				<body style="padding: 0px; margin: 0px;">
				<div id="mailsub" class="notification" align="center">

				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


				<!--[if gte mso 10]>
				<table width="680" border="0" cellspacing="0" cellpadding="0">
				<tr><td>
				<![endif]-->

				<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px;min-width: 300px;width: 680px;">
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="180" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="' . $logo . '" alt="MixaKids" border="0" style="display: block;height:100px"></font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
									<table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl" style="width: 88px;">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td width="30" align="center" style="line-height: 19px;">
															<a href="https://www.facebook.com/opmuganda/" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/facebook.gif" width="10" height="19" alt="Follow us on Facebook" border="0" style="display: block;"></font></a>
														</td><td width="39" align="center" style="line-height: 19px;">
															<a href="https://www.twitter.com/mixakids" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/twitter.gif" width="19" height="16" alt="Follow us on Twitter" border="0" style="display: block;"></font></a>
														</td><!--<td width="29" align="right" style="line-height: 19px;">
															<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/dribbble.gif" width="19" height="19" alt="Dribbble" border="0" style="display: block;"></font></a>
														</td>--></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header END-->

					<!--content 1 -->
					<tr><td align="center" bgcolor="#f8f8f8">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
								<font face="Arial, Helvetica, sans-serif" size="4" color="#333" style="font-size: 15px;">
									<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #333;">
										' . $body . '
									</span>
								</font>
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>
					<!--content 1 END-->
					<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
						<div style="line-height: 22px;">
							<font face="Arial, Helvetica, sans-serif" size="5" color="#6b6b6b" style="font-size: 20px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #6b6b6b;">
									Thank you for Subscribing with <strong>Governmnent evaluation Facility</strong>
								</span>
							</font>
						</div>
					<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>

					<!--footer -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 15px; line-height: 15px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="115" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="http://www.opm.go.ug" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="src="' . $logo . '"" alt="Government Evaluation Facility" border="0" style="display: block;width:100%" /><br>
													Support Team</font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 150px;">
									<table class="mob_center" width="150" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															FAQ</font></a>
														</td><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Blog</font></a>
														</td><td align="right" style="line-height: 19px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Contact</font></a>
														</td></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--footer END-->
					<tr>
						<td>
							<p>&nbsp;</p>
							<p style="background:#e9e9e9;color:#333"><small>This email was sent automatically by <a href="http://opm.go.ug/">opm.go.ug/</a>. Please, do not reply</small></p>
						</td>
					</tr>
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
				</table>
				<!--[if gte mso 10]>
				</td></tr>
				</table>
				<![endif]-->

				</td></tr>
				</table>

				</div>
				</body>
		</html>';
        return $html;
    }


    function sendHTMLEmail($to, $subject, $message)
    {
        $headers = sprintf("From: Government Evaluation Facility<noreply@opm.go.ug>\r\n");
        $headers .= sprintf("MIME-Version: 1.0\r\n");
        $headers .= "Reply-To: " . $to . "\r\n";
        $headers .= "Bcc: tamaledns@gmail.com\r\n";
        $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");
        $send = @mail($to, $subject, $message, $headers);
        if ($send) {
            return true;
        } else {
            return false;
        }
    }


    function sendMail($to, $subject, $message, $attachment = null, $cc = null, $bcc = null)
    {

        //$this->CI->load->library('email');
        //$this->CI->load->helper('email');

        $config['protocol'] = 'smtp';
        //$config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['useragent'] = $this->CI->site_options->title('site_name');
        $config['smtp_host'] = 'www.wealthmoneytransfer.com';
        $config['smtp_user'] = 'no-reply@wealthmoneytransfer.com';
        $config['smtp_pass'] = 'svuN82^6';
        $this->CI->email->initialize($config);

        if (valid_email($to)) {

            $this->CI->email->clear(TRUE);
            $this->CI->email->from('no-reply@wealthmoneytransfer.com', $this->CI->site_options->title('site_name'));
            $this->CI->email->to($to);
            isset($cc) && valid_email($cc) ? $this->CI->email->cc($cc) : '';
            isset($bcc) && valid_email($bcc) ? $this->CI->email->bcc($bcc) : '';

            $this->CI->email->subject($subject);
//            $this->email->message($message);
            $data['message'] = $message;
            isset($attachment) ? $this->CI->email->attach($attachment) : '';
            $this->CI->email->message($this->CI->load->view('email_templates/email', $data, true));
            $this->CI->email->set_mailtype('html');


            if ($this->CI->email->send()) {
                $this->CI->db->insert('outbox', array('to_user' => $to, 'subject' => $subject, 'm_type' => 'email', 'message' => $message, 'created_on' => time(),));

                echo 'message sent';
                // return true;
            } else {
                echo 'message failed';
//                false;
            }

        } else {
            echo 'Message Failed firsttime';
            //return false;
        }
    }


}